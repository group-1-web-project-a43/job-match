package com.example.jobmatch.helpers;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.models.Location;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.services.interfaces.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class FilteringHelper {

    private final LocationService locationService;

    @Autowired
    public FilteringHelper(LocationService locationService) {
        this.locationService = locationService;
    }


    public boolean checkForMatchingSkills(Map<Skill, SkillLevel> skillSet,
                                          Map<Skill, SkillLevel> requirements,
                                          int threshold) {

        Map<Skill, SkillLevel> requirementsCopy = new HashMap<>(requirements);

        for (Map.Entry<Skill, SkillLevel> entry : skillSet.entrySet()) {
            if (requirementsCopy.containsKey(entry.getKey())) {
                if (requirementsCopy.get(entry.getKey()).ordinal() <= (entry.getValue()).ordinal()) {
                    requirementsCopy.remove(entry.getKey());
                }
            }
        }

        return requirementsCopy.size() - threshold <= 0;
    }


    public Optional<Integer> getLocationFilter(String locationInput) {
        if (locationInput == null || locationInput.isEmpty()) {
            return Optional.empty();
        }
        Location location;
        if (locationInput.contains(",")) {
            location = getByCityAndCountry(locationInput.substring(0, locationInput.indexOf(",")),
                    locationInput.substring(locationInput.indexOf(",") + 1));
        } else {
            location = getByCity(locationInput);
        }
        if (location == null) return Optional.empty();
        return Optional.of(location.getId());
    }

    private Location getByCityAndCountry(String city, String country) {
        return locationService.getByCountryAndCity(country, city);
    }

    private Location getByCity(String city) {
        return locationService.getByCity(city);
    }
}
