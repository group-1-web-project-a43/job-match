package com.example.jobmatch.helpers;

import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.Account;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.Employee;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.services.interfaces.BaseAccountService;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.EmployeeService;
import com.example.jobmatch.services.interfaces.ProfessionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String INVALID_AUTHENTICATION_ERROR = "Invalid authentication";
    public static final String REQUIRES_AUTHENTICATION = "The requested resource requires authentication";
    private static final String AUTHENTICATION_FAILURE_EXCEPTION = "Wrong username or password";

    private final CompanyService companyService;
    private final ProfessionalService professionalService;
    private final EmployeeService employeeService;

    private final BaseAccountService accountService;

    @Autowired
    public AuthenticationHelper(CompanyService companyService, ProfessionalService professionalService, EmployeeService employeeService, BaseAccountService accountService) {
        this.companyService = companyService;
        this.professionalService = professionalService;
        this.employeeService = employeeService;
        this.accountService = accountService;
    }

    private static void checkForAuthorizationHeader(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    REQUIRES_AUTHENTICATION);
        }
    }

    public void tryGetLogged(HttpHeaders headers) {
        checkForAuthorizationHeader(headers);
        try {
            String accountCredentials = headers.getFirst(HttpHeaders.AUTHORIZATION);

            String username = getUsername(accountCredentials);
            String password = getPassword(accountCredentials);

            Account account = accountService.getAccountByUsername(username);

            if (!account.getPassword().equals(password)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
            }

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    INVALID_AUTHENTICATION_ERROR);
        }
    }

    public Company tryGetCompany(HttpHeaders headers) {
        checkForAuthorizationHeader(headers);

        String companyCredentials = headers.getFirst(HttpHeaders.AUTHORIZATION);
        String username = getUsername(companyCredentials);
        String password = getPassword(companyCredentials);

        Optional<Company> company = companyService.getByUsername(username);

        if (company.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }

        if (!company.get().getPassword().equals(password)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }

        return company.get();
    }

    public Employee tryGetEmployee(HttpHeaders headers) {
        checkForAuthorizationHeader(headers);
        String empCredentials = headers.getFirst(HttpHeaders.AUTHORIZATION);
        String username = getUsername(empCredentials);
        String password = getPassword(empCredentials);

        Optional<Employee> employee = employeeService.getByUsername(username);
        if (employee.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }

        if (!employee.get().getPassword().equals(password)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }

        return employee.get();
    }

    public Professional tryGetProfessional(HttpHeaders headers) {
        checkForAuthorizationHeader(headers);
        try {
            String profCredentials = headers.getFirst(HttpHeaders.AUTHORIZATION);
            String username = getUsername(profCredentials);
            String password = getPassword(profCredentials);

            Professional professional = professionalService.getByUsername(username);

            if (!professional.getPassword().equals(password)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
            }

            return professional;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    INVALID_AUTHENTICATION_ERROR);
        }
    }

    private String getPassword(String userInfo) {
        int index = userInfo.indexOf(" ");
        if (index == -1) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }
        return userInfo.substring(index + 1);
    }

    private String getUsername(String userInfo) {
        int index = userInfo.indexOf(" ");
        if (index == -1) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_AUTHENTICATION_ERROR);
        }
        return userInfo.substring(0, index);
    }

    public Account tryGetCurrentUser(HttpSession session) {
        String currentUsername = (String) session.getAttribute("currentUser");

        if (currentUsername == null) {
            throw new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR);
        }
        return accountService.getAccountByUsername(currentUsername);
    }

    public Professional tryGetCurrentProf(HttpSession session) {

        try {
            String currentUsername = (String) session.getAttribute("currentUser");

            if (currentUsername == null) {
                throw new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR);
            }
            return professionalService.getByUsername(currentUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR);
        }
    }

    public Employee tryGetCurrentEmployee(HttpSession session) {

        String currentUsername = (String) session.getAttribute("currentUser");
        if (currentUsername == null) {
            throw new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR);
        }
        Optional<Employee> employee = employeeService.getByUsername(currentUsername);
        return employee.orElseThrow(() -> new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR));

    }

    public Company tryGetCurrentCompany(HttpSession session) {
        String currentUsername = (String) session.getAttribute("currentUser");

        if (currentUsername == null) {
            throw new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR);
        }
        Optional<Company> company = companyService.getByUsername(currentUsername);
        return company.orElseThrow(() -> new UnauthorizedOperationException(INVALID_AUTHENTICATION_ERROR));
    }

    public void AuthenticationProfessionals(String username, String password) {
        try {
            Professional professional = professionalService.getByUsername(username);
            if (!professional.getPassword().equals(password)) {
                throw new UnauthorizedOperationException(AUTHENTICATION_FAILURE_EXCEPTION);
            }
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(AUTHENTICATION_FAILURE_EXCEPTION);
        }
    }

    public void AuthenticationCompanies(String username, String password) {
        Company company = companyService.getByUsername(username)
                .orElseThrow(() -> new UnauthorizedOperationException(AUTHENTICATION_FAILURE_EXCEPTION));
        if (!company.getPassword().equals(password)) {
            throw new UnauthorizedOperationException(AUTHENTICATION_FAILURE_EXCEPTION);
        }
    }

    public void AuthenticationEmployees(String username, String password) {
        Employee employee = employeeService.getByUsername(username)
                .orElseThrow(() -> new UnauthorizedOperationException(AUTHENTICATION_FAILURE_EXCEPTION));
        if (!employee.getPassword().equals(password)) {
            throw new UnauthorizedOperationException(AUTHENTICATION_FAILURE_EXCEPTION);
        }
    }
}
