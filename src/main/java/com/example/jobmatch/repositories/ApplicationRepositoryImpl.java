package com.example.jobmatch.repositories;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.repositories.interfaces.ApplicationRepositoryCustom;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Component
public class ApplicationRepositoryImpl implements ApplicationRepositoryCustom {

    private final SessionFactory sessionFactory;

    @Autowired
    public ApplicationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Application> filter(ApplicationFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptions.getName().ifPresent(value -> {
                filters.add("name like: nameStr");
                params.put("nameStr", "%" + value + "%");
            });

            filterOptions.getDescription().ifPresent(value -> {
                filters.add("description like: descriptionStr");
                params.put("descriptionStr", "%" + value + "%");
            });

            filterOptions.getMinSalary().ifPresent((value -> {
                filters.add("maxSalary >=: minSalaryStr");
                params.put("minSalaryStr", value);

            }));
            filterOptions.getMaxSalary().ifPresent(value -> {
                filters.add("minSalary <=: maxSalaryStr");
                params.put("maxSalaryStr", value);
            });

            filterOptions.getLocation().ifPresent(value -> {
                filters.add("location_id =: locationIdStr");
                params.put("locationIdStr", value);
            });

            filters.add("applicationStatus =: statusStr");
            params.put("statusStr", ApplicationStatus.ACTIVE);


            String queryString = " from Application " + " where " + String.join(" and ", filters) +
                    generateStringFromSort(filterOptions);
            Query<Application> query = session.createQuery(queryString, Application.class);
            query.setProperties(params);
            return query.list();
        }
    }

    private String generateStringFromSort(ApplicationFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }
        StringBuilder orderBy = new StringBuilder(" order by ");
        switch (filterOptions.getSortBy().get().toLowerCase()) {
            case "name":
                orderBy.append("name");
                break;
            case "minsalary":
                orderBy.append("minSalary");
                break;
            case "maxsalary":
                orderBy.append("maxSalary");
                break;
            case "location":
                orderBy.append("location.id");
                break;
            default:
                throw new IllegalArgumentException(format("Cannot order by %s", filterOptions.getSortBy().get()));
        }

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy.append(" desc");
        }
        return orderBy.toString();

    }
}
