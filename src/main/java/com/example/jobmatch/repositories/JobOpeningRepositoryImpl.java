package com.example.jobmatch.repositories;

import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.repositories.interfaces.JobOpeningRepositoryCustom;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Component
public class JobOpeningRepositoryImpl implements JobOpeningRepositoryCustom {

    private final SessionFactory sessionFactory;

    @Autowired
    public JobOpeningRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<JobOpening> filter(JobFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptions.getName().ifPresent(value -> {
                filters.add("name like: nameStr");
                params.put("nameStr", "%" + value + "%");
            });

            filterOptions.getDescription().ifPresent(value -> {
                filters.add("description like: descriptionStr");
                params.put("descriptionStr", "%" + value + "%");
            });

            filterOptions.getMinSalary().ifPresent((value -> {
                filters.add("maxSalary >=: minSalaryStr");
                params.put("minSalaryStr", value);

            }));
            filterOptions.getMaxSalary().ifPresent(value -> {
                filters.add("minSalary <=: maxSalaryStr");
                params.put("maxSalaryStr", value);
            });

            filterOptions.getLocation().ifPresent(value -> {
                filters.add("location_id =: locationIdStr");
                params.put("locationIdStr", value);
            });

            filterOptions.getCompany().ifPresent(value -> {
                filters.add("company_id =: companyIdStr");
                params.put("companyIdStr", value);
            });

            filterOptions.getIsActive().ifPresent(value -> {
                filters.add("isActive =: activeStr");
                params.put("activeStr", value);
            });

            StringBuilder queryString = new StringBuilder(" from JobOpening ");

            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filters));
            }
            queryString.append(generateStringFromSort(filterOptions));
            Query<JobOpening> query = session.createQuery(queryString.toString(), JobOpening.class);
            query.setProperties(params);
            return query.list();
        }
    }

    private String generateStringFromSort(JobFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }
        StringBuilder orderBy = new StringBuilder(" order by ");
        switch (filterOptions.getSortBy().get().toLowerCase()) {
            case "name":
                orderBy.append("name");
                break;
            case "minsalary":
                orderBy.append("minSalary");
                break;
            case "maxsalary":
                orderBy.append("maxSalary");
                break;
            case "location":
                orderBy.append("location.id");
                break;
            case "date":
                orderBy.append("publishedOn");
                break;
            default:
                throw new IllegalArgumentException(format("Cannot order by %s", filterOptions.getSortBy().get()));
        }

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy.append(" desc");
        }
        return orderBy.toString();

    }

}
