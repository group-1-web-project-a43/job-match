package com.example.jobmatch.repositories.interfaces;


import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.ProfessionalInfo;
import org.springframework.data.repository.CrudRepository;

public interface ProfessionalInfoRepository extends CrudRepository<ProfessionalInfo, Integer> {

    ProfessionalInfo findProfessionalInfoByProfessionalEquals(Professional professional);
}
