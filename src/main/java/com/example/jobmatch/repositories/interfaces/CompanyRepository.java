package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Company;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;


public interface CompanyRepository extends PagingAndSortingRepository<Company, Integer> {

    boolean existsCompanyByNameEquals(String name);

    Optional<Company> getCompanyByNameEquals(String name);

    Optional<Company> getCompanyByUsernameEqualsIgnoreCase(String username);

    @Query("select c from Company c where c.name like %:name%")
    List<Company> searchCompanyByName(String name, Pageable pageable);
}
