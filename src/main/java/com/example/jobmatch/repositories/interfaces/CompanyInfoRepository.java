package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.CompanyInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CompanyInfoRepository extends CrudRepository<CompanyInfo, Integer> {

    Optional<CompanyInfo> findCompanyInfoByCompanyEquals(Company company);

}
