package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Location;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface LocationRepository extends CrudRepository<Location, Integer> {

    boolean existsLocationByCityAndCountry(String city, String country);

    Optional<Location> findLocationByCountryAndCity(String country, String city);

    Iterable<Location> findLocationsByCityEquals(String city);

}
