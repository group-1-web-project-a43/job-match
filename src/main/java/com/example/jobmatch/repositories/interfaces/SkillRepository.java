package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Skill;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SkillRepository extends CrudRepository<Skill, Integer> {

    boolean existsSkillBySkillName(String name);

    Optional<Skill> findSkillBySkillName(String name);
}
