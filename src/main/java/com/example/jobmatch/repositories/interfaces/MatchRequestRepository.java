package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.MatchRequest;
import com.example.jobmatch.models.Professional;
import org.springframework.data.repository.CrudRepository;

public interface MatchRequestRepository extends CrudRepository<MatchRequest, Integer> {

    Iterable<MatchRequest> findMatchRequestsByJobOpeningEquals(JobOpening jobOpening);

    boolean existsMatchRequestByJobOpeningEquals(JobOpening jobOpening);

    Iterable<MatchRequest> findMatchRequestsByApplication_Professional(Professional professional);
}
