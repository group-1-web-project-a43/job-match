package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.JobForm;
import org.springframework.data.repository.CrudRepository;


public interface JobFormRepository extends CrudRepository<JobForm, Integer> {
}
