package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.Professional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationRepository extends JpaRepository<Application, Integer>, ApplicationRepositoryCustom {

    Iterable<Application> findAllByProfessionalEquals(Professional professional);

}
