package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

    boolean existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(String username, String email);

    boolean existsAccountByEmailAndIdNot(String email, int id);

    Optional<Account> getAccountByEmailEquals(String email);

    Optional<Account> getAccountByUsernameEqualsIgnoreCase(String username);

}
