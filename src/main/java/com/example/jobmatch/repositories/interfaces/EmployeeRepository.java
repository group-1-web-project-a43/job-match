package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    Iterable<Employee> findAllByCompanyEquals(Company company);

    Optional<Employee> getEmployeeByUsernameEqualsIgnoreCase(String username);
}
