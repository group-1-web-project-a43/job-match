package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.JobOpening;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface JobOpeningRepository extends JpaRepository<JobOpening, Integer>, JobOpeningRepositoryCustom {

    Iterable<JobOpening> findAllByCompanyEquals(Company company);

    @Query("select j from JobOpening j where j.company = ?1 and j.isActive = ?2")
    Iterable<JobOpening> findJobOpeningsByCompanyAndActive(Company company, boolean active);

    @Query("select j from JobOpening j where j.isActive is true")
    List<JobOpening> searchByStatusActive();

    @Query("select j from JobOpening j where j.isActive is true order by j.publishedOn desc")
    List<JobOpening> findJobOpeningByPublishedOn();


}
