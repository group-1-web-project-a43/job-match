package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.filters.JobFilterOptions;

import java.util.List;


public interface JobOpeningRepositoryCustom {

    List<JobOpening> filter(JobFilterOptions filterOptions);
}
