package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;

import java.util.List;

public interface ApplicationRepositoryCustom {

    List<Application> filter(ApplicationFilterOptions filterOptions);
}
