package com.example.jobmatch.repositories.interfaces;

import com.example.jobmatch.models.Professional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProfessionalsRepository extends CrudRepository<Professional, Integer> {

    Optional<Professional> findProfessionalByUsernameEqualsIgnoreCase(String username);

    @Query("Select p from Professional p where concat(p.firstName,' ',p.lastName) like %?1%")
    List<Professional> findProfessionalByName(String fullName);

    int countProfessionalsByJobOffersIsTrue();
}
