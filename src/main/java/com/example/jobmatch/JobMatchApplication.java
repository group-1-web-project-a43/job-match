package com.example.jobmatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories("com.example.jobmatch.repositories")
public class JobMatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobMatchApplication.class, args);
    }

}
