package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.models.Account;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.services.interfaces.BaseAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BaseAccountServiceImpl implements BaseAccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public BaseAccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getAccountByUsername(String username) {
        Optional<Account> account = accountRepository.getAccountByUsernameEqualsIgnoreCase(username);
        return account.orElseThrow(() -> new EntityNotFoundException("Account", "username", username));
    }
}
