package com.example.jobmatch.services.interfaces;


import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.CompanyInfo;

import java.util.List;

public interface CompanyInfoService {

    List<CompanyInfo> getAll();

    CompanyInfo update(Company companyAuth, int companyId, CompanyInfo companyInfo);

    CompanyInfo getById(int id);

    CompanyInfo getInfoByCompanyId(int companyId);

    CompanyInfo create(Company company, CompanyInfo companyInfo);

    void delete(Company company, int companyId);
}
