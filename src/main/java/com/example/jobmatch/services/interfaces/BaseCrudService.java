package com.example.jobmatch.services.interfaces;

import java.util.List;

public interface BaseCrudService<T> {

    List<T> getAll();

    T getById(int id);

    T create(T t);

    T update(T t, int id);

    void delete(int id);

}
