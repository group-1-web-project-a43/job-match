package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;

import java.util.List;

public interface ApplicationService {

    List<Application> getAll();

    List<Application> searchActiveApplications(String name,
                                               String description,
                                               String location,
                                               Double minSalary,
                                               Double maxSalary,
                                               String sortBy,
                                               String sortOrder);

    List<Application> searchActiveApplications(ApplicationFilterOptions filterOptions);

    List<JobOpening> getPotentialMatchingJobOpenings(Professional professional, Application application,
                                                     double salaryThreshold, int skillThreshold);

    Application getById(int id);

    Application getProfessionalApplicationById(Professional professional, int id);

    Application create(Professional authenticated, Application application);

    Application update(Professional authenticated, Application t, int id);

    void addOrModifySkill(Professional authenticated, Application application, Skill skill, SkillLevel level);

    void removeSkill(Professional authenticated, Application application, Skill skill);

    void delete(Professional authenticated, Professional professional, int id);

}
