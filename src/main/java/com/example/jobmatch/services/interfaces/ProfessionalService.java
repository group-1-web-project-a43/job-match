package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.dtos.PasswordDto;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProfessionalService {

    List<Professional> getAll();

    int getAllClosedContracts();

    Professional getById(int id);

    Set<Application> getAllProfessionalApplications(Professional profAuth, int profId);

    Professional create(Professional professional);

    Professional update(Professional authenticated, Professional professional, int id);

    void setMainApplication(Professional authenticated, Application application, Professional professional);

    void delete(Professional authenticated, int id);

    Professional getByUsername(String username);


    List<Professional> searchProfessionalByName(Optional<String> keyword,
                                                Optional<String> sortBy,
                                                Optional<Boolean> desc,
                                                Optional<Integer> page,
                                                Optional<Integer> size);


    void changePassword(Professional authenticated, PasswordDto passwordDto, int id);
}
