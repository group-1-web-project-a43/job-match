package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.dtos.PasswordDto;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface CompanyService {

    List<Company> getAll();

    Page<Company> findAll(int pageNumber);

    Company getById(int id);

    Company getByName(String name);

    Company create(Company t);

    Company update(Company companyAuth, Company company, int id);

    List<Company> searchCompanyByName(Optional<String> keyword,
                                      Optional<String> filterBy,
                                      Optional<String> sortBy,
                                      Optional<Boolean> desc,
                                      Optional<Integer> page,
                                      Optional<Integer> size);

    Optional<Company> getByUsername(String username);

    void delete(Company company, int id);

    void changePassword(Company companyAuthenticated, PasswordDto passwordDto, int id);
}
