package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.*;

import java.util.List;
import java.util.Map;

public interface MatchRequestService {


    List<MatchRequest> getByJobOpening(Employee employee, JobOpening jobOpening);

    List<MatchRequest> getByProfessionalApplication(Professional professional);

    Map<JobOpening, Boolean> existMatchRequestsForJobOpening(Employee authenticated);

    Map<MatchRequest, Boolean> getByProfessional(Professional authenticated, Professional professional);

    MatchRequest getById(Employee employee, int id);

    MatchRequest create(Professional professional, JobOpening jobOpening, Application application);

    MatchRequest approve(Employee employee, Company company, JobOpening jobOpening, int id);

    void acceptJobOffer(Professional authenticated, Professional professional, int matchId);
}
