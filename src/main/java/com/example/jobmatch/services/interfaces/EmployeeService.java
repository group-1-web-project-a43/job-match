package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    List<Employee> getAllCompanyEmployees(Company companyAuth, Company company);

    Employee getCompanyEmployeeById(Employee employee, int companyId, int id);

    Optional<Employee> getByUsername(String username);

    Employee create(Company company, Employee employee);

    Employee update(Employee employeeAuth, Employee employee, int companyId, int id);

    void delete(Company companyAuth, int companyId, int id);


}
