package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.ProfessionalInfo;

public interface ProfessionalInfoService {

    ProfessionalInfo update(Professional professional,
                            ProfessionalInfo professionalInfo,
                            int id);

    ProfessionalInfo getById(int id);

    ProfessionalInfo create(Professional profAuth, Professional professional, ProfessionalInfo professionalInfo);

    void delete(Professional professional, int id);
}
