package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.Account;

public interface BaseAccountService {

    Account getAccountByUsername(String username);
}
