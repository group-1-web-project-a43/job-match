package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.models.JobForm;
import com.example.jobmatch.models.Skill;


public interface SkillService extends BaseCrudService<Skill> {

    Skill getByName(String name);

    <T extends JobForm> void addOrModifySkill(T jobForm, Skill skill, SkillLevel level);

    <T extends JobForm> void removeSkill(T jobForm, Skill skill);
}
