package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.filters.JobFilterOptions;

import java.util.List;


public interface JobOpeningService {

    List<JobOpening> getAll();

    List<JobOpening> searchJobOpening(String name,
                                      String description,
                                      String location,
                                      String company,
                                      Double minSalary,
                                      Double maxSalary,
                                      String active,
                                      String sortBy,
                                      String sortOrder);

    List<JobOpening> searchJobOpening(JobFilterOptions filterOptions);

    List<Application> getPotentialMatchingApplications(Employee authenticated, JobOpening jobOpening,
                                                       double salaryThreshold, int skillThreshold);


    JobOpening getById(int id);

    JobOpening getByIdAndCompany(int id, Company company);

    JobOpening create(Employee employee, JobOpening jobOpening);

    JobOpening update(Employee employee, JobOpening jobOpening, int id);

    void addOrModifySkill(Employee authenticated, JobOpening jobOpening, Skill skill, SkillLevel level);

    void removeSkill(Employee authenticated, JobOpening jobOpening, Skill skill);

    void deleteCompanyOpening(Employee employeeAuth, int id);

    int countJobOpeningsByActiveStatus();

    List<JobOpening> findJobOpeningByPublishedOn();
}
