package com.example.jobmatch.services.interfaces;

import com.example.jobmatch.models.Location;


public interface LocationService extends BaseCrudService<Location> {

    Location getByCity(String city);

    Location getByCountryAndCity(String country, String city);

}
