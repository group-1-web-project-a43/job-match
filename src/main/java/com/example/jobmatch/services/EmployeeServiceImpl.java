package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.Account;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.Employee;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.repositories.interfaces.EmployeeRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final String OTHER_COMPANIES_EMPLOYEES_ERROR = "Company and employee do not match.";
    private static final String INVALID_COMPANY_AUTHORIZATION = "Cannot edit other companies' data.";
    private static final String EMPLOYEE_AUTHORIZATION_ERROR = "Employees can only edit their own accounts";
    private static final String OTHER_COMPANY_EMPLOYEES_VIEW_ERROR = "Cannot view other companies' employees.";

    private final AccountRepository accountRepository;
    private final EmployeeRepository employeeRepository;
    private final CompanyService companyService;

    @Autowired
    public EmployeeServiceImpl(AccountRepository accountRepository,
                               EmployeeRepository employeeRepository,
                               CompanyService companyService) {
        this.accountRepository = accountRepository;
        this.employeeRepository = employeeRepository;
        this.companyService = companyService;

    }

    @Override
    public List<Employee> getAllCompanyEmployees(Company companyAuth, Company company) {
        if (!companyAuth.equals(company)) {
            throw new UnauthorizedOperationException(OTHER_COMPANY_EMPLOYEES_VIEW_ERROR);
        }

        return StreamSupport
                .stream(employeeRepository.findAllByCompanyEquals(company).spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Employee getCompanyEmployeeById(Employee employeeAuth, int companyId, int id) {
        Company company = companyService.getById(companyId);
        if (!employeeAuth.getCompany().equals(company)) {
            throw new UnauthorizedOperationException(OTHER_COMPANIES_EMPLOYEES_ERROR);
        }
        Employee employee = getById(id);
        if (!employee.getCompany().equals(company)) {
            throw new UnauthorizedOperationException(OTHER_COMPANIES_EMPLOYEES_ERROR);
        }
        return employee;

    }

    @Override
    public Optional<Employee> getByUsername(String username) {
        return employeeRepository.getEmployeeByUsernameEqualsIgnoreCase(username);
    }


    @Override
    public Employee create(Company company, Employee employee) {
        if (!company.equals(employee.getCompany())) {
            throw new UnauthorizedOperationException(INVALID_COMPANY_AUTHORIZATION);
        }
        if (accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                employee.getUsername(),
                employee.getEmail())) {
            throw new DuplicateEntityException("Account", "username or email");
        }

        return employeeRepository.save(employee);
    }

    @Override
    public Employee update(Employee employeeAuth, Employee employee, int companyId, int id) {
        Employee employeeToUpdate = getEmployeeFromCompany(companyId, id);

        if (!employeeAuth.equals(employeeToUpdate)) {
            throw new UnauthorizedOperationException(EMPLOYEE_AUTHORIZATION_ERROR);
        }

        Optional<Account> duplicateEmailCheck = accountRepository.getAccountByEmailEquals(employee.getEmail());
        if (duplicateEmailCheck.isPresent() && duplicateEmailCheck.get().getId() != id) {
            throw new DuplicateEntityException("Account", "email");
        }

        employee.setId(id);
        employee.setCompany(employeeToUpdate.getCompany());
        employee.setUsername(employeeToUpdate.getUsername());

        return employeeRepository.save(employee);
    }

    @Override
    public void delete(Company companyAuth, int companyId, int id) {
        Employee employeeToDelete = getEmployeeFromCompany(companyId, id);
        if (!companyAuth.equals(employeeToDelete.getCompany())) {
            throw new UnauthorizedOperationException(INVALID_COMPANY_AUTHORIZATION);
        }
        employeeRepository.delete(employeeToDelete);
    }

    private Employee getEmployeeFromCompany(int companyId, int id) {
        Company company = companyService.getById(companyId);
        Employee employee = getById(id);
        if (!employee.getCompany().equals(company)) {
            throw new UnauthorizedOperationException(OTHER_COMPANIES_EMPLOYEES_ERROR);
        }
        return employee;
    }

    private Employee getById(int id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isEmpty()) {
            throw new EntityNotFoundException("Employee", id);
        }
        return employee.get();
    }
}
