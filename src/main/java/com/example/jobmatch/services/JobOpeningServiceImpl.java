package com.example.jobmatch.services;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.FilteringHelper;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.repositories.interfaces.ApplicationRepository;
import com.example.jobmatch.repositories.interfaces.JobOpeningRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.JobOpeningService;
import com.example.jobmatch.services.interfaces.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JobOpeningServiceImpl implements JobOpeningService {

    public static final String JOB_OPENING_AND_COMPANY_MATCH_ERROR = "Job Opening and posting company don't match.";

    private final JobOpeningRepository jobOpeningRepository;
    private final CompanyService companyService;
    private final SkillService skillService;
    private final ApplicationRepository applicationRepository;
    private final FilteringHelper filteringHelper;

    @Autowired
    public JobOpeningServiceImpl(JobOpeningRepository jobOpeningRepository,
                                 CompanyService companyService,
                                 SkillService skillService, ApplicationRepository applicationRepository,
                                 FilteringHelper filteringHelper) {
        this.jobOpeningRepository = jobOpeningRepository;
        this.companyService = companyService;
        this.skillService = skillService;
        this.applicationRepository = applicationRepository;
        this.filteringHelper = filteringHelper;
    }

    @Override
    public List<JobOpening> getAll() {
        return jobOpeningRepository.findAll().stream()
                .sorted(Comparator.comparing(JobOpening::getId))
                .collect(Collectors.toList());
    }

    public int countJobOpeningsByActiveStatus() {
        return jobOpeningRepository.searchByStatusActive().size();
    }

    @Override
    public List<JobOpening> findJobOpeningByPublishedOn() {
        return jobOpeningRepository.findJobOpeningByPublishedOn();
    }

    @Override
    public List<JobOpening> searchJobOpening(String name,
                                             String description,
                                             String location,
                                             String company,
                                             Double minSalary,
                                             Double maxSalary,
                                             String active,
                                             String sortBy,
                                             String sortOrder) {

        Optional<Integer> locationId = filteringHelper.getLocationFilter(location);
        Optional<Integer> companyId = getCompanyFilter(company);
        Optional<Boolean> isActive = getActivityStatus(active);

        JobFilterOptions filterOptions = new JobFilterOptions(name,
                description,
                locationId.orElse(null),
                companyId.orElse(null),
                minSalary,
                maxSalary,
                isActive.orElse(null),
                sortBy,
                sortOrder);

        return jobOpeningRepository.filter(filterOptions);
    }

    @Override
    public List<JobOpening> searchJobOpening(JobFilterOptions filterOptions) {
        return jobOpeningRepository.filter(filterOptions);
    }

    @Override
    public List<Application> getPotentialMatchingApplications(Employee authenticated, JobOpening jobOpening,
                                                              double salaryThreshold, int skillThreshold) {

        throwIfViewIsAttemptedByEmployeeOfNonmatchingCompany(authenticated, jobOpening);

        throwIfJobOpeningIsInactive(jobOpening);

        throwIfSalaryThresholdIsNotBetween0And99Percent(salaryThreshold);

        throwIfSkillThresholdIsNegative(skillThreshold);

        ApplicationFilterOptions filterOptions = createApplicationFilterObject(jobOpening, salaryThreshold);

        List<Application> applications = applicationRepository.filter(filterOptions);

        return applications.stream().filter(application -> filteringHelper.checkForMatchingSkills(
                        application.getSkillSet(),
                        jobOpening.getSkillSet(),
                        skillThreshold))
                .collect(Collectors.toList());
    }

    @Override
    public JobOpening getById(int id) {
        Optional<JobOpening> jobOpening = jobOpeningRepository.findById(id);
        if (jobOpening.isEmpty()) {
            throw new EntityNotFoundException("Job opening", id);
        }
        return jobOpening.get();
    }

    @Override
    public JobOpening getByIdAndCompany(int id, Company company) {
        JobOpening jobOpening = getById(id);
        if (!jobOpening.getCompany().equals(company)) {
            throw new UnauthorizedOperationException(JOB_OPENING_AND_COMPANY_MATCH_ERROR);
        }
        return jobOpening;
    }

    @Override
    public JobOpening create(Employee employee, JobOpening jobOpening) {
        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(employee, jobOpening);
        jobOpening.setActive(true);
        return jobOpeningRepository.save(jobOpening);
    }

    @Override
    public JobOpening update(Employee employee, JobOpening jobOpening, int id) {

        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(employee, jobOpening);

        JobOpening openingToUpdate = getById(id);

        throwIfOriginalJobOpeningBelongsToAnotherCompany(jobOpening, openingToUpdate);

        jobOpening.setId(id);
        jobOpening.setPublishedOn(openingToUpdate.getPublishedOn());

        return jobOpeningRepository.save(jobOpening);
    }

    @Override
    public void addOrModifySkill(Employee authenticated, JobOpening jobOpening, Skill skill, SkillLevel level) {

        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(authenticated, jobOpening);

        skillService.addOrModifySkill(jobOpening, skill, level);
    }

    @Override
    public void removeSkill(Employee authenticated, JobOpening jobOpening, Skill skill) {

        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(authenticated, jobOpening);

        skillService.removeSkill(jobOpening, skill);
    }

    @Override
    public void deleteCompanyOpening(Employee employeeAuth, int id) {
        JobOpening jobOpening = getById(id);

        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(employeeAuth, jobOpening);

        jobOpeningRepository.delete(jobOpening);
    }

    private void throwIfSkillThresholdIsNegative(int skillThreshold) {
        if (skillThreshold < 0) {
            throw new IllegalArgumentException("Matching skills threshold cannot be negative.");
        }
    }

    private Optional<Integer> getCompanyFilter(String company) {
        try {
            return Optional.of(companyService.getByName(company).getId());
        } catch (EntityNotFoundException e) {
            return Optional.empty();
        }
    }

    private ApplicationFilterOptions createApplicationFilterObject(JobOpening jobOpening, double salaryThreshold) {
        return new ApplicationFilterOptions(
                null,
                null,
                jobOpening.getLocation().getId(),
                jobOpening.getMinSalary() * (1 - (salaryThreshold / 100)),
                jobOpening.getMaxSalary() * (1 + (salaryThreshold / 100)),
                "name",
                null);
    }

    private void throwIfViewIsAttemptedByEmployeeOfNonmatchingCompany(Employee authenticated, JobOpening jobOpening) {
        if (!authenticated.getCompany().equals(jobOpening.getCompany())) {
            throw new UnauthorizedOperationException("Employees cannot view potential matches of other companies' jobs");
        }
    }

    private void throwIfSalaryThresholdIsNotBetween0And99Percent(double salaryThreshold) {
        if (salaryThreshold < 0 || salaryThreshold > 99) {
            throw new IllegalArgumentException("Salary threshold must be between 0 and 99%.");
        }
    }

    private void throwIfJobOpeningIsInactive(JobOpening jobOpening) {
        if (!jobOpening.isActive()) {
            throw new IllegalStateException("Job opening is archived!");
        }
    }

    private void throwIfOriginalJobOpeningBelongsToAnotherCompany(JobOpening jobOpening, JobOpening openingToUpdate) {
        if (!jobOpening.getCompany().equals(openingToUpdate.getCompany())) {
            throw new UnauthorizedOperationException(JOB_OPENING_AND_COMPANY_MATCH_ERROR);
        }
    }

    private void throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(Employee employee, JobOpening jobOpening) {
        if (!employee.getCompany().equals(jobOpening.getCompany())) {
            throw new UnauthorizedOperationException(JOB_OPENING_AND_COMPANY_MATCH_ERROR);
        }
    }

    private Optional<Boolean> getActivityStatus(String active) {
        if (active == null || active.isEmpty()) {
            return Optional.empty();
        }
        switch (active.toLowerCase()) {
            case "active":
                return Optional.of(true);
            case "archived":
                return Optional.of(false);
            default:
                throw new IllegalArgumentException("No such job opening status!");
        }
    }
}
















