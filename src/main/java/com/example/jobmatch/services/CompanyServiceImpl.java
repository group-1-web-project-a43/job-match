package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.models.Account;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.repositories.interfaces.CompanyRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CompanyServiceImpl implements CompanyService {

    public static final String COMPANY_DELETE_AUTHORIZATION_ERROR = "Not authorized to delete other companies.";
    public static final String ACCOUNT_EDIT_AUTHORIZATION_ERROR = "Cannot edit other companies' accounts.";

    private final AccountRepository accountRepository;
    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyServiceImpl(AccountRepository accountRepository, CompanyRepository companyRepository) {
        this.accountRepository = accountRepository;
        this.companyRepository = companyRepository;
    }

    private static void maintainRelationshipsWithCompanyRelations(Company company, Company companyToUpdate) {
        companyToUpdate.getCompanyInfo().ifPresent(company::setCompanyInfo);
        company.setJobOpenings(companyToUpdate.getJobOpenings());
    }

    @Override
    public List<Company> getAll() {
        return StreamSupport
                .stream(companyRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Page<Company> findAll(int pageNumber) {
        if (pageNumber < 1) throw new IllegalArgumentException("Page index must not be less than zero");
        Pageable pageable = PageRequest.of(pageNumber - 1, 9);
        return companyRepository.findAll(pageable);
    }

    @Override
    public List<Company> searchCompanyByName(Optional<String> keyword,
                                             Optional<String> filterBy,
                                             Optional<String> sortBy,
                                             Optional<Boolean> desc,
                                             Optional<Integer> page,
                                             Optional<Integer> size) {

        Pageable pageable = sortByAndPagination(sortBy, desc, page, size);
        return search(keyword, pageable);
    }

    @Override
    public Company getById(int id) {
        Optional<Company> company = companyRepository.findById(id);
        if (company.isEmpty()) {
            throw new EntityNotFoundException("Company", id);
        }
        return company.get();
    }

    @Override
    public Company getByName(String name) {
        Optional<Company> company = companyRepository.getCompanyByNameEquals(name);
        if (company.isEmpty()) {
            throw new EntityNotFoundException("Company", "name", name);
        }
        return company.get();
    }

    @Override
    public Optional<Company> getByUsername(String username) {
        return companyRepository.getCompanyByUsernameEqualsIgnoreCase(username);
    }

    @Override
    public Company create(Company company) {

        throwIfUsernameOrEmailAlreadyTaken(company);

        throwIfCompanyWithSameNameAlreadyExists(company);

        return companyRepository.save(company);
    }

    @Override
    public Company update(Company companyAuth, Company company, int id) {
        Company companyToUpdate = getById(id);
        if (!companyAuth.equals(companyToUpdate)) {
            throw new UnauthorizedOperationException(ACCOUNT_EDIT_AUTHORIZATION_ERROR);
        }

        Optional<Account> duplicateEmailCheck = accountRepository.getAccountByEmailEquals(company.getEmail());
        if (duplicateEmailCheck.isPresent() && duplicateEmailCheck.get().getId() != id) {
            throw new DuplicateEntityException("Account", "email");
        }

        Optional<Company> duplicateNameCheck = companyRepository.getCompanyByNameEquals(company.getName());
        if (duplicateNameCheck.isPresent() && duplicateNameCheck.get().getId() != id) {
            throw new DuplicateEntityException("Company", "name");
        }

        company.setId(id);
        maintainRelationshipsWithCompanyRelations(company, companyToUpdate);


        return companyRepository.save(company);
    }

    @Override
    public void changePassword(Company companyAuthenticated, PasswordDto passwordDto, int id) {
        Company company = getById(id);

        throwIfCompanyIsNotAuthenticated(companyAuthenticated, company);

        throwIfPasswordMismatchesOldPassword(companyAuthenticated, passwordDto);

        throwIfOldPasswordMatchesNewPassword(passwordDto);

        throwIfNewPasswordAndPasswordConfirmMismatch(passwordDto);

        company.setPassword(passwordDto.getNewPassword());
        companyRepository.save(company);
    }

    @Override
    public void delete(Company company, int id) {
        Company companyToDelete = getById(id);
        if (!companyToDelete.equals(company)) {
            throw new UnauthorizedOperationException(COMPANY_DELETE_AUTHORIZATION_ERROR);
        }

        companyRepository.delete(companyToDelete);
    }

    private List<Company> search(Optional<String> keyword, Pageable pageable) {
        if (keyword.isEmpty()) {
            throw new EmptySearchException("Search");
        }
        return companyRepository.searchCompanyByName(keyword.get(), pageable);
    }

    private Pageable sortByAndPagination(Optional<String> sortBy, Optional<Boolean> desc, Optional<Integer> page, Optional<Integer> size) {
        int pageOrDefault = page.orElse(0);
        int sizeOrDefault = size.orElse(10);
        String sort = sortBy.orElse("name");
        boolean descOrder = desc.orElse(false);
        return PageRequest.of(pageOrDefault, sizeOrDefault, descOrder
                ? Sort.by(sort).descending() : Sort.by(sort));
    }

    private void throwIfCompanyWithSameNameAlreadyExists(Company company) {
        if (companyRepository.existsCompanyByNameEquals(company.getName())) {
            throw new DuplicateEntityException("Company", "name");
        }
    }

    private void throwIfUsernameOrEmailAlreadyTaken(Company company) {
        if (accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                company.getUsername(),
                company.getEmail())) {
            throw new DuplicateEntityException("Account", "username or email");
        }
    }

    private void throwIfNewPasswordAndPasswordConfirmMismatch(PasswordDto passwordDto) {
        if (!passwordDto.getNewPassword().equals(passwordDto.getNewPasswordConfirmation())) {
            throw new PasswordMismatchException("New password", "New password confirm");
        }
    }

    private void throwIfCompanyIsNotAuthenticated(Company authenticated, Company company) {
        if (!authenticated.equals(company)) {
            throw new UnauthorizedOperationException("Companies can only change their own password.");
        }
    }

    private void throwIfPasswordMismatchesOldPassword(Company authenticated, PasswordDto passwordDto) {
        if (!authenticated.getPassword().equals(passwordDto.getOldPassword())) {
            throw new PasswordMismatchException("current password");
        }
    }

    private void throwIfOldPasswordMatchesNewPassword(PasswordDto passwordDto) {
        if (passwordDto.getOldPassword().equals(passwordDto.getNewPassword())) {
            throw new PasswordDuplicateException("Old password", "New password");
        }
    }
}
