package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.repositories.interfaces.ProfessionalsRepository;
import com.example.jobmatch.services.interfaces.ProfessionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProfessionalServiceImpl implements ProfessionalService {
    public static final String PROFESSIONAL_ACCOUNT_AUTH_ERROR = "Professionals can only edit their own accounts.";
    public static final String PROFESSIONAL_DELETE_AUTHORIZATION_ERROR = "Professionals can only delete their own profiles.";

    private final ProfessionalsRepository professionalsRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public ProfessionalServiceImpl(ProfessionalsRepository professionalsRepository,
                                   AccountRepository accountRepository) {
        this.professionalsRepository = professionalsRepository;
        this.accountRepository = accountRepository;
    }

    private static void maintainProfessionalCompositionRelationships(Professional professional, Professional professionalToUpdate) {
        professional.setJobOffers(professionalToUpdate.getJobOffers());
        professionalToUpdate.getMainApplication().ifPresent(professional::setMainApplication);
        professionalToUpdate.getProfessionalInfo().ifPresent(professional::setProfessionalInfo);
        professional.setApplications(professionalToUpdate.getApplications());
    }

    @Override
    public List<Professional> getAll() {
        return StreamSupport
                .stream(professionalsRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }


    @Override
    public List<Professional> searchProfessionalByName(Optional<String> keyword,
                                                       Optional<String> sortBy,
                                                       Optional<Boolean> desc,
                                                       Optional<Integer> page,
                                                       Optional<Integer> size) {
        int pageOrDefault = page.orElse(0);
        int sizeOrDefault = size.orElse(10);
        String sort = sortBy.orElse("name");
        boolean descOrder = desc.orElse(false);
        Pageable pageable = PageRequest.of(pageOrDefault, sizeOrDefault, descOrder
                ? Sort.by(sort).descending() : Sort.by(sort));

        if (keyword.isEmpty()) {
            throw new EmptySearchException("Search");
        }
        return professionalsRepository.findProfessionalByName(keyword.get());
    }

    @Override
    public Professional getById(int id) {
        Optional<Professional> professional = professionalsRepository.findById(id);
        if (professional.isEmpty()) {
            throw new EntityNotFoundException("Professional", id);
        }
        return professional.get();
    }

    @Override
    public Professional getByUsername(String username) {
        Optional<Professional> professional =
                professionalsRepository.findProfessionalByUsernameEqualsIgnoreCase(username);
        if (professional.isEmpty()) {
            throw new EntityNotFoundException("Professional", "username", username);
        }
        return professional.get();
    }

    @Override
    public Set<Application> getAllProfessionalApplications(Professional profAuth, int profId) {
        Professional professional = getById(profId);

        if (!profAuth.equals(professional)) {
            throw new UnauthorizedOperationException("Professionals can only view their own applications");
        }
        return professional.getApplications();
    }

    @Override
    public Professional create(Professional professional) {

        throwIfDuplicateProfessionalUsernameOrEmail(professional);

        return professionalsRepository.save(professional);
    }

    @Override
    public Professional update(Professional authenticated, Professional professional, int id) {
        Professional professionalToUpdate = getById(id);

        if (!authenticated.equals(professionalToUpdate)) {
            throw new UnauthorizedOperationException(PROFESSIONAL_ACCOUNT_AUTH_ERROR);
        }

        if (accountRepository.existsAccountByEmailAndIdNot(professional.getEmail(), id)) {
            throw new DuplicateEntityException("Account", "email");
        }

        professional.setId(professionalToUpdate.getId());

        maintainProfessionalCompositionRelationships(professional, professionalToUpdate);

        return professionalsRepository.save(professional);
    }

    @Override
    public void setMainApplication(Professional authenticated, Application application, Professional professional) {

        throwIfProfessionalIsNotAuthenticated(authenticated, professional);

        throwIfProfessionalsApplicationDontContainApplication(application, professional);

        throwIfApplicationIsAlreadySetToMain(application, professional);

        professional.setMainApplication(application);
        professionalsRepository.save(professional);
    }


    @Override
    public void changePassword(Professional authenticated, PasswordDto passwordDto, int id) {
        Professional professional = getById(id);

        throwIfProfessionalIsNotAuthenticated(authenticated, professional);

        throwIfPasswordMismatchesOldPassword(authenticated, passwordDto);

        throwIfOldPasswordMatchesNewPassword(passwordDto);

        throwIfNewPasswordAndPasswordConfirmMismatch(passwordDto);

        professional.setPassword(passwordDto.getNewPassword());
        professionalsRepository.save(professional);
    }

    @Override
    public void delete(Professional authenticated, int id) {
        Professional professional = getById(id);
        if (!authenticated.equals(professional)) {
            throw new UnauthorizedOperationException(PROFESSIONAL_DELETE_AUTHORIZATION_ERROR);
        }
        professionalsRepository.delete(professional);
    }

    private void throwIfApplicationIsAlreadySetToMain(Application application, Professional professional) {
        if (professional.getMainApplication().isPresent() &&
                professional.getMainApplication().get().equals(application)) {
            throw new DuplicateEntityException("Main application of professional", "application");
        }
    }

    private void throwIfDuplicateProfessionalUsernameOrEmail(Professional professional) {
        if (accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                professional.getUsername(),
                professional.getEmail())) {
            throw new DuplicateEntityException("Account", "username or email");
        }
    }


    private void throwIfNewPasswordAndPasswordConfirmMismatch(PasswordDto passwordDto) {
        if (!passwordDto.getNewPassword().equals(passwordDto.getNewPasswordConfirmation())) {
            throw new PasswordMismatchException("New password", "New password confirm");
        }
    }

    private void throwIfProfessionalsApplicationDontContainApplication(Application application, Professional professional) {
        if (!professional.getApplications().contains(application)) {
            throw new ApplicationMismatchException("application", "professional");
        }
    }

    private void throwIfProfessionalIsNotAuthenticated(Professional authenticated, Professional professional) {
        if (!authenticated.equals(professional)) {
            throw new UnauthorizedOperationException("Professionals can only change their own password.");
        }
    }

    private void throwIfPasswordMismatchesOldPassword(Professional authenticated, PasswordDto passwordDto) {
        if (!authenticated.getPassword().equals(passwordDto.getOldPassword())) {
            throw new PasswordMismatchException("current password");
        }
    }

    private void throwIfOldPasswordMatchesNewPassword(PasswordDto passwordDto) {
        if (passwordDto.getOldPassword().equals(passwordDto.getNewPassword())) {
            throw new PasswordDuplicateException("Old password", "New password");
        }
    }


    @Override
    public int getAllClosedContracts() {
        return professionalsRepository.countProfessionalsByJobOffersIsTrue();
    }

}



