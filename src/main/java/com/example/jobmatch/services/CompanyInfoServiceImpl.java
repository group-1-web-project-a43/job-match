package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.CompanyInfo;
import com.example.jobmatch.repositories.interfaces.CompanyInfoRepository;
import com.example.jobmatch.services.interfaces.CompanyInfoService;
import com.example.jobmatch.services.interfaces.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CompanyInfoServiceImpl implements CompanyInfoService {

    private static final String INVALID_COMPANY_AUTHORIZATION = "Cannot create or edit info of other companies";

    private final CompanyInfoRepository companyInfoRepository;
    private final CompanyService companyService;

    @Autowired
    public CompanyInfoServiceImpl(CompanyInfoRepository companyInfoRepository, CompanyService companyService) {
        this.companyInfoRepository = companyInfoRepository;
        this.companyService = companyService;
    }

    @Override
    public List<CompanyInfo> getAll() {
        return StreamSupport
                .stream(companyInfoRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public CompanyInfo getById(int id) {
        Optional<CompanyInfo> companyInfo = companyInfoRepository.findById(id);
        if (companyInfo.isEmpty()) {
            throw new EntityNotFoundException("Company Info", id);
        }
        return companyInfo.get();
    }

    @Override
    public CompanyInfo getInfoByCompanyId(int companyId) {
        Company company = companyService.getById(companyId);
        Optional<CompanyInfo> companyInfo = companyInfoRepository.findCompanyInfoByCompanyEquals(company);
        if (companyInfo.isEmpty()) {
            throw new EntityNotFoundException("Company info", "company id", Integer.toString(companyId));
        }
        return companyInfo.get();
    }

    @Override
    public CompanyInfo create(Company company, CompanyInfo companyInfo) {
        if (!company.equals(companyInfo.getCompany())) {
            throw new UnauthorizedOperationException(INVALID_COMPANY_AUTHORIZATION);
        }
        if (companyInfoRepository.findCompanyInfoByCompanyEquals(companyInfo.getCompany()).isPresent()) {
            throw new DuplicateEntityException("Company Info", "Company");
        }

        companyInfo.setCompany(company);
        return companyInfoRepository.save(companyInfo);
    }

    @Override
    public CompanyInfo update(Company companyAuth, int companyId, CompanyInfo companyInfo) {
        CompanyInfo companyInfoToUpdate = getInfoByCompanyId(companyId);
        if (!companyInfoToUpdate.getCompany().equals(companyAuth)) {
            throw new UnauthorizedOperationException(INVALID_COMPANY_AUTHORIZATION);
        }
        companyInfo.setId(companyInfoToUpdate.getId());
        companyInfo.setCompany(companyInfoToUpdate.getCompany());
        return companyInfoRepository.save(companyInfo);
    }

    @Override
    public void delete(Company company, int companyId) {

        CompanyInfo companyInfoToDelete = getInfoByCompanyId(companyId);
        companyInfoRepository.delete(companyInfoToDelete);
    }
}
