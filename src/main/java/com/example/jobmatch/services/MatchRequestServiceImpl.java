package com.example.jobmatch.services;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.exceptions.ApplicationMismatchException;
import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.*;
import com.example.jobmatch.repositories.interfaces.MatchRequestRepository;
import com.example.jobmatch.services.interfaces.MatchRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.lang.String.format;

@Service
public class MatchRequestServiceImpl implements MatchRequestService {

    private static final String MATCH_REQUEST_VIEW_AUTHORIZATION_ERROR =
            "Can only view applications to openings of one's own company";

    private static final String PROFESSIONAL_BUSY_ERROR =
            "Busy professionals cannot apply to job openings.";

    private static final String JOB_OPENING_EDIT_AUTHORIZATION_ERROR =
            "Not authorized to edit other companies' job openings.";
    private static final String JOB_OPENING_AND_COMPANY_MATCH_ERROR =
            "Job Opening and posting company don't match.";

    private static final String JOB_OPENING_ALREADY_ARCHIVED =
            "Job opening is already archived!";

    private static final String APPLICATION_ALREADY_MATCHED_ERROR =
            "Application is already matched!";
    private static final String JOB_OFFERS_VIEW_AUTH_ERROR = "Unauthorized operation!";

    private final MatchRequestRepository matchRequestRepository;

    @Autowired
    public MatchRequestServiceImpl(MatchRequestRepository matchRequestRepository) {
        this.matchRequestRepository = matchRequestRepository;
    }

    @Override
    public List<MatchRequest> getByJobOpening(Employee employee, JobOpening jobOpening) {
        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(employee, jobOpening);
        Iterable<MatchRequest> requests = matchRequestRepository.findMatchRequestsByJobOpeningEquals(jobOpening);
        return StreamSupport
                .stream(requests.spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public List<MatchRequest> getByProfessionalApplication(Professional professional) {
        Iterable<MatchRequest> requests = matchRequestRepository
                .findMatchRequestsByApplication_Professional(professional);

        return StreamSupport
                .stream(requests.spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Map<JobOpening, Boolean> existMatchRequestsForJobOpening(Employee authenticated) {
        Map<JobOpening, Boolean> result = new HashMap<>();
        authenticated.getCompany()
                .getJobOpenings()
                .stream()
                .filter(JobOpening::isActive)
                .forEach(jobOpening1 -> result.put(jobOpening1,
                        matchRequestRepository.existsMatchRequestByJobOpeningEquals(jobOpening1)));
        return result;
    }


    public Map<MatchRequest, Boolean> getByProfessional(Professional authenticated, Professional professional) {
        throwIfProfessionalIsUnauthorized(authenticated, professional);
        return professional.getJobOffers();
    }

    @Override
    public MatchRequest getById(Employee employee, int id) {
        MatchRequest matchRequest = get(id);
        throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(employee, matchRequest.getJobOpening());
        return matchRequest;
    }

    @Override
    public MatchRequest create(Professional professional, JobOpening jobOpening, Application application) {
        //Permissions block
        throwIfProfessionalIsUnauthorized(professional, application.getProfessional());

        throwIfJobOpeningIsArchived(jobOpening);

        throwIfProfessionalStatusIsSetToBusy(professional);

        throwIfProfessionalHasAlreadyAppliedToJobOpening(professional, jobOpening);

        throwIfApplicationIsHiddenOrAlreadyMatched(professional, application);

        //application process
        MatchRequest result = new MatchRequest();
        result.setJobOpening(jobOpening);
        result.setApplication(application);
        result.setMatch(false);

        //Persistence
        return matchRequestRepository.save(result);
    }

    @Override
    public MatchRequest approve(Employee employee, Company company, JobOpening jobOpening, int id) {
        //Permissions block
        throwIfEmployeeCompanyNotAMatch(employee, company);

        throwIfJobOpeningCompanyAndCompanyNotMatch(company, jobOpening);

        throwIfJobOpeningIsArchived(jobOpening);

        MatchRequest matchRequest = getById(employee, id);

        throwIfMatchRequestIsAlreadyMatched(matchRequest);

        throwIfApplicationIsHiddenOrAlreadyMatched(matchRequest.getApplication().getProfessional(),
                matchRequest.getApplication());

        //Approval process
        matchRequest.setMatch(true);
        matchRequest.getApplication().getProfessional().getJobOffers().put(matchRequest, false);

        //Persistence
        return matchRequestRepository.save(matchRequest);
    }

    @Transactional
    public void acceptJobOffer(Professional authenticated, Professional professional, int matchId) {
        //Permissions block
        throwIfProfessionalIsUnauthorized(authenticated, professional);

        throwIfProfessionalStatusIsSetToBusy(professional);

        MatchRequest matchRequest = get(matchId);

        throwIfProfessionalJobOffersDoesNotContainEntryWithMatchRequest(professional, matchRequest);

        throwIfProfessionalNotAuthorOfApplicationInMatchRequest(professional, matchRequest);

        throwIfJobOpeningIsArchived(matchRequest.getJobOpening());

        throwIfApplicationIsAlreadyMatched(matchRequest);

        throwIfProfessionalHasAlreadyAcceptedThisJob(professional, matchRequest);

        //Job offer approval process
        professional.getJobOffers().put(matchRequest, true);
        matchRequest.getApplication().setApplicationStatus(ApplicationStatus.MATCHED);
        matchRequest.getJobOpening().setActive(false);
        professional.getProfessionalInfo().get().setActive(false);
        professional.getApplications()
                .stream()
                .filter(application -> !application.getApplicationStatus().equals(ApplicationStatus.MATCHED))
                .forEach(application -> application.setApplicationStatus(ApplicationStatus.HIDDEN));

        //Persistence
        matchRequestRepository.save(matchRequest);
    }

    private void throwIfProfessionalNotAuthorOfApplicationInMatchRequest(Professional professional, MatchRequest matchRequest) {
        if (!professional.equals(matchRequest.getApplication().getProfessional())) {
            throw new ApplicationMismatchException(matchRequest.getApplication().toString(),
                    format("%s %s", professional.getFirstName(), professional.getLastName()));
        }
    }

    private void throwIfProfessionalJobOffersDoesNotContainEntryWithMatchRequest(Professional professional, MatchRequest matchRequest) {
        if (!professional.getJobOffers().containsKey(matchRequest)) {
            throw new EntityNotFoundException("Job offer", matchRequest.getId());
        }
    }

    private MatchRequest get(int id) {
        Optional<MatchRequest> matchRequest = matchRequestRepository.findById(id);
        if (matchRequest.isEmpty()) {
            throw new EntityNotFoundException("Match request", id);
        }
        return matchRequest.get();
    }

    private void throwIfMatchRequestIsAlreadyMatched(MatchRequest matchRequest) {
        if (matchRequest.isMatch()) {
            throw new IllegalArgumentException(APPLICATION_ALREADY_MATCHED_ERROR);
        }
    }

    private void throwIfJobOpeningCompanyAndCompanyNotMatch(Company company, JobOpening jobOpening) {
        if (!jobOpening.getCompany().equals(company)) {
            throw new UnauthorizedOperationException(JOB_OPENING_AND_COMPANY_MATCH_ERROR);
        }
    }

    private void throwIfEmployeeCompanyNotAMatch(Employee employee, Company company) {
        if (!employee.getCompany().equals(company)) {
            throw new UnauthorizedOperationException(JOB_OPENING_EDIT_AUTHORIZATION_ERROR);
        }
    }

    private void throwIfProfessionalHasAlreadyAppliedToJobOpening(Professional professional, JobOpening jobOpening) {
        if (StreamSupport
                .stream(matchRequestRepository.findMatchRequestsByJobOpeningEquals(jobOpening).spliterator(), false)
                .map(MatchRequest::getApplication)
                .anyMatch(application1 -> application1.getProfessional().equals(professional))) {

            throw new DuplicateEntityException("Application", "professional");
        }
    }

    private void throwIfApplicationIsHiddenOrAlreadyMatched(Professional professional, Application application) {
        if (application.getApplicationStatus().equals(ApplicationStatus.HIDDEN) ||
                application.getApplicationStatus().equals(ApplicationStatus.MATCHED)) {

            throw new ApplicationMismatchException(format("%s %s",
                    professional.getFirstName(), professional.getLastName()));
        }
    }

    private void throwIfJobOpeningIsArchived(JobOpening jobOpening) {
        if (!jobOpening.isActive()) {
            throw new IllegalArgumentException(JOB_OPENING_ALREADY_ARCHIVED);
        }
    }

    private void throwIfProfessionalStatusIsSetToBusy(Professional professional) {
        if (!professional.getProfessionalInfo().
                orElseThrow(() -> new EntityNotFoundException("Professional", "professional info", "info"))
                .isActive()) {
            throw new IllegalArgumentException(PROFESSIONAL_BUSY_ERROR);
        }
    }

    private void throwIfEmployeeCompanyAndJobOpeningCompanyNotMatch(Employee employee, JobOpening jobOpening) {
        if (!employee.getCompany().equals(jobOpening.getCompany())) {
            throw new UnauthorizedOperationException(MATCH_REQUEST_VIEW_AUTHORIZATION_ERROR);
        }
    }

    private void throwIfProfessionalIsUnauthorized(Professional authenticated, Professional professional) {
        if (!authenticated.equals(professional)) {
            throw new UnauthorizedOperationException(JOB_OFFERS_VIEW_AUTH_ERROR);
        }
    }


    private void throwIfProfessionalHasAlreadyAcceptedThisJob(Professional professional, MatchRequest matchRequest) {
        if (professional.getJobOffers().get(matchRequest)) {
            throw new IllegalArgumentException("You have already accepted this job.");
        }
    }

    private void throwIfApplicationIsAlreadyMatched(MatchRequest matchRequest) {
        if (matchRequest.getApplication().getApplicationStatus().equals(ApplicationStatus.MATCHED)) {
            throw new IllegalArgumentException("Application is already matched.");
        }
    }
}
