package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.ProfessionalInfo;
import com.example.jobmatch.repositories.interfaces.ProfessionalInfoRepository;
import com.example.jobmatch.services.interfaces.ProfessionalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ProfessionalInfoServiceImpl implements ProfessionalInfoService {

    public static final String PROFESSIONAL_INFO_AUTHORIZATION_ERROR = "Cannot create or edit other professionals' info.";
    public static final String INFO_DELETE_AUTHORIZATION_ERROR = "Professionals cannot delete info of other professionals.";

    private final ProfessionalInfoRepository professionalInfoRepository;

    @Autowired
    public ProfessionalInfoServiceImpl(ProfessionalInfoRepository professionalInfoRepository) {
        this.professionalInfoRepository = professionalInfoRepository;
    }

    @Override
    public ProfessionalInfo getById(int id) {
        Optional<ProfessionalInfo> professionalInfo = professionalInfoRepository.findById(id);
        if (professionalInfo.isEmpty()) {
            throw new EntityNotFoundException("ProfessionalInfo", id);
        }
        return professionalInfo.get();
    }

    @Override
    public ProfessionalInfo create(Professional profAuth, Professional professional, ProfessionalInfo professionalInfo) {
        if (!profAuth.equals(professional)) {
            throw new UnauthorizedOperationException(PROFESSIONAL_INFO_AUTHORIZATION_ERROR);
        }
        professionalInfo.setProfessional(professional);
        return professionalInfoRepository.save(professionalInfo);
    }

    @Override
    public ProfessionalInfo update(Professional professional, ProfessionalInfo professionalInfo, int id) {
        if (professional.getId() != (professionalInfo.getProfessional().getId())) {
            throw new UnauthorizedOperationException(PROFESSIONAL_INFO_AUTHORIZATION_ERROR);
        }
        ProfessionalInfo professionalInfoToUpdate = professional.getProfessionalInfo()
                .orElseThrow(() -> new EntityNotFoundException(
                        "Professional info", "professional",
                        professional.getUsername()));

        professionalInfo.setProfessional(professional);
        professionalInfo.setId(professionalInfoToUpdate.getId());

        return professionalInfoRepository.save(professionalInfo);
    }

    @Override
    public void delete(Professional professional, int id) {
        ProfessionalInfo professionalInfo = getById(id);
        if (!professionalInfo.getProfessional().equals(professional)) {
            throw new UnauthorizedOperationException(INFO_DELETE_AUTHORIZATION_ERROR);
        }
        professionalInfoRepository.delete(professionalInfo);
    }


}
