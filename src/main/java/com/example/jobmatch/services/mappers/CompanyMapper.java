package com.example.jobmatch.services.mappers;

import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.CompanyInfo;
import com.example.jobmatch.models.Employee;
import com.example.jobmatch.models.dtos.RegisterCompanyDto;
import com.example.jobmatch.models.dtos.dtosIn.CompanyDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.CompanyInfoDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.EmployeeDtoIn;
import com.example.jobmatch.models.dtos.dtosOut.CompanyDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.CompanyInfoDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.EmployeeDtoOut;
import org.springframework.stereotype.Component;


@Component
public class CompanyMapper {


    public Company dtoToObject(CompanyDtoIn companyDtoIn) {
        Company company = new Company();

        company.setUsername(companyDtoIn.getUsername());
        company.setPassword(companyDtoIn.getPassword());
        company.setEmail(companyDtoIn.getEmail());
        company.setName(companyDtoIn.getName());

        return company;
    }

    public CompanyDtoOut objectToDto(Company company) {
        CompanyDtoOut companyDtoOut = new CompanyDtoOut();
        companyDtoOut.setCompanyName(company.getName());
        return companyDtoOut;
    }

    public CompanyInfo infoDtoToObject(CompanyInfoDtoIn infoDtoIn) {
        CompanyInfo companyInfo = new CompanyInfo();

        if (infoDtoIn.getPictureUrl() == null || infoDtoIn.getPictureUrl().isEmpty()) {
            companyInfo.setPictureUrl("/images/user-photos/default.png");
        } else {
            companyInfo.setPictureUrl(infoDtoIn.getPictureUrl());
        }
        companyInfo.setDescription(infoDtoIn.getDescription());
        companyInfo.setLocation(infoDtoIn.getLocation());
        companyInfo.setContacts(infoDtoIn.getContacts());

        return companyInfo;

    }

    public CompanyInfoDtoIn companyInfoDtoMvc(CompanyInfo companyInfo) {
        CompanyInfoDtoIn companyInfoDtoIn = new CompanyInfoDtoIn();
        companyInfoDtoIn.setDescription(companyInfo.getDescription());
        companyInfoDtoIn.setPictureUrl(companyInfo.getPictureUrl());
        companyInfoDtoIn.setContacts(companyInfo.getContacts());
        companyInfoDtoIn.setLocation(companyInfo.getLocation());
        return companyInfoDtoIn;
    }

    public CompanyInfoDtoOut infoObjectToDto(CompanyInfo companyInfo) {
        CompanyInfoDtoOut dtoOut = new CompanyInfoDtoOut();

        dtoOut.setCompanyName(companyInfo.getCompany().getName());

        dtoOut.setLocationName(companyInfo.getLocation().toString());
        dtoOut.setDescription(companyInfo.getDescription());
        dtoOut.setContacts(companyInfo.getContacts());

        return dtoOut;
    }

    public Employee employeeDtoToObject(EmployeeDtoIn employeeDtoIn) {
        Employee employee = new Employee();

        employee.setUsername(employeeDtoIn.getUsername());
        employee.setPassword(employeeDtoIn.getPassword());
        employee.setEmail(employeeDtoIn.getEmail());
        employee.setName(employeeDtoIn.getName());

        return employee;
    }

    public EmployeeDtoIn employeeDtoMvc(Employee employee) {
        EmployeeDtoIn employeeDtoIn = new EmployeeDtoIn();
        employeeDtoIn.setName(employee.getName());
        employeeDtoIn.setEmail(employee.getEmail());
        employeeDtoIn.setPassword(employee.getPassword());
        employeeDtoIn.setUsername(employee.getUsername());
        return employeeDtoIn;
    }

    public EmployeeDtoOut employeeObjectToDto(Employee employee) {
        EmployeeDtoOut dtoOut = new EmployeeDtoOut();
        dtoOut.setName(employee.getName());
        return dtoOut;
    }

    public Company fromDto(RegisterCompanyDto registerDto) {
        Company company = new Company();
        company.setUsername(registerDto.getUsername());
        company.setPassword(registerDto.getPassword());
        company.setEmail(registerDto.getEmail());
        company.setName(registerDto.getCompanyName());
        return company;
    }
}
