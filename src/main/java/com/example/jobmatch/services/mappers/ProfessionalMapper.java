package com.example.jobmatch.services.mappers;

import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.MatchRequest;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.ProfessionalInfo;
import com.example.jobmatch.models.dtos.RegisterProfessionalDto;
import com.example.jobmatch.models.dtos.dtosIn.ProfessionalDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.ProfessionalInfoDtoIn;
import com.example.jobmatch.models.dtos.dtosOut.ProfessionalDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.ProfessionalInfoDtoOut;
import com.example.jobmatch.services.interfaces.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

@Component
public class ProfessionalMapper {
    private final LocationService locationService;

    @Autowired
    public ProfessionalMapper(LocationService locationService) {
        this.locationService = locationService;
    }

    public Professional professionalDtoToObject(ProfessionalDtoIn professionalDtoIn) {
        Professional professional = new Professional();
        professional.setUsername(professionalDtoIn.getUsername());
        professional.setPassword(professionalDtoIn.getPassword());
        professional.setEmail(professionalDtoIn.getEmail());
        professional.setFirstName(professionalDtoIn.getFirstName());
        professional.setLastName(professionalDtoIn.getLastName());
        return professional;
    }

    public ProfessionalInfo professionalInfoDtoToObject(ProfessionalInfoDtoIn professionalInfoDtoIn) {
        ProfessionalInfo professionalInfo = new ProfessionalInfo();
        professionalInfo.setDescription(professionalInfoDtoIn.getDescription());

        if (professionalInfoDtoIn.getPhotoUrl() == null || professionalInfoDtoIn.getPhotoUrl().isEmpty()) {
            professionalInfo.setPictureUrl("/images/user-photos/default.png");
        } else {
            professionalInfo.setPictureUrl(professionalInfoDtoIn.getPhotoUrl());
        }
        professionalInfo.setLocation(professionalInfoDtoIn.getLocation());
        professionalInfo.setActive(professionalInfoDtoIn.isActive());
        return professionalInfo;
    }

    public ProfessionalInfoDtoIn professionalInfoDtoMvc(ProfessionalInfo professionalInfo) {
        ProfessionalInfoDtoIn professionalInfoDtoIn = new ProfessionalInfoDtoIn();
        professionalInfoDtoIn.setPhotoUrl(professionalInfo.getPictureUrl());
        professionalInfoDtoIn.setActive(professionalInfo.isActive());
        professionalInfoDtoIn.setDescription(professionalInfo.getDescription());
        professionalInfoDtoIn.setLocation(professionalInfo.getLocation());
        return professionalInfoDtoIn;
    }

    public ProfessionalDtoOut professionalObjectToDto(Professional professional) {
        ProfessionalDtoOut professionalDtoOut = new ProfessionalDtoOut();
        professionalDtoOut.setFullName((format("%s %s", professional.getFirstName(), professional.getLastName())));
        return professionalDtoOut;
    }

    public ProfessionalInfoDtoOut professionalInfoObjectToDto(ProfessionalInfo professionalInfo) {
        ProfessionalInfoDtoOut professionalDtoOut = new ProfessionalInfoDtoOut();
        professionalDtoOut.setDescription(professionalInfo.getDescription());
        professionalDtoOut.setPhotoUrl(professionalInfo.getPictureUrl());
        professionalDtoOut.setCity(professionalInfo.getLocation().getCity());
        professionalDtoOut.setCountry(professionalInfo.getLocation().getCountry());
        professionalDtoOut.setProfessionalFullName((format("%s %s",
                professionalInfo.getProfessional().getFirstName(),
                professionalInfo.getProfessional().getLastName())));
        professionalDtoOut.setActive(professionalInfo.isActive());
        professionalDtoOut.setJobOffers(mapJobOffers(professionalInfo.getProfessional().getJobOffers()));
        return professionalDtoOut;
    }

    private Map<JobOpening, Boolean> mapJobOffers(Map<MatchRequest, Boolean> jobOffers) {
        Map<JobOpening, Boolean> result = new HashMap<>();
        for (Map.Entry<MatchRequest, Boolean> entry : jobOffers.entrySet()) {
            result.put(entry.getKey().getJobOpening(), entry.getValue());
        }
        return result;
    }


    public Professional fromDto(RegisterProfessionalDto registerProfessionalDto) {
        Professional professional = new Professional();
        professional.setUsername(registerProfessionalDto.getUsername());
        professional.setPassword(registerProfessionalDto.getPassword());
        professional.setEmail(registerProfessionalDto.getEmail());
        professional.setFirstName(registerProfessionalDto.getFirstName());
        professional.setLastName(registerProfessionalDto.getLastName());
        return professional;
    }

}
