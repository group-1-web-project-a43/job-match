package com.example.jobmatch.services.mappers;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.dtos.dtosIn.ApplicationDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.JobOpeningDtoIn;
import com.example.jobmatch.models.dtos.dtosOut.ApplicationDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.JobOpeningDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.MatchRequestDtoOut;
import com.example.jobmatch.services.interfaces.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

@Component
public class JobFormMapper {

    private static final String MAX_SMALLER_THAN_MIN_ERROR = "Max salary cannot be smaller than min salary.";

    private final SkillService skillService;

    @Autowired
    public JobFormMapper(SkillService skillService) {
        this.skillService = skillService;
    }

    public MatchRequestDtoOut matchObjectToDto(MatchRequest matchRequest) {
        MatchRequestDtoOut dtoOut = new MatchRequestDtoOut();
        dtoOut.setJobOpening(objectToDto(matchRequest.getJobOpening()));
        dtoOut.setApplication(applicationObjectToDto(matchRequest.getApplication()));
        dtoOut.setIs_match(matchRequest.isMatch());
        return dtoOut;
    }

    public JobOpening dtoToObject(JobOpeningDtoIn jobOpeningDtoIn) {

        JobOpening jobOpening = new JobOpening();
        jobOpening.setName(jobOpeningDtoIn.getName());
        checkMaxIsBiggerThanMin(jobOpeningDtoIn.getMinSalary(), jobOpeningDtoIn.getMaxSalary());
        jobOpening.setMinSalary(jobOpeningDtoIn.getMinSalary());
        jobOpening.setMaxSalary(jobOpeningDtoIn.getMaxSalary());

        checkSalaryForMinBiggerThanMax(jobOpening);

        jobOpening.setDescription(jobOpeningDtoIn.getDescription());
        jobOpening.setLocation(jobOpeningDtoIn.getLocation());
        jobOpening.setSkillSet(mapSkillSet(jobOpeningDtoIn.getSkillSet()));
        jobOpening.setRemote(jobOpeningDtoIn.isRemote());
        jobOpening.setActive(jobOpeningDtoIn.isActive());

        return jobOpening;
    }

    public JobOpeningDtoOut objectToDto(JobOpening jobOpening) {
        JobOpeningDtoOut dtoOut = new JobOpeningDtoOut();

        dtoOut.setName(jobOpening.getName());
        dtoOut.setCompanyName(jobOpening.getCompany().getName());

        if (jobOpening.isActive()) dtoOut.setStatus("Active");
        else dtoOut.setStatus("Archived");

        dtoOut.setMinSalary(jobOpening.getMinSalary());
        dtoOut.setMaxSalary(jobOpening.getMaxSalary());

        dtoOut.setDescription(jobOpening.getDescription());
        dtoOut.setRemotePossible(jobOpening.isRemote());
        dtoOut.setPublishedOn(jobOpening.getPublishedOn());
        dtoOut.setLocation(jobOpening.getLocation().toString());
        dtoOut.setSkillSet(mapSkillsToDto(jobOpening.getSkillSet()));


        return dtoOut;
    }


    public Application applicationDtoToObject(ApplicationDtoIn applicationDtoIn) {
        Application application = new Application();
        application.setName(applicationDtoIn.getName());
        checkMaxIsBiggerThanMin(applicationDtoIn.getMinSalary(), applicationDtoIn.getMaxSalary());
        application.setMinSalary(applicationDtoIn.getMinSalary());
        application.setMaxSalary(applicationDtoIn.getMaxSalary());

        checkSalaryForMinBiggerThanMax(application);

        application.setDescription(applicationDtoIn.getDescription());
        application.setLocation(applicationDtoIn.getLocation());
        application.setApplicationStatus(parseApplicationStatus(applicationDtoIn.getApplicationStatus().name()));
        return application;
    }

    public ApplicationDtoOut applicationObjectToDto(Application application) {
        ApplicationDtoOut applicationDtoOut = new ApplicationDtoOut();
        applicationDtoOut.setName(application.getName());
        applicationDtoOut.setProfessionalName(format("%s %s",
                application.getProfessional().getFirstName(),
                application.getProfessional().getLastName()));
        applicationDtoOut.setStatus(toStringEnum(application.getApplicationStatus()));
        applicationDtoOut.setMinSalary(application.getMinSalary());
        applicationDtoOut.setMaxSalary(application.getMaxSalary());
        applicationDtoOut.setRemotePossible(application.isRemote());
        applicationDtoOut.setLocation(application.getLocation().toString());
        applicationDtoOut.setDescription(application.getDescription());
        applicationDtoOut.setSkillSet(mapSkillsToDto(application.getSkillSet()));

        return applicationDtoOut;
    }

    public ApplicationDtoIn applicationToDtoInMvc(Application application) {
        ApplicationDtoIn applicationDtoIn = new ApplicationDtoIn();
        applicationDtoIn.setName(application.getName());
        applicationDtoIn.setDescription(application.getDescription());
        applicationDtoIn.setMinSalary(application.getMinSalary());
        applicationDtoIn.setMaxSalary(application.getMaxSalary());
        applicationDtoIn.setLocation(application.getLocation());
        applicationDtoIn.setApplicationStatus(application.getApplicationStatus());
        return applicationDtoIn;
    }

    public JobOpeningDtoIn jobOpeningToDtoInMvc(JobOpening jobOpening) {
        JobOpeningDtoIn jobOpeningDtoIn = new JobOpeningDtoIn();
        jobOpeningDtoIn.setName(jobOpening.getName());
        jobOpeningDtoIn.setDescription(jobOpening.getDescription());
        jobOpeningDtoIn.setMaxSalary(jobOpening.getMaxSalary());
        jobOpeningDtoIn.setMinSalary(jobOpening.getMinSalary());
        jobOpeningDtoIn.setRemote(jobOpening.isRemote());
        jobOpeningDtoIn.setLocation(jobOpening.getLocation());
        jobOpeningDtoIn.setActive(jobOpening.isActive());
        return jobOpeningDtoIn;
    }


    private void checkMaxIsBiggerThanMin(double minSalary, double maxSalary) {
        if (maxSalary < minSalary) {
            throw new IllegalArgumentException("Min salary cannot be greater than max");
        }
    }

    private <T extends JobForm> void checkSalaryForMinBiggerThanMax(T jobForm) {
        if (jobForm.getMaxSalary() < jobForm.getMinSalary()) {
            throw new IllegalArgumentException(MAX_SMALLER_THAN_MIN_ERROR);
        }
    }

    private Map<Skill, SkillLevel> mapSkillSet(Map<String, String> skillSet) {
        Map<Skill, SkillLevel> result = new HashMap<>();
        for (Map.Entry<String, String> entry : skillSet.entrySet()) {
            result.put(skillService.getByName(entry.getKey()), parseSkillLevel(entry.getValue()));
        }
        return result;
    }

    private Map<String, SkillLevel> mapSkillsToDto(Map<Skill, SkillLevel> skillSet) {
        Map<String, SkillLevel> result = new HashMap<>();
        for (Map.Entry<Skill, SkillLevel> entry : skillSet.entrySet()) {
            result.put(entry.getKey().getSkillName(), entry.getValue());
        }
        return result;
    }

    public SkillLevel parseSkillLevel(String enumStr) {
        try {
            return SkillLevel.valueOf(SkillLevel.class, enumStr.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new EntityNotFoundException("Skill Level", "name", enumStr);
        }
    }

    private ApplicationStatus parseApplicationStatus(String enumApplication) {
        try {
            return ApplicationStatus.valueOf(ApplicationStatus.class, enumApplication.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new EntityNotFoundException("Application status", "name", enumApplication);
        }
    }

    private String toStringEnum(ApplicationStatus applicationStatus) {
        return applicationStatus.toString().toUpperCase();
    }


}
