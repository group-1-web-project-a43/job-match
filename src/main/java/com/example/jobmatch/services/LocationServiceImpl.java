package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.LocationNotExistException;
import com.example.jobmatch.models.Location;
import com.example.jobmatch.repositories.interfaces.LocationRepository;
import com.example.jobmatch.services.interfaces.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
public class LocationServiceImpl implements LocationService {
    private final LocationRepository locationRepository;

    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public List<Location> getAll() {
        return StreamSupport
                .stream(locationRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Location getById(int id) {
        Optional<Location> locationOptional = locationRepository.findById(id);
        if (locationOptional.isEmpty()) {
            throw new EntityNotFoundException("Location", id);
        }
        return locationOptional.get();
    }

    @Override
    public Location create(Location location) {
        if (locationRepository.existsLocationByCityAndCountry(location.getCity(), location.getCountry())) {
            throw new DuplicateEntityException("Location", "country", "city");
        }
        return locationRepository.save(location);
    }

    @Override
    public Location update(Location location, int id) {
        Location locationToUpdate = getById(id);
        if (locationRepository.existsLocationByCityAndCountry(location.getCity(), location.getCountry())) {
            throw new DuplicateEntityException("Location", "country", "city");
        }
        location.setId(locationToUpdate.getId());
        return locationRepository.save(location);
    }

    @Override
    public void delete(int id) {
        locationRepository.deleteById(id);
    }

    @Override
    public Location getByCity(String city) {
        List<Location> locations = StreamSupport
                .stream(locationRepository.findLocationsByCityEquals(city).spliterator(), false)
                .collect(Collectors.toList());

        if (locations.isEmpty()) {
            throw new LocationNotExistException(city);
        }
        return locations.get(0);
    }

    @Override
    public Location getByCountryAndCity(String country, String city) {
        Optional<Location> location = locationRepository.findLocationByCountryAndCity(country, city);
        if (location.isEmpty()) {
            throw new LocationNotExistException(city, country);
        }
        return location.get();
    }
}
