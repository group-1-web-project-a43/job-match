package com.example.jobmatch.services;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.ApplicationMismatchException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.FilteringHelper;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.repositories.interfaces.ApplicationRepository;
import com.example.jobmatch.repositories.interfaces.JobOpeningRepository;
import com.example.jobmatch.services.interfaces.ApplicationService;
import com.example.jobmatch.services.interfaces.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ApplicationServiceImpl implements ApplicationService {
    private static final String PROF_APPLICATION_AUTH_ERROR =
            "Unauthorized action with another professional's application";

    private final ApplicationRepository applicationRepository;
    private final JobOpeningRepository jobOpeningRepository;

    private final SkillService skillService;
    private final FilteringHelper filteringHelper;

    @Autowired
    public ApplicationServiceImpl(ApplicationRepository applicationRepository,
                                  JobOpeningRepository jobOpeningRepository,
                                  SkillService skillService, FilteringHelper filteringHelper) {
        this.applicationRepository = applicationRepository;
        this.jobOpeningRepository = jobOpeningRepository;
        this.skillService = skillService;
        this.filteringHelper = filteringHelper;
    }

    @Override
    public List<Application> getAll() {
        return applicationRepository.findAll();
    }

    @Override
    public List<Application> searchActiveApplications(String name,
                                                      String description,
                                                      String location,
                                                      Double minSalary,
                                                      Double maxSalary,
                                                      String sortBy,
                                                      String sortOrder) {
        Optional<Integer> locationId = filteringHelper.getLocationFilter(location);

        return applicationRepository.filter(new ApplicationFilterOptions(
                name,
                description,
                locationId.orElse(null),
                minSalary,
                maxSalary,
                sortBy,
                sortOrder));
    }

    @Override
    public List<Application> searchActiveApplications(ApplicationFilterOptions filterOptions) {
        return applicationRepository.filter(filterOptions);
    }

    @Override
    public List<JobOpening> getPotentialMatchingJobOpenings(Professional professional,
                                                            Application application,
                                                            double salaryThreshold,
                                                            int skillThreshold) {

        throwIfProfessionalAndApplicationAuthorNotMatch(professional, application);

        throwIfApplicationStatusIsAlreadyMatched(application);

        throwIfSalaryThresholdIsNotBetween0And99Percent(salaryThreshold);

        throwIfSkillThresholdIsNegative(skillThreshold);

        JobFilterOptions filterOptions = createJobFilterBasedOnApplication(application, salaryThreshold);

        List<JobOpening> jobOpenings = jobOpeningRepository.filter(filterOptions);

        return jobOpenings.stream().filter(jobOpening -> filteringHelper.checkForMatchingSkills(
                        application.getSkillSet(),
                        jobOpening.getSkillSet(),
                        skillThreshold))
                .collect(Collectors.toList());
    }


    @Override
    public Application getById(int id) {
        Optional<Application> application = applicationRepository.findById(id);
        if (application.isEmpty()) {
            throw new EntityNotFoundException("Application", id);
        }
        return application.get();
    }

    @Override
    public Application getProfessionalApplicationById(Professional professional, int id) {
        Application application = getById(id);
        if (!professional.getApplications().contains(application) ||
                !application.getProfessional().equals(professional)) {
            throw new ApplicationMismatchException("application", "professional");
        }
        return application;
    }

    @Override
    public Application create(Professional authenticated, Application application) {

        throwIfProfessionalAndApplicationAuthorNotMatch(authenticated, application);

        application.setApplicationStatus(ApplicationStatus.ACTIVE);
        return applicationRepository.save(application);
    }

    @Override
    public Application update(Professional authenticated, Application application, int id) {

        throwIfProfessionalAndApplicationAuthorNotMatch(authenticated, application);

        Application applicationToUpdate = getById(id);
        application.setId(applicationToUpdate.getId());
        application.setSkillSet(applicationToUpdate.getSkillSet());

        return applicationRepository.save(application);
    }


    @Override
    public void addOrModifySkill(Professional authenticated, Application application, Skill skill, SkillLevel level) {

        throwIfProfessionalAndApplicationAuthorNotMatch(authenticated, application);

        skillService.addOrModifySkill(application, skill, level);
    }

    @Override
    public void removeSkill(Professional authenticated, Application application, Skill skill) {

        throwIfProfessionalAndApplicationAuthorNotMatch(authenticated, application);

        skillService.removeSkill(application, skill);
    }


    @Override
    public void delete(Professional authenticated, Professional professional, int id) {
        if (!authenticated.equals(professional)) {
            throw new UnauthorizedOperationException(PROF_APPLICATION_AUTH_ERROR);
        }
        Application applicationToDelete = getById(id);
        if (!applicationToDelete.getProfessional().equals(professional)) {
            throw new ApplicationMismatchException("application", "professional");
        }
        applicationRepository.delete(applicationToDelete);
    }

    private JobFilterOptions createJobFilterBasedOnApplication(Application application, double salaryThreshold) {
        return new JobFilterOptions(
                null,
                null,
                application.getLocation().getId(),
                null,
                application.getMinSalary() * (1 - (salaryThreshold / 100)),
                application.getMaxSalary() * (1 + (salaryThreshold / 100)),
                true,
                "date",
                "desc");
    }

    private void throwIfProfessionalAndApplicationAuthorNotMatch(Professional authenticated, Application application) {
        if (!authenticated.equals(application.getProfessional())) {
            throw new UnauthorizedOperationException(PROF_APPLICATION_AUTH_ERROR);
        }
    }

    private void throwIfSalaryThresholdIsNotBetween0And99Percent(double salaryThreshold) {
        if (salaryThreshold < 0 || salaryThreshold > 99) {
            throw new IllegalArgumentException("Salary threshold must be between 0 and 99%.");
        }
    }

    private void throwIfSkillThresholdIsNegative(int skillThreshold) {
        if (skillThreshold < 0) {
            throw new IllegalArgumentException("Matching skills threshold cannot be negative.");
        }
    }

    private void throwIfApplicationStatusIsAlreadyMatched(Application application) {
        if (application.getApplicationStatus() == ApplicationStatus.MATCHED) {
            throw new IllegalStateException("Application is already matched!");
        }
    }


}

