package com.example.jobmatch.services;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.models.JobForm;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.repositories.interfaces.JobFormRepository;
import com.example.jobmatch.repositories.interfaces.SkillRepository;
import com.example.jobmatch.services.interfaces.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.lang.String.format;

@Service
public class SkillServiceImpl implements SkillService {
    private final SkillRepository skillRepository;
    private final JobFormRepository jobFormRepository;

    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository, JobFormRepository jobFormRepository) {
        this.skillRepository = skillRepository;
        this.jobFormRepository = jobFormRepository;
    }

    @Override
    public List<Skill> getAll() {
        return StreamSupport
                .stream(skillRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Skill getById(int id) {
        Optional<Skill> skill = skillRepository.findById(id);
        if (skill.isEmpty()) {
            throw new EntityNotFoundException("Skill", id);
        }
        return skill.get();
    }

    @Override
    public Skill create(Skill skill) {
        throwIfDuplicateSkillName(skill);
        return skillRepository.save(skill);
    }


    @Override
    public Skill update(Skill skill, int id) {
        throwIfDuplicateSkillName(skill);
        Skill skillToUpdate = getById(id);
        skill.setId(skillToUpdate.getId());
        return skillRepository.save(skill);
    }

    @Override
    public void delete(int id) {
        Skill skillToDelete = getById(id);
        skillRepository.delete(skillToDelete);
    }

    @Override
    public Skill getByName(String name) {
        Optional<Skill> skill = skillRepository.findSkillBySkillName(name);
        if (skill.isEmpty()) {
            throw new EntityNotFoundException("Skill", "name", name);
        }
        return skill.get();
    }

    @Override
    public <T extends JobForm> void addOrModifySkill(T jobForm, Skill skill, SkillLevel level) {
        boolean isDuplicate = jobForm.getSkillSet().containsKey(skill)
                && jobForm.getSkillSet().get(skill) == level;

        if (!isDuplicate) {
            jobForm.getSkillSet().put(skill, level);
            jobFormRepository.save(jobForm);
        } else {
            throw new DuplicateEntityException("Skillset", "skill and level");
        }
    }

    @Override
    public <T extends JobForm> void removeSkill(T jobForm, Skill skill) {
        boolean isFound = jobForm.getSkillSet().containsKey(skill);

        if (isFound) {
            jobForm.getSkillSet().remove(skill);
            jobFormRepository.save(jobForm);
        } else {
            throw new IllegalArgumentException(format("Skill %s not found in professional's skillset",
                    skill.getSkillName()));
        }
    }

    private void throwIfDuplicateSkillName(Skill skill) {
        if (skillRepository.existsSkillBySkillName(skill.getSkillName())) {
            throw new DuplicateEntityException("Skill", "name");
        }
    }
}
