package com.example.jobmatch.exceptions;

public class EmptySearchException extends RuntimeException {
    public EmptySearchException(String type) {
        super(String.format("%s is empty", type));
    }
}
