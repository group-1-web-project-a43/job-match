package com.example.jobmatch.exceptions;

public class LocationNotExistException extends RuntimeException {

    public LocationNotExistException(String city, String country) {
        super(String.format("Location with city %s and country %s not found in database.", city, country));
    }

    public LocationNotExistException(String city) {
        super(String.format("Location with city %s not found in database.", city));
    }

}
