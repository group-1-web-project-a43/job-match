package com.example.jobmatch.exceptions;

public class PasswordDuplicateException extends RuntimeException {

    public PasswordDuplicateException(String newPassword, String oldPassword) {
        super(String.format("%s and %s should not be the same ", newPassword, oldPassword));
    }
}
