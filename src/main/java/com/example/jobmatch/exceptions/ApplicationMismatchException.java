package com.example.jobmatch.exceptions;

public class ApplicationMismatchException extends RuntimeException {

    public ApplicationMismatchException(String application, String professional) {
        super(String.format("The %s doesn't belong to this %s.", application, professional));
    }

    public ApplicationMismatchException(String professional) {
        super(String.format("Application of professional %s not found", professional));
    }
}
