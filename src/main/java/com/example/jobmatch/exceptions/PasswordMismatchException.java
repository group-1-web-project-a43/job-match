package com.example.jobmatch.exceptions;

public class PasswordMismatchException extends RuntimeException {

    public PasswordMismatchException(String newPassword, String oldPassword) {
        super(String.format("%s and %s should match ", newPassword, oldPassword));
    }

    public PasswordMismatchException(String oldPassword) {
        super(String.format("Try again - that's not your %s ", oldPassword));
    }
}
