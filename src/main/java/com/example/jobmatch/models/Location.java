package com.example.jobmatch.models;


import javax.persistence.*;
import java.util.Objects;

import static java.lang.String.format;

@Entity
@Table(name = "locations")
public class Location implements Comparable<Location> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    public Location() {
    }

    public Location(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;
        Location location = (Location) o;
        return getCountry().equals(location.getCountry()) && getCity().equals(location.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCountry(), getCity());
    }

    @Override
    public String toString() {
        return format("%s, %s", this.city, this.country);
    }

    @Override
    public int compareTo(Location o) {
        int comparison = country.compareTo(o.country);
        if (comparison == 0) {
            comparison = city.compareTo(o.city);
        }
        return comparison;
    }
}
