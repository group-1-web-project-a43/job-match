package com.example.jobmatch.models;

import javax.persistence.*;

@Entity(name = "Employee")
@PrimaryKeyJoinColumn(name = "account_id")
@Table(name = "employees")
public class Employee extends Account {

    @Column(name = "name")
    private String name;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "company_id")
    private Company company;

    public Employee() {
    }

    public Employee(String username, String password, String email, Company company) {
        super(username, password, email);
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
