package com.example.jobmatch.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Entity
@PrimaryKeyJoinColumn(name = "account_id")
@Table(name = "companies")
public class Company extends Account {

    @Column
    private String name;

    @JsonIgnore
    @OneToOne(mappedBy = "company", cascade = CascadeType.REMOVE)
    private CompanyInfo companyInfo;

    @JsonIgnore
    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
    private Set<JobOpening> jobOpenings = new HashSet<>();

    public Company() {
    }

    public Company(String username, String password, String email, String name) {
        super(username, password, email);
        this.name = name;
    }

    public Set<JobOpening> getJobOpenings() {
        return jobOpenings;
    }

    public void setJobOpenings(Set<JobOpening> jobOpenings) {
        this.jobOpenings = jobOpenings;
    }

    public Optional<CompanyInfo> getCompanyInfo() {
        return Optional.ofNullable(companyInfo);
    }

    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        if (!super.equals(o)) return false;
        Company company = (Company) o;
        return getName().equals(company.getName()) && Objects.equals(companyInfo, company.companyInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getName(), companyInfo);
    }
}
