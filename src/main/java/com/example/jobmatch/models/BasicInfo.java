package com.example.jobmatch.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "basic_infos")
public abstract class BasicInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "picture_url")
    private String pictureUrl;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;

    public BasicInfo() {
    }

    public BasicInfo(String description, String pictureUrl, Location location) {
        this.description = description;
        this.pictureUrl = pictureUrl;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasicInfo)) return false;
        BasicInfo basicInfo = (BasicInfo) o;
        return id == basicInfo.id && description.equals(basicInfo.description) && pictureUrl.equals(basicInfo.pictureUrl) && location.equals(basicInfo.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, pictureUrl, location);
    }
}