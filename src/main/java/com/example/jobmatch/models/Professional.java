package com.example.jobmatch.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.*;

@Entity(name = "Professional")
@PrimaryKeyJoinColumn(name = "account_id")
@Table(name = "professionals")
public class Professional extends Account {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @JsonIgnore
    @OneToMany(mappedBy = "professional", fetch = FetchType.EAGER)
    private Set<Application> applications = new HashSet<>();

    @JsonIgnore
    @OneToOne(mappedBy = "professional", cascade = CascadeType.REMOVE)
    private ProfessionalInfo professionalInfo;

    @JsonIgnore
    @OneToOne
    @JoinTable(name = "professionals_main_applications",
            joinColumns = @JoinColumn(name = "professional_id", referencedColumnName = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "application_id", referencedColumnName = "job_form_id", nullable = true))
    private Application mainApplication;

    @ElementCollection
    @CollectionTable(
            name = "professionals_match_requests",
            joinColumns = {@JoinColumn(name = "professional_id", referencedColumnName = "account_id")}
    )
    @MapKeyJoinColumn(name = "match_request_id", referencedColumnName = "match_request_id")
    @Column(name = "accepted_match")
    private Map<MatchRequest, Boolean> jobOffers = new HashMap<>();

    public Professional() {
    }

    public Professional(String username, String password, String email, String firstName, String lastName) {
        super(username, password, email);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Set<Application> getApplications() {
        return applications;
    }

    public void setApplications(Set<Application> applications) {
        this.applications = applications;
    }

    public Optional<ProfessionalInfo> getProfessionalInfo() {
        return Optional.ofNullable(professionalInfo);
    }

    public void setProfessionalInfo(ProfessionalInfo professionalInfo) {
        this.professionalInfo = professionalInfo;
    }

    public Optional<Application> getMainApplication() {
        return Optional.ofNullable(mainApplication);
    }

    public void setMainApplication(Application mainApplication) {
        this.mainApplication = mainApplication;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<MatchRequest, Boolean> getJobOffers() {
        return jobOffers;
    }

    public void setJobOffers(Map<MatchRequest, Boolean> jobOffers) {
        this.jobOffers = jobOffers;
    }
}
