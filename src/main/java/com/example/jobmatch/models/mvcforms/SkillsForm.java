package com.example.jobmatch.models.mvcforms;

public class SkillsForm {
    private String skill;
    private String level;

    public SkillsForm() {
    }

    public SkillsForm(String skill, String level) {
        this.skill = skill;
        this.level = level;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String levelStr) {
        this.level = levelStr;
    }
}