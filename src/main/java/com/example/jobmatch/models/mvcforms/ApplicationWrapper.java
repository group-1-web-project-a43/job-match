package com.example.jobmatch.models.mvcforms;

import com.example.jobmatch.models.Application;

import javax.validation.constraints.NotNull;

public class ApplicationWrapper {
    @NotNull
    private Application application;

    public ApplicationWrapper() {
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
