package com.example.jobmatch.models.mvcforms;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ThresholdForm {

    @NotNull
    @Range(min = 0, max = 99, message = "Salary threshold percentage must be between 0 and 99")
    private double salaryThreshold;
    @NotNull
    @Min(value = 0, message = "Skill threshold cannot be less than 0")
    private int skillThreshold;

    public ThresholdForm() {
    }

    public double getSalaryThreshold() {
        return salaryThreshold;
    }

    public void setSalaryThreshold(double salaryThreshold) {
        this.salaryThreshold = salaryThreshold;
    }

    public int getSkillThreshold() {
        return skillThreshold;
    }

    public void setSkillThreshold(int skillThreshold) {
        this.skillThreshold = skillThreshold;
    }
}
