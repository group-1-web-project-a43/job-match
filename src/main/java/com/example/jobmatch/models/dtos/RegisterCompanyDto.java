package com.example.jobmatch.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterCompanyDto extends LoginDto {

    @NotEmpty
    @Email(message = "Email should have valid email address structure")
    private String email;

    @NotEmpty
    @Size(min = 5, max = 20, message = "Company name must be between 5 and 20 characters")
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
