package com.example.jobmatch.models.dtos.dtosOut;

public class EmployeeDtoOut {

    private String name;

    public EmployeeDtoOut() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
