package com.example.jobmatch.models.dtos.dtosOut;

import com.example.jobmatch.enums.SkillLevel;

import java.util.HashMap;
import java.util.Map;

public class ApplicationDtoOut {

    private String name;

    private String professionalName;

    private double minSalary;

    private double maxSalary;

    private String description;

    private boolean remotePossible;

    private String location;

    private String status;

    private Map<String, SkillLevel> skillSet = new HashMap<>();

    public ApplicationDtoOut() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(double minSalary) {
        this.minSalary = minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(double maxSalary) {
        this.maxSalary = maxSalary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRemotePossible() {
        return remotePossible;
    }

    public void setRemotePossible(boolean remotePossible) {
        this.remotePossible = remotePossible;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, SkillLevel> getSkillSet() {
        return skillSet;
    }

    public void setSkillSet(Map<String, SkillLevel> skillSet) {
        this.skillSet = skillSet;
    }
}
