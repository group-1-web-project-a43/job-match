package com.example.jobmatch.models.dtos.dtosOut;

public class MatchRequestDtoOut {

    private JobOpeningDtoOut jobOpening;
    private ApplicationDtoOut application;

    private boolean is_match;

    public MatchRequestDtoOut() {
    }

    public JobOpeningDtoOut getJobOpening() {
        return jobOpening;
    }

    public void setJobOpening(JobOpeningDtoOut jobOpening) {
        this.jobOpening = jobOpening;
    }

    public ApplicationDtoOut getApplication() {
        return application;
    }

    public void setApplication(ApplicationDtoOut application) {
        this.application = application;
    }

    public boolean isIs_match() {
        return is_match;
    }

    public void setIs_match(boolean is_match) {
        this.is_match = is_match;
    }
}
