package com.example.jobmatch.models.dtos.dtosIn;

import com.example.jobmatch.models.Location;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CompanyInfoDtoIn {

    @NotNull
    @Size(max = 1800)
    private String description;


    private String pictureUrl;

    @NotNull
    private Location location;

    @NotNull
    @Size(max = 200)
    private String contacts;

    public CompanyInfoDtoIn() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
}
