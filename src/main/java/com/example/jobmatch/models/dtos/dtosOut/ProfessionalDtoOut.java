package com.example.jobmatch.models.dtos.dtosOut;

public class ProfessionalDtoOut {

    private String fullName;

    public ProfessionalDtoOut() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
