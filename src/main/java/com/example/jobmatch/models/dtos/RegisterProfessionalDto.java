package com.example.jobmatch.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterProfessionalDto extends LoginDto {

    @NotEmpty
    @Email(message = "Email should have valid email address structure")
    private String email;

    @NotEmpty
    @Size(min = 5, max = 20, message = "Firstname must be between 5 and 20 characters")
    private String firstName;

    @NotEmpty
    @Size(min = 5, max = 20, message = "Lastname must be between 5 and 20 characters")
    private String lastName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
