package com.example.jobmatch.models.dtos.dtosIn;

import com.example.jobmatch.models.Location;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProfessionalInfoDtoIn {

    @NotNull
    @Size(max = 1800)
    private String description;

    private String photoUrl;


    @NotNull
    private Location location;

    @NotNull
    private boolean isActive;

    public ProfessionalInfoDtoIn() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
