package com.example.jobmatch.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginDto {
    @NotEmpty
    @Size(min = 5, max = 30, message = "Username must be between 10 and 40 characters")
    private String username;

    @NotEmpty
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20 characters")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
