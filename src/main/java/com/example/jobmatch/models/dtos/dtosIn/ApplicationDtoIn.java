package com.example.jobmatch.models.dtos.dtosIn;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.models.Location;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApplicationDtoIn {
    @NotNull
    @Size(min = 10, max = 45)
    private String name;

    @NotNull
    private double minSalary;

    @NotNull
    private double maxSalary;

    @NotNull
    @Size(max = 1800)
    private String description;

    @NotNull
    private Location location;

    @NotNull
    private ApplicationStatus applicationStatus;

    public ApplicationDtoIn() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(double minSalary) {
        this.minSalary = minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(double maxSalary) {
        this.maxSalary = maxSalary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
}
