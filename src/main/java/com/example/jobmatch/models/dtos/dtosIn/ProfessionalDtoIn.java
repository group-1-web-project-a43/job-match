package com.example.jobmatch.models.dtos.dtosIn;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ProfessionalDtoIn {

    @NotEmpty
    @Size(min = 5, max = 30, message = "Username must be between 10 and 30 characters")
    private String username;


    @NotEmpty
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20 characters")
    private String password;


    @NotEmpty
    @Email(message = "Email should have valid email address structure")
    private String email;

    @NotEmpty
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 characters")
    private String firstName;


    @NotEmpty
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 characters")
    private String lastName;

    public ProfessionalDtoIn() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
