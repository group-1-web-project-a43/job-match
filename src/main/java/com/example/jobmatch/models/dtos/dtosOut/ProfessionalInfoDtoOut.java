package com.example.jobmatch.models.dtos.dtosOut;

import com.example.jobmatch.models.JobOpening;

import java.util.Map;

public class ProfessionalInfoDtoOut {
    private String description;

    private String photoUrl;

    private String country;

    private String city;

    private boolean isActive;

    private String professionalFullName;

    private Map<JobOpening, Boolean> jobOffers;


    public ProfessionalInfoDtoOut() {
    }

    public String getProfessionalFullName() {
        return professionalFullName;
    }

    public void setProfessionalFullName(String professionalFullName) {
        this.professionalFullName = professionalFullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Map<JobOpening, Boolean> getJobOffers() {
        return jobOffers;
    }

    public void setJobOffers(Map<JobOpening, Boolean> jobOffers) {
        this.jobOffers = jobOffers;
    }
}
