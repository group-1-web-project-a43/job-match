package com.example.jobmatch.models.dtos.dtosIn;


import com.example.jobmatch.models.Location;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

public class JobOpeningDtoIn {

    @NotNull
    @Size(min = 10, max = 45)
    private String name;

    @NotNull
    private double minSalary;

    @NotNull
    private double maxSalary;

    @NotNull
    @Size(max = 1800)
    private String description;

    @NotNull
    private Location location;

    @NotNull
    private Map<String, String> skillSet = new HashMap<>();

    @NotNull
    private boolean isRemote;

    private boolean isActive;

    public JobOpeningDtoIn() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(double minSalary) {
        this.minSalary = minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(double maxSalary) {
        this.maxSalary = maxSalary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<String, String> getSkillSet() {
        return skillSet;
    }

    public void setSkillSet(Map<String, String> skillSet) {
        this.skillSet = skillSet;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public void setRemote(boolean remote) {
        isRemote = remote;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
