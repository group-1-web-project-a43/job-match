package com.example.jobmatch.models.dtos.dtosOut;

import com.example.jobmatch.enums.SkillLevel;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;


public class JobOpeningDtoOut {

    private String name;

    private String companyName;

    private String status;

    private double minSalary;

    private double maxSalary;

    private String description;

    private boolean remotePossible;

    private String location;

    private Map<String, SkillLevel> skillSet = new HashMap<>();

    private LocalDateTime publishedOn;

    public JobOpeningDtoOut() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(double minSalary) {
        this.minSalary = minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(double maxSalary) {
        this.maxSalary = maxSalary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRemotePossible() {
        return remotePossible;
    }

    public void setRemotePossible(boolean remotePossible) {
        this.remotePossible = remotePossible;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Map<String, SkillLevel> getSkillSet() {
        return skillSet;
    }

    public void setSkillSet(Map<String, SkillLevel> skillSet) {
        this.skillSet = skillSet;
    }

    public LocalDateTime getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(LocalDateTime publishedOn) {
        this.publishedOn = publishedOn;
    }
}
