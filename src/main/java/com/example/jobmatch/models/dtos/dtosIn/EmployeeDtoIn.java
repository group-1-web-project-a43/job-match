package com.example.jobmatch.models.dtos.dtosIn;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EmployeeDtoIn {

    @Size(min = 5, max = 30, message = "Username must be between 10 and 30 characters")
    private String username;


    @NotNull
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20 characters")
    private String password;


    @NotNull
    @Email(message = "Email should have valid email address structure")
    private String email;

    @NotNull
    @Size(min = 8, max = 40, message = "Employee name must be between 8 and 40 characters long")
    private String name;


    public EmployeeDtoIn() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
