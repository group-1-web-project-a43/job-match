package com.example.jobmatch.models.dtos.dtosOut;

public class CompanyDtoOut {

    private String companyName;

    public CompanyDtoOut() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
