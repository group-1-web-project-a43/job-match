package com.example.jobmatch.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "match_requests")
public class MatchRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "match_request_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "job_opening_id")
    private JobOpening jobOpening;

    @ManyToOne
    @JoinColumn(name = "application_id")
    private Application application;

    @Column(name = "is_match")
    private boolean isMatch;

    public MatchRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JobOpening getJobOpening() {
        return jobOpening;
    }

    public void setJobOpening(JobOpening jobOpening) {
        this.jobOpening = jobOpening;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public boolean isMatch() {
        return isMatch;
    }

    public void setMatch(boolean match) {
        isMatch = match;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchRequest)) return false;
        MatchRequest that = (MatchRequest) o;
        return id == that.id && jobOpening.equals(that.jobOpening) && application.equals(that.application);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, jobOpening, application);
    }

    @Override
    public String toString() {
        return "MatchRequest{" +
                jobOpening +
                ", isMatch=" + isMatch +
                '}';
    }
}
