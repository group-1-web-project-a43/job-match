package com.example.jobmatch.models;


import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

import static java.lang.String.format;


@Entity
@PrimaryKeyJoinColumn(name = "job_form_id")
@Table(name = "job_openings")
public class JobOpening extends JobForm {


    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "company_id")
    private Company company;

    @Column(name = "is_active")
    private boolean isActive;


    @CreationTimestamp
    @Column(name = "published_on")
    private LocalDateTime publishedOn;

    public JobOpening() {
    }

    public JobOpening(String name, double minSalary, double maxSalary, String description, Location location, boolean isRemote, Company company, boolean isActive) {
        super(name, minSalary, maxSalary, description, location, isRemote);
        this.company = company;
        this.isActive = isActive;
    }

    public LocalDateTime getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(LocalDateTime publishedOn) {
        this.publishedOn = publishedOn;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        String status;
        if (isActive()) status = "Active";
        else status = "Archived";
        return format("%s: %s; Status: %s", getCompany().getName(), getName(), status);
    }
}
