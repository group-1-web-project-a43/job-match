package com.example.jobmatch.models;

import com.example.jobmatch.enums.ApplicationStatus;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name = "job_form_id")
@Table(name = "applications")
public class Application extends JobForm {

    @ManyToOne
    @JoinColumn(name = "professional_id")
    private Professional professional;

    @Enumerated(EnumType.STRING)
    @Column(name = "application_status")
    private ApplicationStatus applicationStatus;

    public Application() {
    }

    public Application(String name, double minSalary, double maxSalary, String description, Location location, boolean isRemote, Professional professional, ApplicationStatus applicationStatus) {
        super(name, minSalary, maxSalary, description, location, isRemote);
        this.professional = professional;
        this.applicationStatus = applicationStatus;
    }

    public Professional getProfessional() {
        return professional;
    }

    public void setProfessional(Professional professional) {
        this.professional = professional;
    }

    public ApplicationStatus getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(ApplicationStatus applicationStatus) {
        this.applicationStatus = applicationStatus;
    }


    @Override
    public String toString() {
        return "Application: "
                + professional.getFirstName() + " "
                + professional.getLastName() +
                ", Status: " + applicationStatus;
    }
}
