package com.example.jobmatch.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@PrimaryKeyJoinColumn(name = "info_id")
@Table(name = "professional_infos")
public class ProfessionalInfo extends BasicInfo {

    @JsonIgnore
    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "professional_id")
    private Professional professional;

    @Column(name = "is_active")
    private boolean isActive;

    public ProfessionalInfo() {
    }

    public ProfessionalInfo(String description, String pictureUrl, Location location, Professional professional, boolean isActive) {
        super(description, pictureUrl, location);
        this.professional = professional;
        this.isActive = isActive;
    }

    public Professional getProfessional() {
        return professional;
    }

    public void setProfessional(Professional professional) {
        this.professional = professional;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfessionalInfo)) return false;
        if (!super.equals(o)) return false;
        ProfessionalInfo that = (ProfessionalInfo) o;
        return professional.equals(that.professional);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), professional);
    }
}
