package com.example.jobmatch.models.filters;


import java.util.Optional;

public class ApplicationFilterOptions {

    private Optional<String> name;
    private Optional<String> description;
    private Optional<Integer> location;
    private Optional<Double> minSalary;
    private Optional<Double> maxSalary;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;


    public ApplicationFilterOptions(Optional<String> name,
                                    Optional<String> description,
                                    Optional<Integer> location,
                                    Optional<Double> minSalary,
                                    Optional<Double> maxSalary,
                                    Optional<String> sortBy,
                                    Optional<String> sortOrder) {

        this.name = name;
        this.description = description;
        this.location = location;
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public ApplicationFilterOptions() {
    }

    public ApplicationFilterOptions(String name,
                                    String description,
                                    Integer location,
                                    Double minSalary,
                                    Double maxSalary,
                                    String sortBy,
                                    String sortOrder) {

        this.name = Optional.ofNullable(name);
        this.description = Optional.ofNullable(description);
        this.location = Optional.ofNullable(location);
        this.minSalary = Optional.ofNullable(minSalary);
        this.maxSalary = Optional.ofNullable(maxSalary);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Optional.ofNullable(name);
    }

    public Optional<String> getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Optional.ofNullable(description);
    }

    public Optional<Integer> getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = Optional.ofNullable(location);
    }

    public Optional<Double> getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Double minSalary) {
        this.minSalary = Optional.ofNullable(minSalary);
    }

    public Optional<Double> getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Double maxSalary) {
        this.maxSalary = Optional.ofNullable(maxSalary);
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = Optional.ofNullable(sortBy);
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = Optional.ofNullable(sortOrder);
    }
}
