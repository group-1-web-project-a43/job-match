package com.example.jobmatch.models.filters;

import java.util.Optional;

public class JobFilterOptions {
    private Optional<Boolean> isActive;
    private Optional<Integer> company;
    private Optional<String> name;
    private Optional<String> description;
    private Optional<Integer> location;
    private Optional<Double> minSalary;
    private Optional<Double> maxSalary;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public JobFilterOptions(Optional<Boolean> isActive,
                            Optional<Integer> company,
                            Optional<String> name,
                            Optional<String> description,
                            Optional<Integer> location,
                            Optional<Double> minSalary,
                            Optional<Double> maxSalary,
                            Optional<String> sortBy,
                            Optional<String> sortOrder) {
        this.isActive = isActive;
        this.company = company;
        this.name = name;
        this.description = description;
        this.location = location;
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public JobFilterOptions() {
    }

    public JobFilterOptions(String name,
                            String description,
                            Integer locationId,
                            Integer companyId,
                            Double minSalary,
                            Double maxSalary,
                            Boolean isActive,
                            String sortBy,
                            String sortOrder) {

        this.name = Optional.ofNullable(name);
        this.description = Optional.ofNullable(description);
        this.location = Optional.ofNullable(locationId);
        this.company = Optional.ofNullable(companyId);
        this.minSalary = Optional.ofNullable(minSalary);
        this.maxSalary = Optional.ofNullable(maxSalary);
        this.isActive = Optional.ofNullable(isActive);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Optional.ofNullable(name);
    }

    public Optional<String> getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Optional.ofNullable(description);
    }

    public Optional<Integer> getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = Optional.ofNullable(location);
    }

    public Optional<Double> getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Double minSalary) {
        this.minSalary = Optional.ofNullable(minSalary);
    }

    public Optional<Double> getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(Double maxSalary) {
        this.maxSalary = Optional.ofNullable(maxSalary);
    }


    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = Optional.ofNullable(sortBy);
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<Boolean> getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = Optional.ofNullable(isActive);
    }

    public Optional<Integer> getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = Optional.ofNullable(company);
    }
}
