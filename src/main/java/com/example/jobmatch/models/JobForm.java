package com.example.jobmatch.models;


import com.example.jobmatch.enums.SkillLevel;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "job_forms")
public abstract class JobForm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "salary_min")
    private double minSalary;

    @Column(name = "salary_max")
    private double maxSalary;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;


    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "job_forms_skillsets",
            joinColumns = {@JoinColumn(name = "job_form_id", referencedColumnName = "id")}
    )
    @MapKeyJoinColumn(name = "skill_id")
    @Column(name = "skill_level")
    @Enumerated(EnumType.STRING)
    private Map<Skill, SkillLevel> skillSet = new HashMap<>();

    @Column(name = "is_remote")
    private boolean isRemote;

    public JobForm() {
    }

    public JobForm(String name, double minSalary, double maxSalary, String description, Location location, boolean isRemote) {
        this.name = name;
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
        this.description = description;
        this.location = location;
        this.isRemote = isRemote;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(double minSalary) {
        this.minSalary = minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(double maxSalary) {
        this.maxSalary = maxSalary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<Skill, SkillLevel> getSkillSet() {
        return skillSet;
    }

    public void setSkillSet(Map<Skill, SkillLevel> skillSet) {
        this.skillSet = skillSet;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public void setRemote(boolean remote) {
        isRemote = remote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobForm)) return false;
        JobForm jobForm = (JobForm) o;
        return id == jobForm.id && Double.compare(jobForm.minSalary, minSalary) == 0 &&
                Double.compare(jobForm.maxSalary, maxSalary) == 0 &&
                name.equals(jobForm.name) &&
                description.equals(jobForm.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, minSalary, maxSalary, description);
    }
}
