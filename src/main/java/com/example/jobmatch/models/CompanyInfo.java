package com.example.jobmatch.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "CompanyInfo")
@PrimaryKeyJoinColumn(name = "info_id")
@Table(name = "company_infos")
public class CompanyInfo extends BasicInfo {

    @JsonIgnore
    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "company_id")
    private Company company;

    @Column(name = "contacts")
    private String contacts;

    public CompanyInfo() {
    }

    public CompanyInfo(String description, String pictureUrl, Location location, Company company, String contacts) {
        super(description, pictureUrl, location);
        this.company = company;
        this.contacts = contacts;
    }


    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompanyInfo)) return false;
        if (!super.equals(o)) return false;
        CompanyInfo that = (CompanyInfo) o;
        return contacts.equals(that.contacts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), contacts);
    }
}
