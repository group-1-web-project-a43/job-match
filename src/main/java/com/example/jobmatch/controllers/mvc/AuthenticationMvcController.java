package com.example.jobmatch.controllers.mvc;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.dtos.LoginDto;
import com.example.jobmatch.models.dtos.RegisterCompanyDto;
import com.example.jobmatch.models.dtos.RegisterProfessionalDto;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.ProfessionalService;
import com.example.jobmatch.services.mappers.CompanyMapper;
import com.example.jobmatch.services.mappers.ProfessionalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final CompanyMapper companyMapper;
    private final CompanyService companyService;
    private final ProfessionalMapper professionalMapper;
    private final ProfessionalService professionalService;

    @Autowired
    public AuthenticationMvcController(AuthenticationHelper authenticationHelper, CompanyMapper companyMapper, CompanyService companyService, ProfessionalMapper professionalMapper, ProfessionalService professionalService) {
        this.authenticationHelper = authenticationHelper;
        this.companyMapper = companyMapper;
        this.companyService = companyService;
        this.professionalMapper = professionalMapper;
        this.professionalService = professionalService;
    }

    @GetMapping("/login-professionals")
    public String showProfessionalLoginPage(Model model) {
        model.addAttribute("professionalLogin", new LoginDto());
        return "ProfessionalLoginView";
    }

    @GetMapping("/login-companies")
    public String showCompanyLoginPage(Model model) {
        model.addAttribute("companyLogin", new LoginDto());
        return "CompanyLoginView";
    }

    @GetMapping("/login-employees")
    public String showEmployeeLoginPage(Model model) {
        model.addAttribute("employeeLogin", new LoginDto());
        return "EmployeeLoginView";
    }


    @PostMapping("/login-professionals")
    public String handleProfessionalLogin(@Valid @ModelAttribute("professionalLogin") LoginDto dto,
                                          BindingResult bindingResult,
                                          HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "ProfessionalLoginView";
        }
        try {
            authenticationHelper.AuthenticationProfessionals(dto.getUsername(), dto.getPassword());
            session.setAttribute("currentUser", dto.getUsername());
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "ProfessionalLoginView";
        }
    }

    @PostMapping("/login-companies")
    public String handleCompanyLogin(@Valid @ModelAttribute("companyLogin") LoginDto dto,
                                     BindingResult bindingResult,
                                     HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "CompanyLoginView";
        }
        try {
            authenticationHelper.AuthenticationCompanies(dto.getUsername(), dto.getPassword());
            session.setAttribute("currentUser", dto.getUsername());
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "CompanyLoginView";
        }
    }

    @PostMapping("/login-employees")
    public String handleEmployeeLogin(@Valid @ModelAttribute("employeeLogin") LoginDto dto,
                                      BindingResult bindingResult,
                                      HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "EmployeeLoginView";
        }
        try {
            authenticationHelper.AuthenticationEmployees(dto.getUsername(), dto.getPassword());
            session.setAttribute("currentUser", dto.getUsername());
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "EmployeeLoginView";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";

    }

    @GetMapping("/register-companies")
    public String handleCompanyRegister(Model model) {
        model.addAttribute("registerCompany", new RegisterCompanyDto());
        return "RegisterCompanyView";
    }

    @GetMapping("/register-professionals")
    public String handleProfRegister(Model model) {
        model.addAttribute("registerProf", new RegisterProfessionalDto());
        return "RegisterProfessionalView";
    }


    @PostMapping("/register-companies")
    public String handleRegisterCompany(@Valid @ModelAttribute("registerCompany") RegisterCompanyDto registerDto,
                                        BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "RegisterCompanyView";
        }
        try {
            Company company = companyMapper.fromDto(registerDto);
            companyService.create(company);
            return "redirect:/auth/login-companies";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "RegisterCompanyView";
        }

    }

    @PostMapping("/register-professionals")
    public String handleRegisterProfessional(@Valid @ModelAttribute("registerProf") RegisterProfessionalDto professionalDto,
                                             BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "RegisterProfessionalView";
        }
        try {
            Professional professional = professionalMapper.fromDto(professionalDto);
            professionalService.create(professional);
            return "redirect:/auth/login-professionals";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "RegisterProfessionalView";
        }
    }
}



