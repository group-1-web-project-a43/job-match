package com.example.jobmatch.controllers.mvc;

import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.*;
import com.example.jobmatch.repositories.interfaces.ProfessionalsRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.JobOpeningService;
import com.example.jobmatch.services.interfaces.LocationService;
import com.example.jobmatch.services.interfaces.ProfessionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

@Controller
public class GlobalMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final CompanyService companyService;
    private final ProfessionalService professionalService;
    private final JobOpeningService jobOpeningService;
    private final LocationService locationService;

    @Autowired
    public GlobalMvcController(AuthenticationHelper authenticationHelper,
                               CompanyService companyService,
                               ProfessionalService professionalService,
                               JobOpeningService jobOpeningService,
                               LocationService locationService) {

        this.authenticationHelper = authenticationHelper;
        this.companyService = companyService;
        this.professionalService = professionalService;
        this.jobOpeningService = jobOpeningService;
        this.locationService = locationService;
    }

    @ControllerAdvice("com.example.jobmatch.controllers.mvc")
    public class testAdvice {

        @InitBinder
        public void initBinder(WebDataBinder binder) {
            binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        }

        @ModelAttribute("companies")
        public List<Company> populateCompanies() {
            return companyService.getAll();
        }

        @ModelAttribute("registeredProfessionals")
        public int populateProfessionals() {
            return professionalService.getAll().size();
        }

        @ModelAttribute("registeredCompanies")
        public int populateNumberCompanies() {
            return companyService.getAll().size();
        }

        @ModelAttribute("jobOpeningsByDate")
        public List<JobOpening> findJobOpeningsByDate() {
            return jobOpeningService.findJobOpeningByPublishedOn();
        }

        @ModelAttribute("isAuthenticated")
        public boolean populateIsAuthenticated(HttpSession session) {
            return session.getAttribute("currentUser") != null;
        }

        @ModelAttribute("loggedUser")
        public Optional<Account> populateLoggedUser(HttpSession session) {
            if (populateIsAuthenticated(session)) {
                return Optional.ofNullable(authenticationHelper.tryGetCurrentUser(session));
            }

            return Optional.empty();
        }

        @ModelAttribute("loggedProfessional")
        public Optional<Professional> populateLoggedProfessional(HttpSession session) {
            try {

                if (populateIsAuthenticated(session)) {
                    return Optional.ofNullable(authenticationHelper.tryGetCurrentProf(session));
                }
                return Optional.empty();
            } catch (UnauthorizedOperationException e) {
                return Optional.empty();
            }
        }

        @ModelAttribute("loggedEmployee")
        public Optional<Employee> populateLoggedEmployee(HttpSession session) {
            try {

                if (populateIsAuthenticated(session)) {
                    return Optional.ofNullable(authenticationHelper.tryGetCurrentEmployee(session));
                }
                return Optional.empty();
            } catch (UnauthorizedOperationException e) {
                return Optional.empty();
            }
        }

        @ModelAttribute("loggedCompany")
        public Optional<Company> populateLoggedCompany(HttpSession session) {
            try {

                if (populateIsAuthenticated(session)) {
                    return Optional.ofNullable(authenticationHelper.tryGetCurrentCompany(session));
                }
                return Optional.empty();
            } catch (UnauthorizedOperationException e) {
                return Optional.empty();
            }
        }

        @ModelAttribute("locations")
        public Set<Location> populateLocations() {
            return new TreeSet<>(locationService.getAll());
        }

        @ModelAttribute("matchRequestsSize")
        public int showMatchRequestSize()
        {
            return professionalService.getAllClosedContracts();
        }
    }
}
