package com.example.jobmatch.controllers.mvc;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.ApplicationMismatchException;
import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.models.mvcforms.ApplicationWrapper;
import com.example.jobmatch.models.mvcforms.SkillsForm;
import com.example.jobmatch.models.mvcforms.ThresholdForm;
import com.example.jobmatch.services.interfaces.JobOpeningService;
import com.example.jobmatch.services.interfaces.MatchRequestService;
import com.example.jobmatch.services.interfaces.SkillService;
import com.example.jobmatch.services.mappers.JobFormMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/job-openings")
public class JobOpeningsMvcController {

    private final JobOpeningService jobOpeningService;
    private final MatchRequestService matchRequestService;
    private final AuthenticationHelper authenticationHelper;
    private final JobFormMapper jobFormMapper;
    private final SkillService skillService;

    @Autowired
    public JobOpeningsMvcController(JobOpeningService jobOpeningService,
                                    MatchRequestService matchRequestService,
                                    AuthenticationHelper authenticationHelper, JobFormMapper jobFormMapper, SkillService skillService) {

        this.jobOpeningService = jobOpeningService;
        this.matchRequestService = matchRequestService;
        this.authenticationHelper = authenticationHelper;
        this.jobFormMapper = jobFormMapper;
        this.skillService = skillService;
    }

    @GetMapping
    public String showAllJobOpenings(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        List<JobOpening> jobOpeningList = jobOpeningService.getAll();
        model.addAttribute("jobs", jobOpeningList);
        model.addAttribute("jobsFilterForm", new JobFilterOptions());
        return "JobOpeningsView";
    }

    @PostMapping
    public String filterJobOpenings(HttpSession session, @ModelAttribute JobFilterOptions filterOptions, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            List<JobOpening> jobOpeningList = jobOpeningService.searchJobOpening(filterOptions);
            model.addAttribute("jobs", jobOpeningList);
            model.addAttribute("jobsFilterForm", new JobFilterOptions());
            return "JobOpeningsView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        }

    }


    @GetMapping("/{id}")
    public String showSingleJobOpening(HttpSession session, @PathVariable int id, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        Optional<List<JobOpening>> appliedToOpenings;

        try {
            Professional professional = authenticationHelper.tryGetCurrentProf(session);
            List<MatchRequest> matchRequests = matchRequestService.getByProfessionalApplication(professional);
            appliedToOpenings = Optional.of(matchRequests.stream().map(MatchRequest::getJobOpening).collect(Collectors.toList()));
        } catch (UnauthorizedOperationException e) {
            appliedToOpenings = Optional.empty();
        }

        try {
            JobOpening jobOpening = jobOpeningService.getById(id);
            model.addAttribute("skills", new HashSet<>(skillService.getAll()));
            model.addAttribute("loggedProfAppliedJobOpenings", appliedToOpenings);
            model.addAttribute("emptyApplication", new ApplicationWrapper());
            model.addAttribute("jobOpening", jobOpening);
            model.addAttribute("form", new SkillsForm());
            model.addAttribute("skillRemove", new Skill());
            model.addAttribute("thresholdForm", new ThresholdForm());
            return "JobOpeningView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("{id}/potential-matches")
    public String showJobOpeningPotentialMatches(@PathVariable int id,
                                                 @Valid @ModelAttribute("thresholdForm")ThresholdForm thresholdForm,
                                                 BindingResult bindingResult,
                                                 HttpSession session,
                                                 Model model) {

        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        if(bindingResult.hasErrors()) {
            return "redirect:/professionals/{profId}/applications/{appId}";
        }
        try {
            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(id, employeeAuth.getCompany());
            List<Application> matchingApplications = jobOpeningService
                    .getPotentialMatchingApplications(employeeAuth, jobOpening,
                            thresholdForm.getSalaryThreshold(), thresholdForm.getSkillThreshold());

            model.addAttribute("applications", matchingApplications);
            model.addAttribute("appsFilterForm", new ApplicationFilterOptions());
            return "ApplicationsView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalStateException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue("salaryThreshold", "threshold_validation", e.getMessage());
            return "redirect:/professionals/{profId}/applications/{appId}";
        }

    }

    @PostMapping("/{jobId}/add-skills")
    public String addOrModifySkillInJobOpening(@PathVariable int jobId,
                                               @Valid @ModelAttribute("form") SkillsForm form,
                                               BindingResult bindingResult,
                                               HttpSession session,
                                               Model model) {

        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/{jobId}";
        }

        try {
            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(jobId, employeeAuth.getCompany());
            Skill skill = skillService.getByName(form.getSkill());
            SkillLevel skillLevel = jobFormMapper.parseSkillLevel(form.getLevel());

            jobOpeningService.addOrModifySkill(employeeAuth, jobOpening, skill, skillLevel);
            return "redirect:/job-openings/{jobId}";
        } catch (ApplicationMismatchException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("skill", "duplicate_skill", e.getMessage());
            return "redirect:/job-openings/{jobId}";
        }
    }

    @PostMapping("/{jobId}/remove-skills")
    public String removeSkillFromJobOpening(@PathVariable int jobId,
                                             @Valid @ModelAttribute("skillRemove") Skill skillStr,
                                             HttpSession session,
                                             BindingResult bindingResult,
                                             Model model) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/job-openings/{jobId}";
        }

        try {

            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(jobId, employeeAuth.getCompany());
            Skill skill = skillService.getByName(skillStr.getSkillName());
            jobOpeningService.removeSkill(employeeAuth, jobOpening, skill);
            return "redirect:/job-openings/{jobId}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue("skillName", "skill_not_found", e.getMessage());
            return "redirect:/job-openings/{jobId}";
        }
    }


    @GetMapping("/{id}/apply")
    public String applyWithMainApplication(HttpSession session,
                                           Model model,
                                           @PathVariable int id) {
        Professional profAuth;
        try {
            profAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            if(profAuth.getMainApplication().isEmpty()) {
                throw new EntityNotFoundException("Main Application", profAuth.getId());
            }
            JobOpening jobOpening = jobOpeningService.getById(id);
            matchRequestService.create(profAuth, jobOpening, profAuth.getMainApplication().get());
            return "redirect:/job-openings/{id}";
        } catch (EntityNotFoundException | ApplicationMismatchException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "ConflictView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        }
    }

    @PostMapping("/{id}/apply-selected")
    public String applyWithSelectApplication(@PathVariable int id,
                                             @Valid @ModelAttribute("emptyApplication") ApplicationWrapper applicationForm,
                                             BindingResult bindingResult,
                                             Model model,
                                             HttpSession session) {
        Professional profAuth;
        try {
            profAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/job-openings/{id}";
        }
        try {
            JobOpening jobOpening = jobOpeningService.getById(id);
            Application application = applicationForm.getApplication();
            MatchRequest matchRequest = matchRequestService.create(profAuth, jobOpening, application);
            model.addAttribute("match", matchRequest);
            return "redirect:/job-openings/{id}";
        } catch (EntityNotFoundException | ApplicationMismatchException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "ConflictView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        }
    }


}


