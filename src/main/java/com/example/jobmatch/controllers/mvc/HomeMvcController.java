package com.example.jobmatch.controllers.mvc;

import com.example.jobmatch.services.interfaces.CompanyInfoService;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.JobOpeningService;
import com.example.jobmatch.services.interfaces.ProfessionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final JobOpeningService jobOpeningService;
    private final CompanyService companyService;
    private final CompanyInfoService companyInfoService;
    private final ProfessionalService professionalService;

    @Autowired
    public HomeMvcController(JobOpeningService jobOpeningService, CompanyService companyService, CompanyInfoService companyInfoService, ProfessionalService professionalService) {
        this.jobOpeningService = jobOpeningService;
        this.companyService = companyService;
        this.companyInfoService = companyInfoService;
        this.professionalService = professionalService;
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("companies", companyService.getAll());
        model.addAttribute("companiesInfo", companyInfoService.getAll());
        model.addAttribute("professionals", professionalService.getAll());
        return "HomeView";
    }

    @ModelAttribute("jobOpeningsCount")
    public int populateJobOpeningsCount() {
        return jobOpeningService.countJobOpeningsByActiveStatus();
    }


}
