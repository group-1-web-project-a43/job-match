package com.example.jobmatch.controllers.mvc;

import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.models.dtos.dtosIn.CompanyInfoDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.EmployeeDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.JobOpeningDtoIn;
import com.example.jobmatch.services.interfaces.*;
import com.example.jobmatch.services.mappers.CompanyMapper;
import com.example.jobmatch.services.mappers.JobFormMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Controller
@RequestMapping("/companies")
public class CompaniesMvcController {

    private final CompanyService companyService;
    private final AuthenticationHelper authenticationHelper;
    private final JobOpeningService jobOpeningService;
    private final EmployeeService employeeService;
    private final CompanyInfoService companyInfoService;
    private final CompanyMapper companyMapper;
    private final JobFormMapper jobFormMapper;
    private final MatchRequestService matchRequestService;

    @Autowired
    public CompaniesMvcController(CompanyService companyService,
                                  AuthenticationHelper authenticationHelper,
                                  JobOpeningService jobOpeningService,
                                  EmployeeService employeeService,
                                  CompanyInfoService companyInfoService,
                                  CompanyMapper companyMapper,
                                  JobFormMapper jobFormMapper, MatchRequestService matchRequestService) {

        this.companyService = companyService;
        this.authenticationHelper = authenticationHelper;
        this.jobOpeningService = jobOpeningService;
        this.employeeService = employeeService;
        this.companyInfoService = companyInfoService;
        this.companyMapper = companyMapper;
        this.jobFormMapper = jobFormMapper;
        this.matchRequestService = matchRequestService;
    }


    @GetMapping("/{id}")
    public String showSingleCompany(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Company company = companyService.getById(id);
            addActiveCompanyJobOpeningsAsModelToView(model, company);
            model.addAttribute("company", company);
            return "CompanyView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping()
    public String getAllPages(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        return showPageCompanies(model, session, 1);
    }

    @GetMapping("/page/{pageNumber}")
    public String showPageCompanies(Model model, HttpSession session, @PathVariable int pageNumber) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Page<Company> page = companyService.findAll(pageNumber);
            int totalPages = page.getTotalPages();
            long totalItems = page.getTotalElements();
            List<Company> companies = page.getContent();
            model.addAttribute("currentPage", pageNumber);
            model.addAttribute("totalPages", totalPages);
            model.addAttribute("totalItems", totalItems);
            model.addAttribute("companies", companies);
            return "CompaniesView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ConflictView";
        }
    }

    @GetMapping("/{id}/job-openings/{jobId}/applications")
    public String showJobOpeningsApplications(@PathVariable int id,
                                              @PathVariable int jobId,
                                              HttpSession session,
                                              Model model) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(jobId, company);
            List<MatchRequest> matchRequest = matchRequestService.getByJobOpening(employeeAuth, jobOpening);
            model.addAttribute("company", company);
            model.addAttribute("applicationsToJobOpening", matchRequest);
            return "EmployeeJobOpeningApplicationsView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    @GetMapping("/{id}/dashboard")
    public String showCompanyDashboard(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        try {
            Company company = companyService.getById(id);
            model.addAttribute("companyDashboard", company);
            return "CompanyDashboardView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/profile")
    public String showCompanyProfilePage(@PathVariable int id,
                                         Model model,
                                         HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            Company company = companyService.getById(id);

            mapCompanyInfoToModelBasedOnOptionalPresent(model, company);
            model.addAttribute("company", company);
            return "CompanyProfileView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/profile-update")
    public String showCompanyProfileEditPage(@PathVariable int id,
                                             Model model,
                                             HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }

        try {
            Company company = companyService.getById(id);
            mapCompanyInfoToModelBasedOnOptionalPresent(model, company);
            model.addAttribute("companyProfile", company);
            return "CompanyProfileUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/profile-update")
    public String editCompanyProfile(@PathVariable int id,
                                     Model model,
                                     HttpSession session,
                                     @Valid @ModelAttribute("companyInfo")
                                     CompanyInfoDtoIn companyInfoDtoIn,
                                     BindingResult companyInfoBindingResult,
                                     @Valid @ModelAttribute("companyProfile") Company company,
                                     BindingResult companyProfileBindingResult) {
        Company companyAuth;
        try {
            companyAuth = authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }

        if (companyInfoBindingResult.hasErrors() || companyProfileBindingResult.hasErrors()) {
            return "CompanyProfileUpdateView";
        }

        try {
            Company companyToUpdate = companyService.getById(id);
            CompanyInfo companyInfo = companyMapper.infoDtoToObject(companyInfoDtoIn);

            maintainCompanyDetailsFromIncompleteMvcForm(company, companyToUpdate);

            if (companyToUpdate.getCompanyInfo().isPresent()) {
                companyInfo.setCompany(company);
                companyInfoService.update(companyAuth, id, companyInfo);
            } else {
                companyInfo.setCompany(companyToUpdate);
                companyInfoService.create(companyAuth, companyInfo);
            }
            companyService.update(companyAuth, company, id);
            return "redirect:/companies/{id}/profile";
        } catch (DuplicateEntityException e) {
            companyProfileBindingResult.rejectValue(
                    format("%s", getCorrectErrorFieldFromDuplicateException(e.getMessage())),
                    "unique_value_duplication", e.getMessage());
            return "CompanyProfileUpdateView";
        } catch (LocationNotExistException e) {
            companyInfoBindingResult.rejectValue("city", "non_existent_location", e.getMessage());
            return "CompanyProfileUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/change-password")
    public String showCompanyChangePassword(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        try {
            Company company = companyService.getById(id);
            model.addAttribute("companyPassword", company);
            model.addAttribute("passwordChange", new PasswordDto());
            return "CompanyChangePasswordView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/change-password")
    public String changePassword(@PathVariable int id,
                                 HttpSession session,
                                 @Valid @ModelAttribute("passwordChange")
                                 PasswordDto passwordDto,
                                 BindingResult bindingResult) {
        Company auth;
        try {
            auth = authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        if (bindingResult.hasErrors()) {
            return "ProfessionalChangePasswordView";
        }
        try {
            companyService.changePassword(auth, passwordDto, id);
            return "redirect:/companies/{id}/dashboard";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("password_entity", "entity_title", e.getMessage());
            return "ProfessionalChangePasswordView";
        } catch (PasswordMismatchException e) {
            bindingResult.rejectValue("oldPassword", "password_mismatch", e.getMessage());
            return "ProfessionalChangePasswordView";
        } catch (PasswordDuplicateException e) {
            bindingResult.rejectValue("password_duplicate", "duplicate_password", e.getMessage());
            return "ProfessionalChangePasswordView";
        }
    }

    @GetMapping("/{companyId}/job-openings")
    public String showActiveCompanyJobOpenings(@PathVariable int companyId,
                                               Model model,
                                               HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            companyService.getById(companyId);
            return "redirect:/companies/{companyId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/job-openings/archived")
    public String showArchivedCompanyJobOpenings(@PathVariable int id,
                                                 Model model,
                                                 HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }

        try {
            Company company = companyService.getById(id);
            Set<JobOpening> jobOpeningCompany = company.getJobOpenings()
                    .stream()
                    .filter(jobOpening -> !jobOpening.isActive())
                    .collect(Collectors.toSet());
            model.addAttribute("archivedJobOpenings", jobOpeningCompany);
            return "EmployeeArchivedJobOpeningsView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/employees")
    public String showAllCompanyEmployees(@PathVariable int id,
                                          Model model,
                                          HttpSession session) {
        Company companyAuth;
        try {
            companyAuth = authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        try {
            Company company = companyService.getById(id);
            List<Employee> employees = employeeService.getAllCompanyEmployees(companyAuth, company);
            model.addAttribute("compEmployees", employees);
            return "CompanyEmployeesView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/employees/{employeeId}/dashboard")
    public String showEmployeeDashboard(@PathVariable int id,
                                        @PathVariable int employeeId,
                                        Model model,
                                        HttpSession session) {

        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            Company company = companyService.getById(id);
            Employee employee = employeeService.getCompanyEmployeeById(employeeAuth, id, employeeId);
            addActiveCompanyJobOpeningsAsModelToView(model, company);
            Map<JobOpening, Boolean> jobOpeningApplicationsCheck = matchRequestService
                    .existMatchRequestsForJobOpening(employee);
            model.addAttribute("jobApplicationCheck", jobOpeningApplicationsCheck);
            model.addAttribute("company", company);
            model.addAttribute("employeeDashboard", employee);
            return "EmployeeDashboardView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/employees/new")
    public String showCompanyCreateEmployeeView(@PathVariable int id,
                                                Model model,
                                                HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        try {
            Company company = companyService.getById(id);
            model.addAttribute("company", company);
            model.addAttribute("newEmployee", new EmployeeDtoIn());
            return "CompanyCreateEmployeesView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/{id}/employees/new")
    public String createNewEmployee(@PathVariable int id, Model model, HttpSession session,
                                    @Valid @ModelAttribute("newEmployee") EmployeeDtoIn employeeDtoIn,
                                    BindingResult bindingResult) {
        Company companyAuth;
        try {
            companyAuth = authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        if (bindingResult.hasErrors()) {
            return "CompanyCreateEmployeesView";
        }
        try {
            Company company = companyService.getById(id);
            Employee employee = companyMapper.employeeDtoToObject(employeeDtoIn);
            employee.setCompany(company);
            employeeService.create(companyAuth, employee);
            return "redirect:/companies/{id}/employees";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "duplicate_title", e.getMessage());
            return "ProfessionalCreateApplicationView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/employees/{empId}/profile")
    public String showEmployeeEditPage(HttpSession session,
                                       Model model,
                                       @PathVariable int id,
                                       @PathVariable int empId) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            Company company = companyService.getById(id);
            Employee employee = employeeService.getCompanyEmployeeById(employeeAuth, id, empId);
            EmployeeDtoIn employeeDtoIn = companyMapper.employeeDtoMvc(employee);
            model.addAttribute("employeeEdit", employeeDtoIn);
            model.addAttribute("company", company);
            return "EmployeeProfileView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/employees/{empId}/profile/update")
    public String showUpdateEmployeeEditPage(HttpSession session,
                                             Model model,
                                             @PathVariable int id,
                                             @PathVariable int empId) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            Company company = companyService.getById(id);
            Employee employee = employeeService.getCompanyEmployeeById(employeeAuth, id, empId);
            EmployeeDtoIn employeeDtoIn = companyMapper.employeeDtoMvc(employee);
            model.addAttribute("employeeEdit", employeeDtoIn);
            model.addAttribute("company", company);
            return "EmployeeUpdateProfileView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/{id}/employees/{empId}/profile/update")
    public String editEmployee(HttpSession session, Model model,
                               @PathVariable int id,
                               @PathVariable int empId,
                               @Valid @ModelAttribute("employeeEdit")
                               EmployeeDtoIn employeeDtoIn,
                               BindingResult bindingResult) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        if (bindingResult.hasErrors()) {
            return "EmployeeUpdateProfileView";
        }
        try {
            Employee employee = companyMapper.employeeDtoToObject(employeeDtoIn);
            employeeService.update(employeeAuth, employee, id, empId);
            return "redirect:/companies/{id}/employees/{empId}/profile/";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "duplicate_mail_in_db", e.getMessage());
            return "EmployeeUpdateProfileView";
        }
    }


    @GetMapping("/{id}/job-opening/new")
    public String showCreateJobOpeningByCompanyEmployee(HttpSession session,
                                                        Model model,
                                                        @PathVariable int id) {
        try {
            authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            companyService.getById(id);
            model.addAttribute("jobOpening", new JobOpeningDtoIn());
            return "EmployeeCreateJobOpeningView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/job-opening/new")
    public String createJobOpeningByCompanyEmployee(HttpSession session,
                                                    Model model,
                                                    @PathVariable int id,
                                                    @Valid @ModelAttribute("jobOpening")
                                                    JobOpeningDtoIn jobOpeningDtoIn,
                                                    BindingResult bindingResult) {

        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        if (bindingResult.hasErrors()) {
            return "EmployeeCreateJobOpeningView";
        }
        try {
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobFormMapper.dtoToObject(jobOpeningDtoIn);
            jobOpening.setCompany(company);
            JobOpening jobDb = jobOpeningService.create(employeeAuth, jobOpening);
            return "redirect:/job-openings/" + jobDb.getId();
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/job-openings/{jobId}/update")
    public String showUpdateJobOpeningByEmployee(Model model,
                                                 HttpSession session,
                                                 @PathVariable int id,
                                                 @PathVariable int jobId) {
        try {
            authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(jobId, company);
            JobOpeningDtoIn jobOpeningDtoIn = jobFormMapper.jobOpeningToDtoInMvc(jobOpening);
            model.addAttribute("jobOpening", jobOpeningDtoIn);
            return "EmployeeUpdateJobOpeningView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    @PostMapping("/{id}/job-openings/{jobId}/update")
    public String updateJobOpeningByEmployee(Model model,
                                             @Valid @ModelAttribute("jobOpening") JobOpeningDtoIn jobOpeningDtoIn,
                                             BindingResult bindingResult,
                                             @PathVariable int id,
                                             HttpSession session,
                                             @PathVariable int jobId) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        if (bindingResult.hasErrors()) {
            return "EmployeeUpdateJobOpeningView";
        }
        try {
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobFormMapper.dtoToObject(jobOpeningDtoIn);
            jobOpening.setCompany(company);
            jobOpeningService.update(employeeAuth, jobOpening, jobId);
            model.addAttribute("jobOpening", jobOpening);
            return "redirect:/companies/{id}/employees/" + employeeAuth.getId() + "/dashboard";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue("minSalary", "min_bigger_than_max", e.getMessage());
            model.addAttribute(jobOpeningDtoIn);
            return "EmployeeUpdateJobOpeningView";
        }

    }

    @GetMapping("/{id}/job-openings/{openingId}/applications/{matchId}")
    public String approveApplicationsMatchByEmployee(Model model,
                                                     HttpSession session,
                                                     @PathVariable int id,
                                                     @PathVariable int openingId,
                                                     @PathVariable int matchId) {
        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(openingId, company);
            matchRequestService.approve(employeeAuth, company, jobOpening, matchId);
            return "redirect:/companies/" + id + "/employees/" + employeeAuth.getId() + "/dashboard";
        } catch (ApplicationMismatchException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        }
    }


    @GetMapping("/{compId}/employees/{empId}/delete")
    public String deleteEmployeeOfCompany(@PathVariable int compId,
                                          @PathVariable int empId,
                                          HttpSession session,
                                          Model model) {
        Company companyAuth;
        try {
            companyAuth = authenticationHelper.tryGetCurrentCompany(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-companies";
        }
        try {
            employeeService.delete(companyAuth, compId, empId);
            return "redirect:/companies/{compId}/employees";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("job-openings/{openingId}/delete")
    public String deleteJobOpeningOfCompany(@PathVariable int openingId,
                                            HttpSession session,
                                            Model model) {

        Employee employeeAuth;
        try {
            employeeAuth = authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        try {
            jobOpeningService.deleteCompanyOpening(employeeAuth, openingId);
            return "redirect:/companies/" + employeeAuth.getCompany().getId() + "/employees/" + employeeAuth.getId() + "/dashboard";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }

    }

    private void mapCompanyInfoToModelBasedOnOptionalPresent(Model model, Company company) {
        if (company.getCompanyInfo().isPresent()) {
            CompanyInfo companyInfo = company.getCompanyInfo().get();
            CompanyInfoDtoIn companyInfoDtoIn = companyMapper.companyInfoDtoMvc(companyInfo);
            model.addAttribute("companyInfo", companyInfoDtoIn);
        } else {
            model.addAttribute("companyInfo", new CompanyInfoDtoIn());
        }
    }

    private String getCorrectErrorFieldFromDuplicateException(String errorMessage) {
        String[] errorArray = errorMessage.split(" ");
        return errorArray[4];
    }

    private void addActiveCompanyJobOpeningsAsModelToView(Model model, Company company) {
        List<JobOpening> jobOpeningCompany = company.getJobOpenings()
                .stream()
                .filter(JobOpening::isActive)
                .sorted(Comparator.comparing(JobOpening::getPublishedOn, Comparator.reverseOrder()))
                .collect(Collectors.toList());

        model.addAttribute("companyJobOpenings", jobOpeningCompany);
    }

    private void maintainCompanyDetailsFromIncompleteMvcForm(Company company, Company companyToUpdate) {
        company.setPassword(companyToUpdate.getPassword());
        company.setUsername(companyToUpdate.getUsername());
    }
}

