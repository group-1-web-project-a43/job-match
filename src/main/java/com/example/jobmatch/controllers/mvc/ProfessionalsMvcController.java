package com.example.jobmatch.controllers.mvc;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.models.dtos.dtosIn.ApplicationDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.ProfessionalInfoDtoIn;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.models.mvcforms.SkillsForm;
import com.example.jobmatch.models.mvcforms.ThresholdForm;
import com.example.jobmatch.services.interfaces.*;
import com.example.jobmatch.services.mappers.JobFormMapper;
import com.example.jobmatch.services.mappers.ProfessionalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/professionals")
public class ProfessionalsMvcController {

    private final ProfessionalService professionalService;
    private final ApplicationService applicationService;
    private final AuthenticationHelper authenticationHelper;
    private final ProfessionalMapper professionalMapper;
    private final ProfessionalInfoService professionalInfoService;
    private final JobFormMapper jobFormMapper;
    private final SkillService skillService;
    private final MatchRequestService matchRequestService;

    @Autowired
    public ProfessionalsMvcController(ProfessionalService professionalService,
                                      ApplicationService applicationService,
                                      AuthenticationHelper authenticationHelper,
                                      ProfessionalMapper professionalMapper,
                                      ProfessionalInfoService professionalInfoService,
                                      JobFormMapper jobFormMapper,
                                      SkillService skillService, MatchRequestService matchRequestService) {
        this.professionalService = professionalService;
        this.applicationService = applicationService;
        this.authenticationHelper = authenticationHelper;
        this.professionalMapper = professionalMapper;
        this.professionalInfoService = professionalInfoService;
        this.jobFormMapper = jobFormMapper;
        this.skillService = skillService;
        this.matchRequestService = matchRequestService;
    }

    @ModelAttribute("skills")
    public Set<Skill> populateSkills() {
        return new HashSet<>(skillService.getAll());
    }

    @GetMapping
    public String showAllProfessionals(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("professionals", professionalService.getAll());
            return "ProfessionalsView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}")
    public String showSingleProfessional(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Professional professional = professionalService.getById(id);
            model.addAttribute("professional", professional);
            return "ProfessionalView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/offers")
    public String showProfessionalAllJobOffers(Model model,
                                               @PathVariable int id,
                                               HttpSession session) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Professional professional = professionalService.getById(id);
            Map<MatchRequest, Boolean> matchRequests =
                    matchRequestService.getByProfessional(professionalAuth, professional);
            model.addAttribute("matchRequests", matchRequests);
            return "View";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }


    @GetMapping("/{id}/dashboard")
    public String showProfessionalDashboard(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Professional professional = professionalService.getById(id);
            List<MatchRequest> matchRequests = matchRequestService.getByProfessionalApplication(professional);
            model.addAttribute("professionalAppCount", matchRequests.size());
            model.addAttribute("professionaldashboard", professional);
            model.addAttribute("professionalApplications", professional.getApplications().size());
            return "ProfessionalDashboardView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/{id}/change-password")
    public String showProfessionalChangePassword(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            model.addAttribute("professional", professionalService.getById(id));
            model.addAttribute("passwordChange", new PasswordDto());
            return "ProfessionalChangePasswordView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/change-password")
    public String changeProfessionalPassword(@PathVariable int id,
                                             @Valid @ModelAttribute("passwordChange") PasswordDto passwordDto,
                                             BindingResult bindingResult, HttpSession session, Model model) {

        Professional authenticated;
        try {
            authenticated = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        if (bindingResult.hasErrors()) {
            return "ProfessionalChangePasswordView";
        }
        try {
            professionalService.changePassword(authenticated, passwordDto, id);
            return "redirect:/professionals/{id}/dashboard";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("password_entity", "entity_title", e.getMessage());
            return "ProfessionalChangePasswordView";
        } catch (PasswordMismatchException e) {
            bindingResult.rejectValue("oldPassword", "password_mismatch", e.getMessage());
            return "ProfessionalChangePasswordView";
        } catch (PasswordDuplicateException e) {
            bindingResult.rejectValue("newPasswordConfirmation", "duplicate_password", e.getMessage());
            return "ProfessionalChangePasswordView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{profId}/applications")
    public String showProfessionalApplications(@PathVariable int profId, Model model, HttpSession session) {
        Professional profAuth;
        try {
            profAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Set<Application> applications = professionalService.getAllProfessionalApplications(profAuth, profId);
            model.addAttribute("appProf", applications);
            return "ProfessionalAppliedJobOpeningsView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{id}/profile")
    public String showProfessionalProfilePage(@PathVariable int id,
                                              Model model,
                                              HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {

            Professional professional = professionalService.getById(id);

            if (professional.getProfessionalInfo().isPresent()) {
                ProfessionalInfo professionalInfo = professional.getProfessionalInfo().get();
                ProfessionalInfoDtoIn professionalInfoDtoIn = professionalMapper.professionalInfoDtoMvc(professionalInfo);
                model.addAttribute("professionalInfo", professionalInfoDtoIn);
            } else {
                model.addAttribute("professionalInfo", new ProfessionalInfoDtoIn());
            }
            model.addAttribute("professional", professional);
            return "ProfessionalProfileView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/profile-update")
    public String showProfessionalProfileEditPage(@PathVariable int id,
                                                  Model model,
                                                  HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Professional professional = professionalService.getById(id);
            if (professional.getProfessionalInfo().isPresent()) {
                ProfessionalInfo professionalInfo = professional.getProfessionalInfo().get();
                ProfessionalInfoDtoIn professionalInfoDtoIn = professionalMapper.professionalInfoDtoMvc(professionalInfo);
                model.addAttribute("profInfo", professionalInfoDtoIn);
            } else {
                model.addAttribute("profInfo", new ProfessionalInfoDtoIn());
            }
            model.addAttribute("professionalprofile", professional);
            return "ProfessionalProfileUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/profile-update")
    public String editProfessionalProfile(@PathVariable int id,
                                          Model model,
                                          HttpSession session,
                                          @Valid @ModelAttribute("professionalprofile") Professional professional,
                                          BindingResult professionalProfileBindingResult,
                                          @Valid @ModelAttribute("profInfo") ProfessionalInfoDtoIn professionalInfoDtoIn,
                                          BindingResult professionalInfoBindingResult) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        if (professionalProfileBindingResult.hasErrors() || professionalInfoBindingResult.hasErrors()) {
            return "ProfessionalProfileUpdateView";
        }

        try {
            Professional professionalToUpdate = professionalService.getById(id);
            maintainProfessionalDetailsFromIncompleteMvcForm(professionalToUpdate, professional);

            ProfessionalInfo professionalInfo = professionalMapper.professionalInfoDtoToObject(professionalInfoDtoIn);
            professionalInfo.setProfessional(professional);

            if (professionalToUpdate.getProfessionalInfo().isPresent()) {
                professionalInfoService.update(professionalAuth, professionalInfo, id);
            } else {
                professionalInfoService.create(professionalAuth, professionalToUpdate, professionalInfo);
            }
            professionalService.update(professionalAuth, professional, id);
            return "redirect:/professionals/{id}/profile";
        } catch (DuplicateEntityException e) {
            professionalProfileBindingResult.rejectValue("email", "duplicate_email", e.getMessage());
            return "ProfessionalProfileUpdateView";
        } catch (LocationNotExistException e) {
            professionalInfoBindingResult.rejectValue("city", "non_existent_location", e.getMessage());
            return "ProfessionalProfileUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/applications")
    public String showAllApplications(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }

        List<Application> applicationList = applicationService.getAll()
                .stream()
                .filter(application -> application.getApplicationStatus() == ApplicationStatus.ACTIVE)
                .collect(Collectors.toList());
        model.addAttribute("applications", applicationList);
        model.addAttribute("appsFilterForm", new ApplicationFilterOptions());
        return "ApplicationsView";
    }

    @PostMapping("/applications")
    public String filterApplications(HttpSession session,
                                     @ModelAttribute ApplicationFilterOptions filterOptions,
                                     Model model) {
        try {
            authenticationHelper.tryGetCurrentEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-employees";
        }
        List<Application> applicationList = applicationService.searchActiveApplications(filterOptions);
        model.addAttribute("applications", applicationList);
        model.addAttribute("appsFilterForm", new ApplicationFilterOptions());
        return "ApplicationsView";
    }

    @GetMapping("/{profId}/applications/{appId}")
    public String showSingleApplication(@PathVariable int profId,
                                        @PathVariable int appId,
                                        HttpSession session,
                                        Model model) {

        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);
            model.addAttribute("applicationsingle", application);
            model.addAttribute("form", new SkillsForm());
            model.addAttribute("skillRemove", new Skill());
            model.addAttribute("thresholdForm", new ThresholdForm());
            return "ApplicationView";
        } catch (ApplicationMismatchException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/{profId}/applications/{appId}/potential-matches")
    public String showApplicationPotentialMatches(@PathVariable int profId,
                                                  @PathVariable int appId,
                                                  @Valid @ModelAttribute("thresholdForm") ThresholdForm thresholdForm,
                                                  BindingResult bindingResult,
                                                  HttpSession session,
                                                  Model model) {
        Professional profAuth;
        try {
            profAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        if(bindingResult.hasErrors()) {
            return "redirect:/professionals/{profId}/applications/{appId}";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);
            List<JobOpening> matchingJobOpenings = applicationService.getPotentialMatchingJobOpenings(
                    profAuth, application, thresholdForm.getSalaryThreshold(), thresholdForm.getSkillThreshold());
            model.addAttribute("jobs", matchingJobOpenings);
            model.addAttribute("jobsFilterForm", new JobFilterOptions());
            return "JobOpeningsView";
        } catch (ApplicationMismatchException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue(getCorrectThresholdErrorField(e.getMessage()),
                    "threshold_validation", e.getMessage());
            model.addAttribute(thresholdForm);
            return "ApplicationView";
        } catch (IllegalStateException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        }
    }



    @GetMapping("/{profId}/applications/new")
    public String showCreateApplicationPage(HttpSession session, Model model, @PathVariable int profId) {
        try {
            authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            Professional professional = professionalService.getById(profId);

            model.addAttribute("profId", professional);
            model.addAttribute("newapplication", new ApplicationDtoIn());
            return "ProfessionalCreateApplicationView";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @PostMapping("/{profId}/applications/new")
    public String createNewApplication(@Valid @ModelAttribute("newapplication") ApplicationDtoIn applicationDtoIn,
                                       BindingResult bindingResult,
                                       Model model,
                                       HttpSession session,
                                       @PathVariable int profId) {

        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        if (bindingResult.hasErrors()) {
            return "ProfessionalCreateApplicationView";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = jobFormMapper.applicationDtoToObject(applicationDtoIn);
            application.setProfessional(professional);
            Application applicationDb = applicationService.create(professionalAuth, application);
            return "redirect:/professionals/{profId}/applications/" + applicationDb.getId();
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (LocationNotExistException e) {
            bindingResult.rejectValue("city", "non_existent_location", e.getMessage());
            model.addAttribute(applicationDtoIn);
            return "ProfessionalCreateApplicationView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue("minSalary", "min_bigger_than_max", e.getMessage());
            model.addAttribute(applicationDtoIn);
            return "ProfessionalCreateApplicationView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{profId}/applications/{appId}/update")
    public String showEditApplicationPage(@PathVariable int profId,
                                          @PathVariable int appId,
                                          HttpSession session,
                                          Model model) {

        try {
            authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);
            ApplicationDtoIn applicationDtoIn = jobFormMapper.applicationToDtoInMvc(application);


            model.addAttribute("professional", professional);
            model.addAttribute("applicationUpdate", applicationDtoIn);
            return "ProfessionalUpdateApplicationView";
        } catch (ApplicationMismatchException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{profId}/applications/{appId}/update")
    public String updateApplication(@PathVariable int profId,
                                    @PathVariable int appId,
                                    HttpSession session,
                                    @Valid @ModelAttribute("applicationUpdate")
                                    ApplicationDtoIn applicationDtoIn,
                                    BindingResult bindingResult,
                                    Model model) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        if (bindingResult.hasErrors()) {
            return "ProfessionalUpdateApplicationView";
        }
        try {
            Professional professional = professionalService.getById(profId);
            Application application = jobFormMapper.applicationDtoToObject(applicationDtoIn);
            application.setProfessional(professional);
            applicationService.update(professionalAuth, application, appId);
            return "redirect:/professionals/{profId}/applications/{appId}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (LocationNotExistException e) {
            bindingResult.rejectValue("city", "non_existent_location", e.getMessage());
            return "ProfessionalUpdateApplicationView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue("minSalary", "min_bigger_than_max", e.getMessage());
            return "ProfessionalUpdateApplicationView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{profId}/applications/{appId}/add-skills")
    public String addOrModifySkillInApplication(@PathVariable int profId,
                                                @PathVariable int appId,
                                                @Valid @ModelAttribute("form") SkillsForm form,
                                                BindingResult bindingResult,
                                                HttpSession session,
                                                Model model) {

        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/professionals/{profId}/applications/{appId}";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);
            Skill skill = skillService.getByName(form.getSkill());
            SkillLevel skillLevel = jobFormMapper.parseSkillLevel(form.getLevel());

            applicationService.addOrModifySkill(professionalAuth, application, skill, skillLevel);
            return "redirect:/professionals/{profId}/applications/{appId}";
        } catch (ApplicationMismatchException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("skill", "duplicate_skill", e.getMessage());
            return "redirect:/professionals/{profId}/applications/{appId}";
        }
    }

    @PostMapping("/{profId}/applications/{appId}/remove-skills")
    public String removeSkillFromApplication(@PathVariable int profId,
                                             @PathVariable int appId,
                                             @Valid @ModelAttribute("skillRemove") Skill skillStr,
                                             HttpSession session,
                                             BindingResult bindingResult,
                                             Model model) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/professionals/{profId}/applications/{appId}";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);
            Skill skill = skillService.getByName(skillStr.getSkillName());

            applicationService.removeSkill(professionalAuth, application, skill);
            return "redirect:/professionals/{profId}/applications/{appId}";
        } catch (ApplicationMismatchException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (IllegalArgumentException e) {
            bindingResult.rejectValue("skillName", "skill_not_found", e.getMessage());
            return "redirect:/professionals/{profId}/applications/{appId}";
        }
    }

    @GetMapping("/{profId}/applications/{appId}/main")
    public String setMainApplication(@PathVariable int profId,
                                     @PathVariable int appId,
                                     HttpSession session,
                                     Model model) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }

        try {
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);
            professionalService.setMainApplication(professionalAuth, application, professional);
            return "redirect:/professionals/{profId}/applications";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "ConflictView";
        } catch (EntityNotFoundException | ApplicationMismatchException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/{profId}/applications/{appId}/delete")
    public String deleteProfessionalApplication(@PathVariable int profId,
                                                @PathVariable int appId,
                                                HttpSession session, Model model) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Professional professional = professionalService.getById(profId);
            applicationService.delete(professionalAuth, professional, appId);
            return "redirect:/professionals/{profId}/applications";
        } catch (ApplicationMismatchException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{profId}/offers/{matchId}/accept")
    public String acceptJobOffer(HttpSession session,
                                 @PathVariable int profId,
                                 @PathVariable int matchId,
                                 Model model) {
        Professional professionalAuth;
        try {
            professionalAuth = authenticationHelper.tryGetCurrentProf(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login-professionals";
        }
        try {
            Professional professional = professionalService.getById(profId);
            matchRequestService.acceptJobOffer(professionalAuth, professional, matchId);
            model.addAttribute("professional", professional);
            return "redirect:/professionals/{profId}/dashboard";
        } catch (ApplicationMismatchException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ForbiddenView";
        }

    }

    private void maintainProfessionalDetailsFromIncompleteMvcForm(Professional professionalToUpdate,
                                                                  Professional professional) {
        professional.setUsername(professionalToUpdate.getUsername());
        professional.setPassword(professionalToUpdate.getPassword());
    }

    private String getCorrectThresholdErrorField(String message) {
        String[] strArr = message.split(" ");
        if(strArr[0].equalsIgnoreCase("salary")) {
            return "salaryThreshold";
        }
        return "skillThreshold";
    }
}
