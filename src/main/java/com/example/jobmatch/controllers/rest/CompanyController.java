package com.example.jobmatch.controllers.rest;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EmptySearchException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.CompanyInfo;
import com.example.jobmatch.models.Employee;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.dtos.dtosIn.CompanyDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.CompanyInfoDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.EmployeeDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.JobOpeningDtoIn;
import com.example.jobmatch.models.dtos.dtosOut.*;
import com.example.jobmatch.services.interfaces.*;
import com.example.jobmatch.services.mappers.CompanyMapper;
import com.example.jobmatch.services.mappers.JobFormMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/companies")
public class CompanyController {

    private final CompanyService companyService;
    private final CompanyInfoService companyInfoService;
    private final EmployeeService employeeService;

    private final JobOpeningService jobOpeningService;

    private final MatchRequestService matchRequestService;

    private final JobFormMapper jobFormMapper;
    private final CompanyMapper companyMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CompanyController(CompanyService companyService,
                             CompanyInfoService companyInfoService,
                             EmployeeService employeeService,
                             JobOpeningService jobOpeningService,
                             MatchRequestService matchRequestService, JobFormMapper jobFormMapper,
                             CompanyMapper companyMapper,
                             AuthenticationHelper authenticationHelper) {

        this.companyService = companyService;
        this.companyInfoService = companyInfoService;
        this.employeeService = employeeService;
        this.jobOpeningService = jobOpeningService;
        this.matchRequestService = matchRequestService;
        this.jobFormMapper = jobFormMapper;
        this.companyMapper = companyMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<CompanyInfoDtoOut> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetLogged(headers);
        return companyInfoService.getAll()
                .stream()
                .map(companyMapper::infoObjectToDto)
                .collect(Collectors.toList());
    }


    @GetMapping("/filter")
    public List<CompanyDtoOut> filterByName(@RequestHeader HttpHeaders headers,
                                            @RequestParam(name = "search", required = false) Optional<String> keyword,
                                            @RequestParam(name = "fiter", required = false) Optional<String> filterBy,
                                            @RequestParam(name = "sort", required = false) Optional<String> sortBy,
                                            @RequestParam(name = "desc", required = false) Optional<Boolean> desc,
                                            @RequestParam(name = "page", required = false) Optional<Integer> page,
                                            @RequestParam(name = "size", required = false) Optional<Integer> size) {

        try {
            authenticationHelper.tryGetLogged(headers);
            return companyService.searchCompanyByName(keyword, filterBy, sortBy, desc, page, size)
                    .stream()
                    .map(companyMapper::objectToDto)
                    .collect(Collectors.toList());
        } catch (EmptySearchException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CompanyInfoDtoOut get(@RequestHeader HttpHeaders headers,
                                 @PathVariable int id) {
        try {
            authenticationHelper.tryGetLogged(headers);
            return companyMapper.infoObjectToDto(companyInfoService.getInfoByCompanyId(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/job-openings")
    public Set<JobOpeningDtoOut> getActiveCompanyJobOpenings(@RequestHeader HttpHeaders headers,
                                                             @PathVariable int id) {
        try {
            authenticationHelper.tryGetLogged(headers);
            Company company = companyService.getById(id);
            return company.getJobOpenings()
                    .stream()
                    .filter(JobOpening::isActive)
                    .map(jobFormMapper::objectToDto)
                    .collect(Collectors.toSet());

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/job-openings/archived")
    public Set<JobOpeningDtoOut> getArchivedCompanyJobOpenings(@RequestHeader HttpHeaders headers,
                                                               @PathVariable int id) {
        try {
            authenticationHelper.tryGetLogged(headers);
            Company company = companyService.getById(id);
            return company.getJobOpenings()
                    .stream()
                    .filter(jobOpening -> !jobOpening.isActive())
                    .map(jobFormMapper::objectToDto)
                    .collect(Collectors.toSet());

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/job-openings/{jobId}")
    public JobOpeningDtoOut getCompanyJobOpening(@RequestHeader HttpHeaders headers,
                                                 @PathVariable int id,
                                                 @PathVariable int jobId) {

        try {
            authenticationHelper.tryGetLogged(headers);
            Company company = companyService.getById(id);
            return jobFormMapper.objectToDto(jobOpeningService.getByIdAndCompany(jobId, company));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/job-openings/{jobId}/potential-matches")
    public List<ApplicationDtoOut> getPotentialMatches(@RequestHeader HttpHeaders headers,
                                                       @PathVariable int id,
                                                       @PathVariable int jobId,
                                                       @RequestParam(name = "salary_threshold", required = false)
                                                       Optional<Double> salaryThreshold,
                                                       @RequestParam(name = "skill_threshold", required = false)
                                                       Optional<Integer> skillThreshold) {
        try {
            Employee employeeAuth = authenticationHelper.tryGetEmployee(headers);
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobOpeningService.getByIdAndCompany(jobId, company);

            if (salaryThreshold.isEmpty()) salaryThreshold = Optional.of(0.0);
            if (skillThreshold.isEmpty()) skillThreshold = Optional.of(0);

            return jobOpeningService.getPotentialMatchingApplications(employeeAuth, jobOpening,
                            salaryThreshold.get(), skillThreshold.get())
                    .stream()
                    .map(jobFormMapper::applicationObjectToDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}/job-openings/{jobId}/applications")
    public List<MatchRequestDtoOut> getApplicationsToJobOpening(@RequestHeader HttpHeaders headers,
                                                                @PathVariable int id,
                                                                @PathVariable int jobId) {
        try {
            Employee employeeAuth = authenticationHelper.tryGetEmployee(headers);
            companyService.getById(id);
            JobOpening jobOpening = jobOpeningService.getById(jobId);

            return matchRequestService.getByJobOpening(employeeAuth, jobOpening)
                    .stream().map(jobFormMapper::matchObjectToDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/employees")
    public List<EmployeeDtoOut> getCompanyEmployees(@RequestHeader HttpHeaders headers,
                                                    @PathVariable int id) {

        Company companyAuth = authenticationHelper.tryGetCompany(headers);

        try {
            Company companyToView = companyService.getById(id);
            return employeeService.getAllCompanyEmployees(companyAuth, companyToView)
                    .stream()
                    .map(companyMapper::employeeObjectToDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/{id}/employees/{employeeId}")
    public EmployeeDtoOut getEmployee(@RequestHeader HttpHeaders headers,
                                      @PathVariable int id,
                                      @PathVariable int employeeId) {

        Employee employee = authenticationHelper.tryGetEmployee(headers);

        try {
            return companyMapper.employeeObjectToDto(employeeService.getCompanyEmployeeById(employee, id, employeeId));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public CompanyDtoOut create(@Valid @RequestBody CompanyDtoIn companyDtoIn) {
        try {
            Company company = companyMapper.dtoToObject(companyDtoIn);
            return companyMapper.objectToDto(companyService.create(company));
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/{id}/info")
    public CompanyInfoDtoOut createInfo(@RequestHeader HttpHeaders headers,
                                        @PathVariable int id,
                                        @Valid @RequestBody CompanyInfoDtoIn companyInfoDtoIn) {
        try {
            Company companyAuth = authenticationHelper.tryGetCompany(headers);

            Company company = companyService.getById(id);
            CompanyInfo companyInfo = companyMapper.infoDtoToObject(companyInfoDtoIn);
            companyInfo.setCompany(company);
            return companyMapper.infoObjectToDto(companyInfoService.create(companyAuth, companyInfo));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/employees")
    public EmployeeDtoOut createCompanyEmployee(@RequestHeader HttpHeaders headers,
                                                @PathVariable int id,
                                                @Valid @RequestBody EmployeeDtoIn employeeDtoIn) {
        try {
            Company companyAuth = authenticationHelper.tryGetCompany(headers);
            Company company = companyService.getById(id);
            Employee employee = companyMapper.employeeDtoToObject(employeeDtoIn);
            employee.setCompany(company);
            return companyMapper.employeeObjectToDto(employeeService.create(companyAuth, employee));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/job-openings")
    public JobOpeningDtoOut createCompanyJobOpening(@RequestHeader HttpHeaders headers,
                                                    @PathVariable int id,
                                                    @Valid @RequestBody JobOpeningDtoIn jobOpeningDto) {
        try {
            Employee employeeAuth = authenticationHelper.tryGetEmployee(headers);

            Company company = companyService.getById(id);
            JobOpening jobOpening = jobFormMapper.dtoToObject(jobOpeningDto);
            jobOpening.setCompany(company);
            jobOpening.setActive(true);
            return jobFormMapper.objectToDto(jobOpeningService.create(employeeAuth, jobOpening));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/job-openings/{jobId}")
    public JobOpeningDtoOut updateCompanyJobOpening(@RequestHeader HttpHeaders headers,
                                                    @PathVariable int id,
                                                    @PathVariable int jobId,
                                                    @Valid @RequestBody JobOpeningDtoIn jobOpeningDtoIn) {
        try {
            Employee employeeAuth = authenticationHelper.tryGetEmployee(headers);

            Company company = companyService.getById(id);
            JobOpening jobOpening = jobFormMapper.dtoToObject(jobOpeningDtoIn);
            jobOpening.setCompany(company);
            return jobFormMapper.objectToDto(jobOpeningService.update(employeeAuth, jobOpening, jobId));

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public CompanyDtoOut update(@RequestHeader HttpHeaders headers,
                                @PathVariable int id,
                                @Valid @RequestBody CompanyDtoIn companyDtoIn) {
        try {
            Company companyAuth = authenticationHelper.tryGetCompany(headers);

            Company company = companyMapper.dtoToObject(companyDtoIn);
            return companyMapper.objectToDto(companyService.update(companyAuth, company, id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/info")
    public CompanyInfoDtoOut updateInfo(@RequestHeader HttpHeaders headers,
                                        @PathVariable int id,
                                        @Valid @RequestBody CompanyInfoDtoIn companyInfoDtoIn) {
        try {
            Company companyAuth = authenticationHelper.tryGetCompany(headers);

            CompanyInfo companyInfo = companyMapper.infoDtoToObject(companyInfoDtoIn);
            return companyMapper.infoObjectToDto(companyInfoService.update(companyAuth, id, companyInfo));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PutMapping("/{id}/job-openings/{openingId}/applications/{matchId}")
    public MatchRequestDtoOut approveApplicationMatch(@RequestHeader HttpHeaders headers,
                                                      @PathVariable int id,
                                                      @PathVariable int openingId,
                                                      @PathVariable int matchId) {
        try {
            Employee employee = authenticationHelper.tryGetEmployee(headers);
            Company company = companyService.getById(id);
            JobOpening jobOpening = jobOpeningService.getById(openingId);

            return jobFormMapper.matchObjectToDto(matchRequestService.approve(employee, company, jobOpening, matchId));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED);
        }
    }

    @PutMapping("/{id}/employees/{employeeId}")
    public EmployeeDtoOut updateCompanyEmployee(@RequestHeader HttpHeaders headers,
                                                @PathVariable int id,
                                                @PathVariable int employeeId,
                                                @Valid @RequestBody EmployeeDtoIn employeeDtoIn) {
        try {
            Employee employeeAuth = authenticationHelper.tryGetEmployee(headers);
            Employee employee = companyMapper.employeeDtoToObject(employeeDtoIn);
            return companyMapper.employeeObjectToDto(employeeService.update(employeeAuth, employee, id, employeeId));

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            Company company = authenticationHelper.tryGetCompany(headers);
            companyService.delete(company, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/info")
    public void deleteInfo(@RequestHeader HttpHeaders headers,
                           @PathVariable int id) {
        try {
            Company companyAuth = authenticationHelper.tryGetCompany(headers);
            companyInfoService.delete(companyAuth, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/employees/{employeeId}")
    public void deleteEmployee(@RequestHeader HttpHeaders headers,
                               @PathVariable int id,
                               @PathVariable int employeeId) {
        try {
            Company companyAuth = authenticationHelper.tryGetCompany(headers);
            employeeService.delete(companyAuth, id, employeeId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/job-openings/{openingId}")
    public void deleteJobOpening(@RequestHeader HttpHeaders headers,
                                 @PathVariable int id,
                                 @PathVariable int openingId) {
        try {
            Employee employeeAuth = authenticationHelper.tryGetEmployee(headers);
            jobOpeningService.deleteCompanyOpening(employeeAuth, openingId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
