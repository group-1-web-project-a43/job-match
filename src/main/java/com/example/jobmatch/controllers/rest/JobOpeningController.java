package com.example.jobmatch.controllers.rest;

import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.dtos.dtosOut.JobOpeningDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.MatchRequestDtoOut;
import com.example.jobmatch.services.interfaces.ApplicationService;
import com.example.jobmatch.services.interfaces.JobOpeningService;
import com.example.jobmatch.services.interfaces.MatchRequestService;
import com.example.jobmatch.services.mappers.JobFormMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/job-openings")
public class JobOpeningController {

    private final JobOpeningService jobOpeningService;
    private final JobFormMapper jobFormMapper;

    private final ApplicationService applicationService;

    private final MatchRequestService matchRequestService;
    private final AuthenticationHelper authenticationHelper;


    public JobOpeningController(JobOpeningService jobOpeningService, JobFormMapper jobFormMapper, ApplicationService applicationService, MatchRequestService matchRequestService, AuthenticationHelper authenticationHelper) {
        this.jobOpeningService = jobOpeningService;
        this.jobFormMapper = jobFormMapper;
        this.applicationService = applicationService;
        this.matchRequestService = matchRequestService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<JobOpeningDtoOut> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetLogged(headers);
        return jobOpeningService.getAll()
                .stream()
                .map(jobFormMapper::objectToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public JobOpeningDtoOut get(@RequestHeader HttpHeaders headers,
                                @PathVariable int id) {
        try {
            authenticationHelper.tryGetLogged(headers);
            return jobFormMapper.objectToDto(jobOpeningService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<JobOpeningDtoOut> searchJobOpenings(@RequestHeader HttpHeaders headers,
                                                    @RequestParam(required = false) String name,
                                                    @RequestParam(required = false) String description,
                                                    @RequestParam(required = false) String location,
                                                    @RequestParam(required = false) String company,
                                                    @RequestParam(required = false) Double minSalary,
                                                    @RequestParam(required = false) Double maxSalary,
                                                    @RequestParam(required = false) String active,
                                                    @RequestParam(required = false) String sortBy,
                                                    @RequestParam(required = false) String sortOrder
    ) {
        try {
            authenticationHelper.tryGetLogged(headers);

            return jobOpeningService.searchJobOpening(name, description, location, company,
                    minSalary, maxSalary, active,
                    sortBy, sortOrder).stream().map(jobFormMapper::objectToDto).collect(Collectors.toList());

        } catch (EmptySearchException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/apply")
    public MatchRequestDtoOut applyWithMainApplication(@RequestHeader HttpHeaders headers,
                                                       @PathVariable int id) {
        try {
            Professional professional = authenticationHelper.tryGetProfessional(headers);
            JobOpening jobOpening = jobOpeningService.getById(id);

            if (professional.getMainApplication().isEmpty()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Main application not found.");
            }

            return jobFormMapper.matchObjectToDto(matchRequestService.create(
                    professional,
                    jobOpening,
                    professional.getMainApplication().get()));

        } catch (EntityNotFoundException | ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/apply/{appId}")
    public MatchRequestDtoOut applyWithSelectApplication(@RequestHeader HttpHeaders headers,
                                                         @PathVariable int id,
                                                         @PathVariable int appId) {

        try {
            Professional professional = authenticationHelper.tryGetProfessional(headers);
            JobOpening jobOpening = jobOpeningService.getById(id);
            Application application = applicationService.getById(appId);

            return jobFormMapper.matchObjectToDto(matchRequestService.create(professional, jobOpening, application));
        } catch (EntityNotFoundException | ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
