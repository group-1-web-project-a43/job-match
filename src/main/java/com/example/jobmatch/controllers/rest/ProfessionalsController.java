package com.example.jobmatch.controllers.rest;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.helpers.AuthenticationHelper;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.MatchRequest;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.ProfessionalInfo;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.models.dtos.dtosIn.ApplicationDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.ProfessionalDtoIn;
import com.example.jobmatch.models.dtos.dtosIn.ProfessionalInfoDtoIn;
import com.example.jobmatch.models.dtos.dtosOut.ApplicationDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.JobOpeningDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.ProfessionalDtoOut;
import com.example.jobmatch.models.dtos.dtosOut.ProfessionalInfoDtoOut;
import com.example.jobmatch.services.interfaces.ApplicationService;
import com.example.jobmatch.services.interfaces.MatchRequestService;
import com.example.jobmatch.services.interfaces.ProfessionalInfoService;
import com.example.jobmatch.services.interfaces.ProfessionalService;
import com.example.jobmatch.services.mappers.JobFormMapper;
import com.example.jobmatch.services.mappers.ProfessionalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/professionals")
public class ProfessionalsController {

    public static final String MAIN_APPLICATION_NOT_FOUND = "Main application not found.";
    private final ApplicationService applicationService;
    private final ProfessionalService professionalService;
    private final ProfessionalInfoService professionalInfoService;

    private final MatchRequestService matchRequestService;
    private final JobFormMapper applicationMapper;
    private final ProfessionalMapper professionalMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public ProfessionalsController(ProfessionalService professionalService,
                                   ApplicationService applicationService,
                                   ProfessionalInfoService professionalInfoService,
                                   MatchRequestService matchRequestService, JobFormMapper applicationMapper,
                                   ProfessionalMapper professionalMapper,
                                   AuthenticationHelper authenticationHelper) {

        this.applicationService = applicationService;
        this.professionalService = professionalService;
        this.professionalInfoService = professionalInfoService;
        this.matchRequestService = matchRequestService;
        this.applicationMapper = applicationMapper;
        this.professionalMapper = professionalMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<ProfessionalDtoOut> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetLogged(headers);
        return professionalService.getAll()
                .stream()
                .map(professionalMapper::professionalObjectToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/filter")
    public List<ProfessionalDtoOut> filterByName(@RequestHeader HttpHeaders headers,
                                                 @RequestParam(name = "search", required = false) Optional<String> keyword,
                                                 @RequestParam(name = "sort", required = false) Optional<String> sortBy,
                                                 @RequestParam(name = "desc", required = false) Optional<Boolean> desc,
                                                 @RequestParam(name = "page", required = false) Optional<Integer> page,
                                                 @RequestParam(name = "size", required = false) Optional<Integer> size) {

        try {
            authenticationHelper.tryGetLogged(headers);
            return professionalService.searchProfessionalByName(keyword, sortBy, desc, page, size)
                    .stream()
                    .map(professionalMapper::professionalObjectToDto)
                    .collect(Collectors.toList());
        } catch (EmptySearchException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/offers")
    public Map<MatchRequest, Boolean> getJobOffersByProfessional(@RequestHeader HttpHeaders headers,
                                                                 @PathVariable int id) {
        try {
            Professional professionalAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(id);

            return matchRequestService.getByProfessional(professionalAuth, professional);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/applications/{appId}/potential-matches")
    public List<JobOpeningDtoOut> getPotentialMatches(@RequestHeader HttpHeaders headers,
                                                      @PathVariable int id,
                                                      @PathVariable int appId,
                                                      @RequestParam(name = "salary_threshold", required = false)
                                                      Optional<Double> salaryThreshold,
                                                      @RequestParam(name = "skill_threshold", required = false)
                                                      Optional<Integer> skillThreshold) {

        try {
            Professional professionalAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(id);
            Application application = applicationService.getProfessionalApplicationById(professional, appId);

            if (salaryThreshold.isEmpty()) salaryThreshold = Optional.of(0.0);
            if (skillThreshold.isEmpty()) skillThreshold = Optional.of(0);

            return applicationService
                    .getPotentialMatchingJobOpenings(
                            professionalAuth,
                            application,
                            salaryThreshold.get(),
                            skillThreshold.get())
                    .stream()
                    .map(applicationMapper::objectToDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException | ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalStateException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ProfessionalDtoOut get(@RequestHeader HttpHeaders headers,
                                  @PathVariable int id) {
        try {
            authenticationHelper.tryGetLogged(headers);
            Professional professional = professionalService.getById(id);
            return professionalMapper.professionalObjectToDto(professional);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/info")
    public ProfessionalInfoDtoOut getInfo(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetLogged(headers);
            Professional professional = professionalService.getById(id);

            ProfessionalInfo professionalInfo = professional.getProfessionalInfo().
                    orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

            return professionalMapper.professionalInfoObjectToDto(professionalInfo);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("applications/filter")
    public List<ApplicationDtoOut> searchApplications(@RequestHeader HttpHeaders headers,
                                                      @RequestParam(required = false) String name,
                                                      @RequestParam(required = false) String description,
                                                      @RequestParam(required = false) String location,
                                                      @RequestParam(required = false) Double minSalary,
                                                      @RequestParam(required = false) Double maxSalary,
                                                      @RequestParam(required = false) String sortBy,
                                                      @RequestParam(required = false) String sortOrder) {
        try {
            authenticationHelper.tryGetLogged(headers);
            return applicationService.searchActiveApplications(name,
                            description,
                            location,
                            minSalary,
                            maxSalary,
                            sortBy,
                            sortOrder)
                    .stream()
                    .map(applicationMapper::applicationObjectToDto)
                    .collect(Collectors.toList());

        } catch (EmptySearchException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("{profId}/applications/main")
    public ApplicationDtoOut getMainApplication(@RequestHeader HttpHeaders headers,
                                                @PathVariable int profId) {
        try {
            authenticationHelper.tryGetLogged(headers);
            Professional professional = professionalService.getById(profId);

            if (professional.getMainApplication().isEmpty()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, MAIN_APPLICATION_NOT_FOUND);
            }

            Application application = professional.getMainApplication().get();

            if (application.getApplicationStatus().equals(ApplicationStatus.PRIVATE)) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, MAIN_APPLICATION_NOT_FOUND);
            }
            return applicationMapper.applicationObjectToDto(application);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ApplicationMismatchException exception) {
            throw new ResponseStatusException(HttpStatus.IM_USED, exception.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/applications")
    public Set<ApplicationDtoOut> getProfessionalApplications(@RequestHeader HttpHeaders headers,
                                                              @PathVariable int id) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);

            return professionalService.getAllProfessionalApplications(profAuth, id).stream()
                    .map(applicationMapper::applicationObjectToDto)
                    .collect(Collectors.toSet());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/applications/{appId}")
    public ApplicationDtoOut getProfessionalApplication(@RequestHeader HttpHeaders headers,
                                                        @PathVariable int id,
                                                        @PathVariable int appId) {
        try {
            authenticationHelper.tryGetLogged(headers);
            Professional professional = professionalService.getById(id);

            return applicationMapper.applicationObjectToDto(
                    applicationService.getProfessionalApplicationById(professional, appId));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public ProfessionalDtoOut create(@Valid @RequestBody ProfessionalDtoIn professionalDtoIn) {
        try {
            Professional professional = professionalMapper.professionalDtoToObject(professionalDtoIn);
            professionalService.create(professional);
            return professionalMapper.professionalObjectToDto(professional);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("{id}/info")
    public ProfessionalInfoDtoOut createInfo(@RequestHeader HttpHeaders headers,
                                             @Valid @RequestBody ProfessionalInfoDtoIn professionalInfoDtoIn,
                                             @PathVariable int id) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(id);
            ProfessionalInfo professionalInfo = professionalMapper.professionalInfoDtoToObject(professionalInfoDtoIn);
            professionalInfoService.create(profAuth, professional, professionalInfo);

            return professionalMapper.professionalInfoObjectToDto(professionalInfo);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("{id}/applications")
    public ApplicationDtoOut createApplication(@RequestHeader HttpHeaders headers,
                                               @PathVariable int id,
                                               @Valid @RequestBody ApplicationDtoIn applicationDtoIn) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);

            Professional professional = professionalService.getById(id);
            Application application = applicationMapper.applicationDtoToObject(applicationDtoIn);
            application.setProfessional(professional);

            applicationService.create(profAuth, application);
            return applicationMapper.applicationObjectToDto(application);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("{id}")
    public ProfessionalDtoOut update(@RequestHeader HttpHeaders headers,
                                     @Valid @RequestBody ProfessionalDtoIn professionalDtoIn,
                                     @PathVariable int id) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalMapper.professionalDtoToObject(professionalDtoIn);
            professionalService.update(profAuth, professional, id);
            return professionalMapper.professionalObjectToDto(professional);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("{profId}/applications/{applicationId}")
    public ApplicationDtoOut update(@RequestHeader HttpHeaders headers,
                                    @PathVariable int profId,
                                    @PathVariable int applicationId,
                                    @Valid @RequestBody ApplicationDtoIn applicationDtoIn) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(profId);
            Application application = applicationMapper.applicationDtoToObject(applicationDtoIn);
            application.setProfessional(professional);

            return applicationMapper.applicationObjectToDto(applicationService.update(profAuth,
                    application,
                    applicationId));

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("{id}/change-password")
    public void changePassword(@PathVariable int id,
                               @Valid @RequestBody PasswordDto
                                       passwordDto,
                               @RequestHeader HttpHeaders headers) {
        try {
            Professional professionalAuth = authenticationHelper.tryGetProfessional(headers);
            professionalService.changePassword(professionalAuth, passwordDto, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (PasswordMismatchException | PasswordDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("{id}/info")
    public ProfessionalInfoDtoOut updateInfo(@RequestHeader HttpHeaders headers,
                                             @Valid @RequestBody ProfessionalInfoDtoIn professionalInfoDtoIn,
                                             @PathVariable int id) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);

            Professional professional = professionalService.getById(id);
            ProfessionalInfo professionalInfo = professionalMapper.professionalInfoDtoToObject(professionalInfoDtoIn);
            professionalInfo.setProfessional(professional);

            professionalInfoService.update(profAuth, professionalInfo, id);
            return professionalMapper.professionalInfoObjectToDto(professionalInfo);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{profId}/applications/{appId}/main")
    public ApplicationDtoOut setMainApplication(@RequestHeader HttpHeaders headers,
                                                @PathVariable int profId,
                                                @PathVariable int appId) {
        try {
            Professional profAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(profId);
            Application application = applicationService.getById(appId);
            professionalService.setMainApplication(profAuth, application, professional);
            return applicationMapper.applicationObjectToDto(application);
        } catch (DuplicateEntityException | ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{profId}/offers/{matchId}/accept")
    public void acceptJobOffer(@RequestHeader HttpHeaders headers,
                               @PathVariable int profId,
                               @PathVariable int matchId) {
        try {
            Professional professionalAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(profId);

            matchRequestService.acceptJobOffer(professionalAuth, professional, matchId);
        } catch (EntityNotFoundException | ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }


    @DeleteMapping("{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        try {
            Professional professional = authenticationHelper.tryGetProfessional(headers);
            professionalService.delete(professional, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("{id}/info")
    public void deleteInfo(@RequestHeader HttpHeaders headers,
                           @PathVariable int id) {
        try {
            Professional professional = authenticationHelper.tryGetProfessional(headers);
            professionalInfoService.delete(professional, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/applications/{appId}")
    public void deleteApplication(@RequestHeader HttpHeaders headers,
                                  @PathVariable int id,
                                  @PathVariable int appId) {
        try {
            Professional professionalAuth = authenticationHelper.tryGetProfessional(headers);
            Professional professional = professionalService.getById(id);
            applicationService.delete(professionalAuth, professional, appId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (ApplicationMismatchException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}
