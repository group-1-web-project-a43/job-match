package com.example.jobmatch.controllers.rest;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.services.interfaces.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/skills")
public class SkillController {
    private final SkillService skillService;

    @Autowired
    public SkillController(SkillService skillService) {
        this.skillService = skillService;
    }

    @GetMapping
    public List<Skill> get() {
        return skillService.getAll();
    }

    @GetMapping("/{id}")
    public Skill get(@PathVariable int id) {
        try {
            return skillService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Skill create(@RequestBody Skill skill) {
        try {
            return skillService.create(skill);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Skill update(@RequestBody Skill skill, @PathVariable int id) {
        try {
            return skillService.update(skill, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable int id) {
        try {
            skillService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
