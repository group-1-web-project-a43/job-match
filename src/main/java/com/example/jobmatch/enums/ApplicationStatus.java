package com.example.jobmatch.enums;

public enum ApplicationStatus {
    ACTIVE, HIDDEN, PRIVATE, MATCHED
}
