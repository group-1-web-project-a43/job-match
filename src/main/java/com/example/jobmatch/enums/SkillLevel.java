package com.example.jobmatch.enums;

public enum SkillLevel {
    NOVICE, INTERMEDIATE, ADVANCED, EXPERT;


    @Override
    public String toString() {
        switch (this) {
            case NOVICE:
                return "Novice";
            case INTERMEDIATE:
                return "Intermediate";
            case ADVANCED:
                return "Advanced";
            case EXPERT:
                return "Expert";
            default:
                throw new UnsupportedOperationException();
        }
    }

}
