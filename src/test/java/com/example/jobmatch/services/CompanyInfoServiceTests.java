package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.CompanyInfo;
import com.example.jobmatch.repositories.interfaces.CompanyInfoRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockCompany;
import static com.example.jobmatch.services.Helpers.createMockCompanyInfo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
public class CompanyInfoServiceTests {

    @Mock
    CompanyInfoRepository companyInfoRepository;
    @InjectMocks
    CompanyInfoServiceImpl companyInfoService;
    @Mock
    CompanyService companyService;


    @Test
    public void getById_Should_ThrowException_WhenCompanyInfoDoesNotExist() {
        //Arrange
        Mockito.when(companyInfoRepository.findById(anyInt()))
                .thenReturn(Optional.empty());

        //Act/Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> companyInfoService.getById(1));
    }

    @Test
    public void getById_Should_ReturnCompanyInfo_WhenIdExists() {
        //Arrange
        var mockCompanyInfo = createMockCompanyInfo();
        Mockito.when(companyInfoRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockCompanyInfo));
        //Act
        CompanyInfo receivedInfo = companyInfoService.getById(mockCompanyInfo.getId());
        //Assert
        Assertions.assertSame(mockCompanyInfo, receivedInfo);
    }


    @Test
    public void create_Should_ThrowException_IfCreatorIsNotAuthorized() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockAuthenticated = createMockCompany(2);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> companyInfoService.create(mockAuthenticated, mockCompanyInfo));
    }

    @Test
    public void create_Should_ThrowException_IfCompanyInfoDoesNotBelongToCompany() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockAuthenticated = mockCompanyInfo.getCompany();
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockAuthenticated))
                .thenReturn(Optional.of(mockCompanyInfo));
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> companyInfoService.create(mockAuthenticated, mockCompanyInfo));
    }

    @Test
    public void create_Should_SetCompany() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockCompany = mockCompanyInfo.getCompany();
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockCompany))
                .thenReturn(Optional.empty());
        Mockito.when(companyInfoRepository.save(mockCompanyInfo))
                .thenReturn(mockCompanyInfo);
        CompanyInfo result = companyInfoService.create(mockCompany, mockCompanyInfo);
        Assertions.assertEquals(mockCompany, result.getCompany());
    }

    @Test
    public void create_Should_CallRepository() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockCompany = mockCompanyInfo.getCompany();


        companyInfoService.create(mockCompany, mockCompanyInfo);
        Mockito.verify(companyInfoRepository
                        , Mockito.times(1))
                .save(mockCompanyInfo);
    }

    @Test
    public void getInfoByCompanyId_Should_Throw_WhenCompanyInfoIsEmpty() {
        var mockCompany = createMockCompany(1);
        Mockito.when(companyService.getById(mockCompany.getId()))
                .thenReturn(mockCompany);
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockCompany))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> companyInfoService.getInfoByCompanyId(1));
    }

    @Test
    public void getInfoByCompanyId_Should_ReturnCompanyInfo_WhenExists() {
        var mockCompany = createMockCompany(1);
        var mockCompanyInfo = createMockCompanyInfo();
        Mockito.when(companyService.getById(mockCompany.getId()))
                .thenReturn(mockCompany);
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockCompany))
                .thenReturn(Optional.of(mockCompanyInfo));
        //Act
        CompanyInfo receivedInfo = companyInfoService.getInfoByCompanyId(mockCompany.getId());
        //Assert
        Assertions.assertSame(mockCompanyInfo, receivedInfo);
    }

    @Test
    public void update_Should_ThrowException_When_ThereIsNoInfoToUpdate() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockAuthenticated = createMockCompany(1);
        Assertions.assertThrows(EntityNotFoundException.class, () -> companyInfoService
                .update(mockAuthenticated, 1, mockCompanyInfo));
    }

    @Test
    public void update_Should_ThrowException_When_EditingCompanyIsNotAuthorized() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockCompanyAuth = createMockCompany(2);
        var mockCompanyInfoDto = createMockCompanyInfo();

        Mockito.when(companyService.getById(anyInt())).thenReturn(mockCompanyInfo.getCompany());
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockCompanyInfo.getCompany()))
                .thenReturn(Optional.of(mockCompanyInfo));

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> companyInfoService
                .update(mockCompanyAuth, 1, mockCompanyInfoDto));
    }

    @Test
    public void update_Should_MaintainIdAndParentCompany_WhenSuccessful() {
        var mockCompanyInfo = createMockCompanyInfo();
        var mockCompanyInfoDto = createMockCompanyInfo();

        mockCompanyInfoDto.setId(0);
        mockCompanyInfoDto.setCompany(null);

        Mockito.when(companyService.getById(anyInt())).thenReturn(mockCompanyInfo.getCompany());
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockCompanyInfo.getCompany()))
                .thenReturn(Optional.of(mockCompanyInfo));

        companyInfoService.update(mockCompanyInfo.getCompany(), 1, mockCompanyInfoDto);
        Assertions.assertAll(
                () -> assertEquals(1, mockCompanyInfoDto.getId()),
                () -> assertEquals(mockCompanyInfo.getCompany(), mockCompanyInfoDto.getCompany())
        );
    }


    @Test
    public void delete_Should_ThrowException_WhenCompanyInfoIsEmpty() {
        var mockAuthenticated = createMockCompany(1);
        Assertions.assertThrows(EntityNotFoundException.class, () -> companyInfoService
                .delete(mockAuthenticated, 1));
    }

    @Test
    public void delete_Should_CallRepository_WhenSuccessful() {
        var mockCompanyInfo = createMockCompanyInfo();

        Mockito.when(companyService.getById(anyInt())).thenReturn(mockCompanyInfo.getCompany());
        Mockito.when(companyInfoRepository.findCompanyInfoByCompanyEquals(mockCompanyInfo.getCompany()))
                .thenReturn(Optional.of(mockCompanyInfo));

        companyInfoService.delete(mockCompanyInfo.getCompany(), 1);

        Mockito.verify(companyInfoRepository, Mockito.times(1)).delete(mockCompanyInfo);
    }

    @Test
    public void getAll_Should_CallRepository() {
        // Arrange
        Iterable<CompanyInfo> emptyList = new ArrayList<>();
        Mockito.when(companyInfoRepository.findAll())
                .thenReturn(emptyList);
        // Act
        companyInfoService.getAll();
        // Assert
        Mockito.verify(companyInfoRepository, Mockito.times(1))
                .findAll();
    }

}

