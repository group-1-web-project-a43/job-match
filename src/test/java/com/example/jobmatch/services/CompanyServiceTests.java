package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.models.Company;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.repositories.interfaces.CompanyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockCompany;
import static com.example.jobmatch.services.Helpers.createMockJobOpening;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTests {

    @Mock
    CompanyRepository repository;

    @Mock
    AccountRepository accountRepository;

    @InjectMocks
    CompanyServiceImpl service;

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoUsersExist() {
        //Arrange

        Iterable<Company> list = new ArrayList<>();
        Mockito.when(repository.findAll())
                .thenReturn(list);
        //Act
        List<Company> listResult = service.getAll();
        //Assert
        Assertions.assertTrue(listResult.isEmpty());
    }

    @Test
    public void getAll_Should_CallRepository() {
        // Arrange
        Iterable<Company> emptyList = new ArrayList<>();
        Mockito.when(repository.findAll())
                .thenReturn(emptyList);
        // Act
        service.getAll();
        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .findAll();
    }

    @Test
    public void getAll_Should_ReturnCompanyList() {
        // Arrange
        List<Company> repositoryList = List.of(createMockCompany(1), createMockCompany(2));
        Mockito.when(repository.findAll()).thenReturn(repositoryList);

        // Act
        List<Company> mockServiceList = service.getAll();

        // Assert
        assertAll(
                () -> assertEquals(2, mockServiceList.size(), "Prof list size was not equal"),
                () -> assertEquals(mockServiceList.get(0), repositoryList.get(0),
                        "The first professional was not equal"),
                () -> assertEquals(mockServiceList.get(1), repositoryList.get(1),
                        "The second professional was not equal")
        );
    }

    @Test
    public void findAll_Should_CallRepository() {
        List<Company> mockCompanies = List.of(createMockCompany(1), createMockCompany(2));
        Mockito.when(repository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(mockCompanies));

        service.findAll(1);
        Mockito.verify(repository, Mockito.times(1))
                .findAll(any(Pageable.class));
    }

    @Test
    public void findAll_Should_ThrowException_IfPageIndexIsLessThanZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> service.findAll(0));
    }

    @Test
    public void searchProfessionalByName_Should_ThrowException_IfKeywordIsEmpty() {
        Assertions.assertThrows(EmptySearchException.class,
                () -> service.searchCompanyByName(Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty()));
    }

    @Test
    public void searchCompanyByName_Should_NotManipulateCollection() {
        List<Company> mockCompanies = List.of(createMockCompany(1), createMockCompany(2));
        Mockito.when(repository.searchCompanyByName(anyString(), any(Pageable.class)))
                .thenReturn(mockCompanies);

        List<Company> mockServiceList = service.searchCompanyByName(
                Optional.of("telerik"),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        assertAll(
                () -> assertSame(mockServiceList, mockCompanies,
                        "Professionals list should be the same"),
                () -> assertEquals(2, mockServiceList.size(),
                        "Beer list size was not equal"),
                () -> assertEquals(mockServiceList.get(0), mockCompanies.get(0),
                        "The first beer was not equal"),
                () -> assertEquals(mockServiceList.get(1), mockCompanies.get(1),
                        "The second beer was not equal")
        );
    }

    @Test
    public void searchProfessionalByName_Should_CallRepository_IfKeywordIsPresent() {
        List<Company> mockCompanies = List.of(createMockCompany(1), createMockCompany(2));
        Mockito.when(repository.searchCompanyByName(anyString(), any(Pageable.class))).thenReturn(mockCompanies);

        service.searchCompanyByName(
                Optional.of("telerik"),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        Mockito.verify(repository, Mockito.times(1))
                .searchCompanyByName(anyString(), any(Pageable.class));
    }

    @Test
    public void getById_Should_ThrowException_WhenCompanyNotFound() {
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(2));
    }

    @Test
    public void getById_Should_ReturnCompanyWhenSuccessful() {
        var mockCompany = createMockCompany(1);
        Mockito.when(repository.findById(mockCompany.getId())).thenReturn(Optional.of(mockCompany));

        Company result = service.getById(1);

        Mockito.verify(repository, Mockito.times(1)).findById(1);
        Assertions.assertSame(mockCompany, result);
    }

    @Test
    public void getByName_Should_ThrowException_WhenCompanyNotFound() {
        Mockito.when(repository.getCompanyByNameEquals(anyString())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getByName("telerik"));
    }

    @Test
    public void getByName_Should_ReturnCompanyWhenSuccessful() {
        var mockCompany = createMockCompany(1);
        Mockito.when(repository.getCompanyByNameEquals(mockCompany.getName())).thenReturn(Optional.of(mockCompany));

        Company result = service.getByName(mockCompany.getName());

        Mockito.verify(repository, Mockito.times(1)).getCompanyByNameEquals(mockCompany.getName());
        Assertions.assertSame(mockCompany, result);
    }

    @Test
    public void getByUsername_Should_CallRepository() {
        service.getByUsername(anyString());
        Mockito.verify(repository, Mockito.times(1))
                .getCompanyByUsernameEqualsIgnoreCase(anyString());
    }

    @Test
    public void create_Should_ThrowException_IfAccountWithSameEmailExists() {
        var mockCompanyFromDto = createMockCompany(0);
        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockCompanyFromDto.getUsername(),
                        mockCompanyFromDto.getEmail()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockCompanyFromDto));
    }

    @Test
    public void create_Should_ThrowException_If_CompanyWithSameNameExists() {
        var mockCompanyFromDto = createMockCompany(0);
        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockCompanyFromDto.getUsername(),
                        mockCompanyFromDto.getEmail()))
                .thenReturn(false);
        Mockito.when(repository.existsCompanyByNameEquals(
                        mockCompanyFromDto.getName()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockCompanyFromDto));
    }

    @Test
    public void create_Should_CallRepository_WhenSuccessful() {
        var mockCompanyFromDto = createMockCompany(0);

        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockCompanyFromDto.getUsername(),
                        mockCompanyFromDto.getEmail()))
                .thenReturn(false);
        Mockito.when(repository.existsCompanyByNameEquals(
                        mockCompanyFromDto.getName()))
                .thenReturn(false);

        service.create(mockCompanyFromDto);

        Mockito.verify(repository, Mockito.times(1))
                .save(mockCompanyFromDto);
    }

    @Test
    public void update_Should_ThrowException_When_AuthenticatedCompanyIsNotAuthorizedToEdit() {
        // Arrange
        var mockUpdateObject = createMockCompany(0);
        var mockCompanyToUpdate = createMockCompany(1);
        var mockCompany2 = createMockCompany(2);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockCompanyToUpdate));

        // Act/Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCompany2, mockUpdateObject, 1));
    }

    @Test
    public void update_Should_ThrowException_When_AccountWithIdenticalEmailAlreadyExists() {
        var mockUpdateObject = createMockCompany(0);
        var mockCompanyToUpdate = createMockCompany(1);
        var mockCompanyAuth = createMockCompany(1);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockCompanyToUpdate));
        Mockito.when(accountRepository.getAccountByEmailEquals(mockUpdateObject.getEmail()))
                .thenReturn(Optional.of(mockCompanyAuth));

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCompanyAuth, mockUpdateObject, 2));
    }

    @Test
    public void update_Should_ThrowException_When_CompanyWithIdenticalNameAlreadyExists() {
        var mockUpdateObject = createMockCompany(0);
        var mockCompanyToUpdate = createMockCompany(1);
        var mockCompanyAuth = createMockCompany(1);
        var mockCompanyOther = createMockCompany(3);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockCompanyToUpdate));
        Mockito.when(accountRepository.getAccountByEmailEquals(mockUpdateObject.getEmail()))
                .thenReturn(Optional.empty());
        Mockito.when(repository.getCompanyByNameEquals(mockUpdateObject.getName()))
                .thenReturn(Optional.of(mockCompanyOther));

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCompanyAuth, mockUpdateObject, 2));
    }

    @Test
    public void update_Should_Maintain_ProfessionalsCompositionRelationships() {
        //Arrange
        var mockUpdateObject = createMockCompany(0);
        var mockJobOpening = createMockJobOpening();
        var mockCompanyAuth = mockJobOpening.getCompany();


        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockCompanyAuth));
        Mockito.when(accountRepository.getAccountByEmailEquals(mockUpdateObject.getEmail()))
                .thenReturn(Optional.empty());
        Mockito.when(repository.getCompanyByNameEquals(mockUpdateObject.getName()))
                .thenReturn(Optional.empty());
        Mockito.when(repository.save(mockUpdateObject)).thenReturn(mockUpdateObject);
        //Act
        Company result = service.update(mockCompanyAuth, mockUpdateObject, 1);

        //Assert
        Assertions.assertAll(
                () -> assertEquals(mockCompanyAuth.getCompanyInfo().get(), result.getCompanyInfo().get(),
                        "company info should be the same"),
                () -> assertEquals(result.getId(), mockCompanyAuth.getId(),
                        "id should be the same"),
                () -> assertSame(result.getJobOpenings(), mockCompanyAuth.getJobOpenings(),
                        "job openings list should be the same")
        );
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        //Arrange
        var mockUpdateObject = createMockCompany(0);
        var mockCompany = createMockCompany(1);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockCompany));
        Mockito.when(accountRepository.getAccountByEmailEquals(mockUpdateObject.getEmail()))
                .thenReturn(Optional.empty());
        Mockito.when(repository.getCompanyByNameEquals(mockUpdateObject.getName()))
                .thenReturn(Optional.empty());

        service.update(mockCompany, mockUpdateObject, 1);

        Mockito.verify(repository, Mockito.times(1))
                .save(mockUpdateObject);
    }

    @Test
    public void changePassword_Should_ThrowException_IfProfessionalIsNotAuthorized() {
        var mockAuthenticated = createMockCompany(1);
        var mockCompany = createMockCompany(2);

        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.of(mockCompany));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.changePassword(mockAuthenticated, new PasswordDto(), 1));
    }

    @Test
    public void changePassword_Should_ThrowException_IfPasswordMismatchesOldPassword() {
        var mockAuthenticated = createMockCompany(1);
        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword("banana");

        Assertions.assertThrows(PasswordMismatchException.class,
                () -> service.changePassword(mockAuthenticated, passwordChange, 1));
    }

    @Test
    public void changePassword_Should_ThrowException_ifNewPasswordIsDuplicateOfOld() {
        var mockAuthenticated = createMockCompany(1);
        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword(mockAuthenticated.getPassword());
        passwordChange.setNewPassword(passwordChange.getOldPassword());
        passwordChange.setNewPasswordConfirmation(passwordChange.getOldPassword());

        Assertions.assertThrows(PasswordDuplicateException.class,
                () -> service.changePassword(mockAuthenticated, passwordChange, 1));
    }

    @Test
    public void changePassword_Should_ThrowException_IfNewPasswordAndConfirmationMismatch() {
        var mockAuthenticated = createMockCompany(1);
        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword(mockAuthenticated.getPassword());
        passwordChange.setNewPassword("banana");
        passwordChange.setNewPasswordConfirmation("orangutan");

        Assertions.assertThrows(PasswordMismatchException.class,
                () -> service.changePassword(mockAuthenticated, passwordChange, 1));
    }

    @Test
    public void changePassword_Should_CallRepository_WhenSuccessful() {
        var mockAuthenticated = createMockCompany(1);
        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword(mockAuthenticated.getPassword());
        passwordChange.setNewPassword("orangutan");
        passwordChange.setNewPasswordConfirmation("orangutan");

        service.changePassword(mockAuthenticated, passwordChange, 1);


        Assertions.assertEquals(mockAuthenticated.getPassword(), passwordChange.getNewPassword());
        Mockito.verify(repository, Mockito.times(1))
                .save(mockAuthenticated);
    }


    @Test
    public void delete_Should_ThrowException_IfTryingToDeleteAnotherCompany() {
        var mockAuthenticated = createMockCompany(1);
        var mockCompany = createMockCompany(2);
        mockCompany.setUsername("bananana");
        mockCompany.setEmail("kapanafest@telerik.bg");

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockCompany));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockAuthenticated, 1));

    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockAuthenticated = createMockCompany(1);


        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        service.delete(mockAuthenticated, 1);

        Mockito.verify(repository, Mockito.times(1))
                .delete(mockAuthenticated);
    }

}
