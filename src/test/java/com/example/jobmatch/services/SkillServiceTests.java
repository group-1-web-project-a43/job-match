package com.example.jobmatch.services;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.repositories.interfaces.JobFormRepository;
import com.example.jobmatch.repositories.interfaces.SkillRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockApplication;
import static com.example.jobmatch.services.Helpers.createMockSkill;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class SkillServiceTests {

    @Mock
    SkillRepository skillRepository;
    @Mock
    JobFormRepository jobFormRepository;
    @InjectMocks
    SkillServiceImpl skillService;

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoSkillsExist() {
        Iterable<Skill> list = new ArrayList<>();
        Mockito.when(skillRepository.findAll())
                .thenReturn(list);

        List<Skill> listResult = skillService.getAll();

        Assertions.assertTrue(listResult.isEmpty());
    }

    @Test
    public void getAll_Should_CallRepository() {
        Iterable<Skill> emptyList = new ArrayList<>();
        Mockito.when(skillRepository.findAll())
                .thenReturn(emptyList);

        skillService.getAll();

        Mockito.verify(skillRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    public void getAll_Should_ReturnSkillsList() {
        List<Skill> repositoryList = List.of(createMockSkill(), createMockSkill());
        Mockito.when(skillRepository.findAll()).thenReturn(repositoryList);

        List<Skill> mockServiceList = skillService.getAll();

        assertAll(
                () -> assertEquals(2, mockServiceList.size(), "Skills list size was not equal"),
                () -> assertEquals(mockServiceList.get(0), repositoryList.get(0),
                        "The first skill was not equal"),
                () -> assertEquals(mockServiceList.get(1), repositoryList.get(1),
                        "The second skill was not equal")
        );
    }

    @Test
    public void getById_Should_ThrowException_WhenSkillNotFound() {
        Mockito.when(skillRepository.findById(anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> skillService.getById(1));
    }

    @Test
    public void getById_Should_ReturnSkillWhenSuccessful() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.findById(mockSkill.getId()))
                .thenReturn(Optional.of(mockSkill));

        Skill result = skillService.getById(1);

        Mockito.verify(skillRepository, Mockito.times(1)).findById(1);
        Assertions.assertSame(mockSkill, result);
    }

    @Test
    public void create_Should_ThrowException_IfSkillWithSameNameExists() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.existsSkillBySkillName(mockSkill.getSkillName()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> skillService.create(mockSkill));
    }

    @Test
    public void create_Should_CallRepository_WhenSuccessful() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.existsSkillBySkillName(mockSkill.getSkillName()))
                .thenReturn(false);

        skillService.create(mockSkill);

        Mockito.verify(skillRepository, Mockito.times(1))
                .save(mockSkill);
    }

    @Test
    public void update_Should_ThrowException_IfSkillWithSameNameExists() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.existsSkillBySkillName(mockSkill.getSkillName()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> skillService.update(mockSkill, mockSkill.getId()));
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.existsSkillBySkillName(mockSkill.getSkillName()))
                .thenReturn(false);
        Mockito.when(skillRepository.findById(2))
                .thenReturn(Optional.of(mockSkill));

        skillService.update(mockSkill, 2);

        Mockito.verify(skillRepository, Mockito.times(1))
                .save(mockSkill);
    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.findById(mockSkill.getId()))
                .thenReturn(Optional.of(mockSkill));

        skillService.delete(mockSkill.getId());

        Mockito.verify(skillRepository, Mockito.times(1))
                .delete(mockSkill);
    }

    @Test
    public void getByName_Should_ThrowException_WhenSkillNotFound() {
        Mockito.when(skillRepository.findSkillBySkillName(anyString()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> skillService.getByName("test"));
    }

    @Test
    public void getByName_Should_ReturnSkill_WhenSuccessful() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.findSkillBySkillName(mockSkill.getSkillName()))
                .thenReturn(Optional.of(mockSkill));

        Skill result = skillService.getByName(mockSkill.getSkillName());

        Assertions.assertSame(mockSkill, result);
    }

    @Test
    public void getByName_Should_CallRepository() {
        var mockSkill = createMockSkill();
        Mockito.when(skillRepository.findSkillBySkillName(mockSkill.getSkillName()))
                .thenReturn(Optional.of(mockSkill));

        Skill result = skillService.getByName(mockSkill.getSkillName());

        Mockito.verify(skillRepository, Mockito.times(1))
                .findSkillBySkillName(result.getSkillName());
    }

    @Test
    public void addOrModifySkill_Should_ThrowException_When_AddingExistingSkillAndSkillLevelEntry() {
        var mockApplication = createMockApplication();
        var mockSkill = createMockSkill();

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> skillService.addOrModifySkill(
                        mockApplication,
                        mockSkill,
                        SkillLevel.EXPERT));
    }

    @Test
    public void addOrModifySkill_Should_UpdateSkillSetAndCallRepository_WhenSuccessful() {
        var mockApplication = createMockApplication();
        var mockSkill = createMockSkill();

        mockSkill.setSkillName("attention to detail");
        mockSkill.setId(2);

        skillService.addOrModifySkill(mockApplication, mockSkill, SkillLevel.ADVANCED);

        Mockito.verify(jobFormRepository, Mockito.times(1)).save(mockApplication);
        Assertions.assertAll(
                () -> assertTrue(mockApplication.getSkillSet().containsKey(mockSkill)),
                () -> assertEquals(mockApplication.getSkillSet().get(mockSkill), SkillLevel.ADVANCED)
        );
    }

    @Test
    public void removeSkill_Should_ThrowException_When_AttemptingToRemoveSkillThatIsNotPresent() {
        var mockApplication = createMockApplication();
        var mockSkill = createMockSkill();

        mockSkill.setSkillName("attention to detail");
        mockSkill.setId(2);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> skillService.removeSkill(
                        mockApplication,
                        mockSkill));
    }

    @Test
    public void removeSkill_Should_UpdateSkillSetAndCallRepository_WhenSuccessful() {
        var mockApplication = createMockApplication();
        var mockSkill = createMockSkill();

        skillService.removeSkill(mockApplication, mockSkill);

        Mockito.verify(jobFormRepository, Mockito.times(1)).save(mockApplication);

        Assertions.assertAll(
                () -> assertFalse(mockApplication.getSkillSet().containsKey(mockSkill)),
                () -> assertEquals(0, mockApplication.getSkillSet().size())
        );
    }
}
