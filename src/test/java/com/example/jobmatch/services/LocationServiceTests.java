package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.LocationNotExistException;
import com.example.jobmatch.models.Location;
import com.example.jobmatch.repositories.interfaces.LocationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockLocation;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class LocationServiceTests {

    @Mock
    LocationRepository locationRepository;
    @InjectMocks
    LocationServiceImpl locationService;

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoLocationExist() {
        Iterable<Location> list = new ArrayList<>();
        Mockito.when(locationRepository.findAll())
                .thenReturn(list);

        List<Location> listResult = locationService.getAll();

        Assertions.assertTrue(listResult.isEmpty());
    }

    @Test
    public void getAll_Should_CallRepository() {
        Iterable<Location> list = new ArrayList<>();
        Mockito.when(locationRepository.findAll())
                .thenReturn(list);

        locationService.getAll();

        Mockito.verify(locationRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    public void getAll_Should_ReturnLocationsList() {
        List<Location> repositoryList = List.of(createMockLocation(), createMockLocation());
        Mockito.when(locationRepository.findAll()).thenReturn(repositoryList);

        List<Location> mockServiceList = locationService.getAll();

        assertAll(
                () -> assertEquals(2, mockServiceList.size(), "Locations list size was not equal"),
                () -> assertEquals(mockServiceList.get(0), repositoryList.get(0),
                        "The first location was not equal"),
                () -> assertEquals(mockServiceList.get(1), repositoryList.get(1),
                        "The second location was not equal")
        );
    }

    @Test
    public void getById_Should_ThrowException_WhenLocationNotFound() {
        Mockito.when(locationRepository.findById(anyInt()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> locationService.getById(1));
    }

    @Test
    public void getById_Should_ReturnLocationWhenSuccessful() {
        var mockLocation = createMockLocation();
        Mockito.when(locationRepository.findById(mockLocation.getId()))
                .thenReturn(Optional.of(mockLocation));

        Location result = locationService.getById(1);

        Mockito.verify(locationRepository, Mockito.times(1)).findById(1);
        Assertions.assertSame(mockLocation, result);
    }

    @Test
    public void create_Should_ThrowException_IfLocationWithSameCityAndCountryExists() {
        var mockLocation = createMockLocation();
        Mockito.when(locationRepository.existsLocationByCityAndCountry(
                        mockLocation.getCity(), mockLocation.getCountry()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class, () -> locationService.create(mockLocation));
    }

    @Test
    public void create_Should_CallRepository_WhenSuccessful() {
        var mockLocation = createMockLocation();
        Mockito.when(locationRepository.existsLocationByCityAndCountry(
                        mockLocation.getCity(), mockLocation.getCountry()))
                .thenReturn(false);

        locationService.create(mockLocation);

        Mockito.verify(locationRepository, Mockito.times(1))
                .save(mockLocation);
    }

    @Test
    public void update_Should_ThrowException_IfLocationWithSameCityAndCountryExists() {
        var mockLocation = createMockLocation();
        Mockito.when(locationRepository.findById(2))
                .thenReturn(Optional.of(mockLocation));
        Mockito.when(locationRepository.existsLocationByCityAndCountry(
                        mockLocation.getCity(), mockLocation.getCountry()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> locationService.update(mockLocation, 2));
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        var mockLocation = createMockLocation();
        Mockito.when(locationRepository.findById(2))
                .thenReturn(Optional.of(mockLocation));
        Mockito.when(locationRepository.existsLocationByCityAndCountry(
                        mockLocation.getCity(), mockLocation.getCountry()))
                .thenReturn(false);

        locationService.update(mockLocation, 2);

        Mockito.verify(locationRepository, Mockito.times(1))
                .save(mockLocation);
    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockLocation = createMockLocation();

        locationService.delete(mockLocation.getId());

        Mockito.verify(locationRepository, Mockito.times(1))
                .deleteById(mockLocation.getId());
    }

    @Test
    public void getByCity_ShouldThrow_WhenCityNotFound() {
        Iterable<Location> list = new ArrayList<>();
        Mockito.when(locationRepository.findLocationsByCityEquals(anyString()))
                .thenReturn(list);

        Assertions.assertThrows(LocationNotExistException.class, () -> locationService.getByCity("test"));
    }

    @Test
    public void getByCity_Should_ReturnLocation_WhenExist() {
        var mockLocation = createMockLocation();
        List<Location> repositoryList = List.of(mockLocation);
        Mockito.when(locationRepository.findLocationsByCityEquals(anyString()))
                .thenReturn(repositoryList);

        Location resultLocation = locationService.getByCity(repositoryList.get(0).toString());

        Assertions.assertSame(mockLocation, resultLocation);
    }

    @Test
    public void getByCountryAndCity_Should_ThrowException_WhenLocationNotFound() {
        Mockito.when(locationRepository.findLocationByCountryAndCity(anyString(), anyString()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(LocationNotExistException.class,
                () -> locationService.getByCountryAndCity("country", "city"));
    }

    @Test
    public void getByCountryAndCity_Should_ReturnLocationWhenSuccessful() {
        var mockLocation = createMockLocation();
        Mockito.when(locationRepository.findLocationByCountryAndCity(anyString(), anyString()))
                .thenReturn(Optional.of(mockLocation));

        Location result = locationService.getByCountryAndCity(mockLocation.getCountry(), mockLocation.getCity());

        Mockito.verify(locationRepository, Mockito.times(1))
                .findLocationByCountryAndCity(mockLocation.getCountry(), mockLocation.getCity());
        Assertions.assertSame(mockLocation, result);
    }
}
