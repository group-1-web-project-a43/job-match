package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.ProfessionalInfo;
import com.example.jobmatch.repositories.interfaces.ProfessionalInfoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockProfessional;
import static com.example.jobmatch.services.Helpers.createMockProfessionalInfo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
public class ProfessionalInfoServiceTests {

    @Mock
    ProfessionalInfoRepository repository;
    @InjectMocks
    ProfessionalInfoServiceImpl service;

    @Test
    public void getById_Should_ThrowException_WhenIdDoesNotExist() {
        //Arrange
        Mockito.when(repository.findById(anyInt()))
                .thenReturn(Optional.empty());

        //Act/Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(2));
    }

    @Test
    public void getById_Should_ReturnProfessionalInfo_WhenIdExists() {
        //Arrange
        var mockProfessionalInfo = createMockProfessionalInfo();

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockProfessionalInfo));
        //Act
        ProfessionalInfo receivedInfo = service.getById(mockProfessionalInfo.getId());
        //Assert
        Assertions.assertSame(mockProfessionalInfo, receivedInfo);
    }

    @Test
    public void create_Should_ThrowException_IfCreatorIsNotAuthorized() {
        var mockProfessionalInfo = createMockProfessionalInfo();
        var mockProfessional = mockProfessionalInfo.getProfessional();
        var mockAuthenticated = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockAuthenticated, mockProfessional, mockProfessionalInfo));
    }

    @Test
    public void create_Should_SetProfessional() {
        var mockProfessionalInfo = createMockProfessionalInfo();

        var mockProfessional = createMockProfessional(3);
        var mockAuthenticated = mockProfessional;

        Mockito.when(repository.save(mockProfessionalInfo)).thenReturn(mockProfessionalInfo);
        ProfessionalInfo result = service.create(mockAuthenticated, mockProfessional, mockProfessionalInfo);

        Assertions.assertEquals(mockProfessional, result.getProfessional());
    }

    @Test
    public void create_Should_CallRepository() {
        var mockProfessionalInfo = createMockProfessionalInfo();

        var mockProfessional = createMockProfessional(3);
        var mockAuthenticated = mockProfessional;


        service.create(mockAuthenticated, mockProfessional, mockProfessionalInfo);

        Mockito.verify(repository, Mockito.times(1))
                .save(mockProfessionalInfo);
    }

    @Test
    public void update_Should_ThrowException_When_EditorIsNotAuthorized() {
        var mockProfessionalInfo = createMockProfessionalInfo();
        var mockAuthenticated = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockAuthenticated, mockProfessionalInfo, 1));

    }

    @Test
    public void update_Should_ThrowException_When_ThereIsNoInfoToUpdate() {
        var mockProfessionalInfo = createMockProfessionalInfo();
        var mockAuthenticated = createMockProfessional(1);


        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(mockAuthenticated, mockProfessionalInfo, 1));
    }

    @Test
    public void update_Should_SetProfessionalAndInfoRelationship() {
        var mockProfessionalInfo = createMockProfessionalInfo();
        var mockProfessionalInfo2 = createMockProfessionalInfo();

        mockProfessionalInfo2.setDescription("asdasdasda");
        mockProfessionalInfo2.setId(2);

        var mockAuthenticated = createMockProfessional(1);
        mockAuthenticated.setProfessionalInfo(mockProfessionalInfo2);

        Mockito.when(repository.save(mockProfessionalInfo)).thenReturn(mockProfessionalInfo);

        ProfessionalInfo result = service.update(mockAuthenticated, mockProfessionalInfo, 2);

        Assertions.assertAll(
                () -> assertEquals(result.getProfessional(), mockAuthenticated),
                () -> assertEquals(result, mockProfessionalInfo)
        );
    }

    @Test
    public void delete_Should_ThrowException_IfDeleterIsNotAuthorized() {
        var mockInfo = createMockProfessionalInfo();
        var mockProf = createMockProfessional(2);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockInfo));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockProf, 1));
    }

    @Test
    public void delete_Should_CallRepository_WhenSuccessful() {
        var mockInfo = createMockProfessionalInfo();
        var mockProf = createMockProfessional(1);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockInfo));

        service.delete(mockProf, 1);

        Mockito.verify(repository, Mockito.times(1))
                .delete(mockInfo);
    }


}
