package com.example.jobmatch.services;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.exceptions.ApplicationMismatchException;
import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.MatchRequest;
import com.example.jobmatch.repositories.interfaces.MatchRequestRepository;
import com.example.jobmatch.repositories.interfaces.ProfessionalInfoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.example.jobmatch.services.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
public class MatchRequestServiceTests {

    @Mock
    MatchRequestRepository repository;
    @Mock
    ProfessionalInfoRepository infoRepository;

    @InjectMocks
    MatchRequestServiceImpl service;

    @Test
    public void getByJobOpening_Should_ThrowException_When_EmployeeCompanyAndJobOpeningCompanyNotMatch() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(createMockCompany(2));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByJobOpening(mockEmployee, mockJobOpening));
    }

    @Test
    public void getByJobOpening_Should_ReturnCollectionWithoutAlterations() {
        List<MatchRequest> mockList = List.of(createMockMatchRequest(), createMockMatchRequest());

        var mockJobOpening = mockList.get(0).getJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.findMatchRequestsByJobOpeningEquals(mockJobOpening)).thenReturn(mockList);

        List<MatchRequest> result = service.getByJobOpening(mockEmployee, mockJobOpening);
        Assertions.assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertSame(result.get(0), mockList.get(0)),
                () -> assertSame(result.get(1), mockList.get(1)),
                () -> assertEquals(result, mockList)
        );
    }

    @Test
    public void getByJobOpening_Should_CallRepository() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        service.getByJobOpening(mockEmployee, mockJobOpening);

        Mockito.verify(repository, Mockito.times(1))
                .findMatchRequestsByJobOpeningEquals(mockJobOpening);
    }

    @Test
    public void getByProfessionalApplication_Should_CallRepository() {
        var mockProfessional = createMockProfessional(1);
        service.getByProfessionalApplication(mockProfessional);
        Mockito.verify(repository, Mockito.times(1))
                .findMatchRequestsByApplication_Professional(mockProfessional);
    }

    @Test
    public void getByProfessionalApplication_ShouldNot_ManipulateCollection() {
        var mockMatchRequest = createMockMatchRequest();
        var mockMatchRequest1 = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        var mockList = List.of(mockMatchRequest, mockMatchRequest1);

        Mockito.when(repository.findMatchRequestsByApplication_Professional(mockProfessional)).thenReturn(mockList);

        List<MatchRequest> result = service.getByProfessionalApplication(mockProfessional);

        Assertions.assertAll(
                () -> assertEquals(result.size(), mockList.size()),
                () -> assertSame(result.get(0), mockList.get(0)),
                () -> assertSame(result.get(1), mockList.get(1))
        );
    }
    @Test
    public void existMatchRequestsForJobOpening_Should_CallRepository() {
        var mockEmployee = createMockEmployee(1);
        mockEmployee.getCompany().getJobOpenings().add(createMockJobOpening());

        service.existMatchRequestsForJobOpening(mockEmployee);
        Mockito.verify(repository, Mockito.times(1))
                .existsMatchRequestByJobOpeningEquals(any(JobOpening.class));
    }

    @Test
    public void getByProfessional_Should_ThrowException_When_ProfessionalIsUnauthorized() {
        var mockAuth = createMockProfessional(1);
        var mockProf = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByProfessional(mockAuth, mockProf));
    }

    @Test
    public void getByProfessional_Should_ReturnEmptyMap_When_ProfessionalHasNoJobOffers() {
        var mockAuth = createMockProfessional(1);
        Map<MatchRequest, Boolean> result = service.getByProfessional(mockAuth, mockAuth);

        Assertions.assertAll(
                () -> assertEquals(0, result.size()),
                () -> assertSame(result, mockAuth.getJobOffers()),
                () -> assertNotNull(result)
        );
    }

    @Test
    public void getById_Should_ThrowException_When_MatchRequestNotExist() {
        var mockEmployee = createMockEmployee(1);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(mockEmployee, 1));
    }

    @Test
    public void getById_Should_ThrowException_When_EmployeeCompanyAndJobOpeningCompanyNotMatch() {
        var mockMatchRequest = createMockMatchRequest();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(createMockCompany(2));

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(mockEmployee, 1));
    }

    @Test
    public void getById_Should_CallRepository_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockMatchRequest.getJobOpening().getCompany());

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.getById(mockEmployee, 1);

        Mockito.verify(repository, Mockito.times(1)).findById(1);
    }

    @Test
    public void create_Should_ThrowException_When_ProfessionalIsUnauthorized() {
        var mockApplication = createMockApplication();
        var mockJobOpening = createMockJobOpening();
        var mockProfessional = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockProfessional, mockJobOpening, mockApplication));
    }

    @Test
    public void create_Should_ThrowException_When_JobOpeningIsArchived() {
        var mockApplication = createMockApplication();
        var mockJobOpening = createMockJobOpening();
        var mockProfessional = mockApplication.getProfessional();
        mockJobOpening.setActive(false);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.create(mockProfessional, mockJobOpening, mockApplication));
    }

    @Test
    public void create_Should_ThrowException_When_ProfessionalIsBusy() {
        var mockApplication = createMockApplication();
        var mockJobOpening = createMockJobOpening();
        var mockProfessional = mockApplication.getProfessional();

        mockProfessional.getProfessionalInfo().get().setActive(false);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.create(mockProfessional, mockJobOpening, mockApplication));
    }

    @Test
    public void create_Should_ThrowException_When_ProfessionalHasAlreadyAppliedToJobOpening() {
        var mockApplication = createMockApplication();
        var mockJobOpening = createMockJobOpening();
        var mockProfessional = mockApplication.getProfessional();

        var mockIdenticalMatchRequest = createMockMatchRequest();
        mockIdenticalMatchRequest.setApplication(mockApplication);

        Mockito.when(repository.findMatchRequestsByJobOpeningEquals(mockJobOpening))
                .thenReturn(List.of(mockIdenticalMatchRequest));

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockProfessional, mockJobOpening, mockApplication));

    }

    @Test
    public void create_Should_ThrowException_When_ApplicationIsHiddenOrMatchedAlready() {

        var mockApplication = createMockApplication();
        var mockJobOpening = createMockJobOpening();
        var mockProfessional = mockApplication.getProfessional();

        mockApplication.setApplicationStatus(ApplicationStatus.HIDDEN);

        Mockito.when(repository.findMatchRequestsByJobOpeningEquals(mockJobOpening))
                .thenReturn(new ArrayList<>());

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> service.create(mockProfessional, mockJobOpening, mockApplication));
    }

    @Test
    public void create_Should_CallRepository_WhenPermissionsArePassed() {

        var mockApplication = createMockApplication();
        var mockJobOpening = createMockJobOpening();
        var mockProfessional = mockApplication.getProfessional();

        Mockito.when(repository.findMatchRequestsByJobOpeningEquals(mockJobOpening))
                .thenReturn(new ArrayList<>());


        service.create(mockProfessional, mockJobOpening, mockApplication);

        Mockito.verify(repository, Mockito.times(1))
                .save(any(MatchRequest.class));
    }

    @Test
    public void approve_Should_ThrowException_When_EmployeesCompanyDifferentFromParameterCompany() {
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(2);
        var mockJobOpening = createMockJobOpening();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.approve(mockEmployee, mockCompany, mockJobOpening, 1));
    }

    @Test
    public void approve_Should_ThrowException_When_JobOpeningCompanyDifferentFromParameterCompany() {
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(2);
        var mockJobOpening = createMockJobOpening();
        mockJobOpening.setCompany(mockCompany);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1));
    }

    @Test
    public void approve_Should_ThrowException_When_JobOpeningIsAlreadyArchived() {
        var mockEmployee = createMockEmployee(1);
        var mockJobOpening = createMockJobOpening();
        mockEmployee.setCompany(mockJobOpening.getCompany());
        mockJobOpening.setActive(false);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1));
    }

    @Test
    public void approve_Should_ThrowException_When_MatchRequestCannotBeFound() {
        var mockMatchRequest = createMockMatchRequest();
        var mockJobOpening = mockMatchRequest.getJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1));
    }

    @Test
    public void approve_Should_ThrowException_When_MatchRequestIsAlreadyMatched() {
        var mockEmployee = createMockEmployee(1);
        var mockJobOpening = createMockJobOpening();
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var mockMatchRequest = createMockMatchRequest();
        mockMatchRequest.setJobOpening(mockJobOpening);
        mockMatchRequest.setMatch(true);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1));
    }

    @Test
    public void approve_Should_ThrowException_When_MatchRequestApplicationIsMatched() {
        var mockMatchRequest = createMockMatchRequest();
        var mockJobOpening = mockMatchRequest.getJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        mockMatchRequest.getApplication().setApplicationStatus(ApplicationStatus.MATCHED);

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1));
    }

    @Test
    public void approve_Should_ThrowException_When_MatchRequestApplicationIsHidden() {
        var mockMatchRequest = createMockMatchRequest();
        var mockJobOpening = mockMatchRequest.getJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        mockMatchRequest.getApplication().setApplicationStatus(ApplicationStatus.HIDDEN);

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1));
    }

    @Test
    public void approve_Should_SetMatchStatusToTrue_When_PermissionsPassed() {
        var mockMatchRequest = createMockMatchRequest();
        var mockJobOpening = mockMatchRequest.getJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));
        Mockito.when(repository.save(mockMatchRequest)).thenReturn(mockMatchRequest);

        MatchRequest result = service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1);

        Assertions.assertAll(
                () -> assertTrue(result.isMatch()),
                () -> assertTrue(mockMatchRequest.isMatch())
        );

    }

    @Test
    public void approve_Should_createJobOfferMappingInApplyingProfessional() {
        var mockMatchRequest = createMockMatchRequest();
        var mockJobOpening = mockMatchRequest.getJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));
        Mockito.when(repository.save(mockMatchRequest)).thenReturn(mockMatchRequest);

        MatchRequest result = service.approve(mockEmployee, mockEmployee.getCompany(), mockJobOpening, 1);

        Assertions.assertAll(
                () -> assertTrue(result.getApplication().getProfessional().getJobOffers().containsKey(result)),
                () -> assertFalse(result.getApplication().getProfessional().getJobOffers().get(result))
        );
    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_ProfessionalIsUnauthorized() {
        var mockProfessional1 = createMockProfessional(1);
        var mockProfessional2 = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.acceptJobOffer(mockProfessional1, mockProfessional2, 1));
    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_ProfessionalIsBusy() {
        var mockProfessionalInfo = createMockProfessionalInfo();
        var mockProfessional = mockProfessionalInfo.getProfessional();
        mockProfessionalInfo.setActive(false);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.acceptJobOffer(mockProfessional, mockProfessional, 1));
    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_ProfessionalJobOffersDoesNotContainMatchRequest() {

        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.acceptJobOffer(mockProfessional, mockProfessional, 1));
    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_ProfessionalIsNotAuthorOfApplication() {
        var mockMatchRequest = createMockMatchRequest();
        var mockInfo = createMockProfessionalInfo();
        var mockProfessional = mockInfo.getProfessional();
        mockProfessional.setId(2);

        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> service.acceptJobOffer(mockProfessional, mockProfessional, 1));

    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_JobOpeningIsAlreadyArchived() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        mockMatchRequest.getJobOpening().setActive(false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.acceptJobOffer(mockProfessional, mockProfessional, 1));
    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_ProfessionalApplicationIsAlreadyMatched() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        mockMatchRequest.getApplication().setApplicationStatus(ApplicationStatus.MATCHED);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.acceptJobOffer(mockProfessional, mockProfessional, 1));
    }

    @Test
    public void acceptJobOffer_Should_ThrowException_When_ProfessionalHasAlreadyAcceptedTheJobOffer() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, true);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.acceptJobOffer(mockProfessional, mockProfessional, 1));
    }

    @Test
    public void acceptJobOffer_Should_SwitchJobOfferAcceptedBooleanInMapToTrue_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.acceptJobOffer(mockProfessional, mockProfessional, 1);

        Assertions.assertTrue(mockProfessional.getJobOffers().get(mockMatchRequest));
    }

    @Test
    public void acceptJobOffer_Should_SwitchMatchRequestApplicationStatusToMatched_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.acceptJobOffer(mockProfessional, mockProfessional, 1);

        Assertions.assertEquals(ApplicationStatus.MATCHED, mockMatchRequest.getApplication().getApplicationStatus());
    }

    @Test
    public void acceptJobOffer_Should_SwitchMatchRequestJobOpeningStatusToArchived_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.acceptJobOffer(mockProfessional, mockProfessional, 1);

        Assertions.assertFalse(mockMatchRequest.getJobOpening().isActive());
    }

    @Test
    public void acceptJobOffer_Should_SwitchProfessionalToBusy_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.acceptJobOffer(mockProfessional, mockProfessional, 1);

        Assertions.assertFalse(mockProfessional.getProfessionalInfo().get().isActive());
    }

    @Test
    public void acceptJobOffer_Should_SwitchProfessionalApplicationsStatusToHidden_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        var mockApplication = createMockApplication();
        var mockApplication2 = createMockApplication();
        mockApplication2.setId(2);
        mockProfessional.getApplications().addAll(Set.of(mockApplication, mockApplication2));
        mockProfessional.getJobOffers().put(mockMatchRequest, false);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.acceptJobOffer(mockProfessional, mockProfessional, 1);

        Assertions.assertAll(
                () -> assertEquals(2, mockProfessional.getApplications().size()),
                () -> assertFalse(mockProfessional.getApplications().stream().anyMatch(
                        application -> application.getApplicationStatus().equals(ApplicationStatus.ACTIVE))),
                () -> assertFalse(mockProfessional.getApplications().stream().anyMatch(
                        application -> application.getApplicationStatus().equals(ApplicationStatus.PRIVATE)))
        );
    }

    @Test
    public void acceptJobOffer_Should_CallRepository_WhenSuccessful() {
        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockMatchRequest));

        service.acceptJobOffer(mockProfessional, mockProfessional, 1);

        Mockito.verify(repository, Mockito.times(1)).save(mockMatchRequest);
    }


}
