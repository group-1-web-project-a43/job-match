package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.DuplicateEntityException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.models.Employee;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.repositories.interfaces.EmployeeRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockCompany;
import static com.example.jobmatch.services.Helpers.createMockEmployee;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTests {

    @Mock
    AccountRepository accountRepository;
    @Mock
    EmployeeRepository employeeRepository;
    @Mock
    CompanyService companyService;
    @InjectMocks
    EmployeeServiceImpl employeeService;

    @Test
    public void getAllCompanyEmployees_Should_Throw_WhenCompanyNotAuthorized() {
        var mockCompany = createMockCompany(1);
        var mockCompanyAuth = createMockCompany(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.getAllCompanyEmployees(mockCompanyAuth, mockCompany));
    }

    @Test
    public void getAllCompanyEmployees_ShouldReturnEmptyList_WhenNoCompanyEmployeesExist() {
        //Arrange
        var mockCompany = createMockCompany(1);
        Iterable<Employee> list = new ArrayList<>();
        Mockito.when(employeeRepository.findAllByCompanyEquals(mockCompany))
                .thenReturn(list);
        //Act
        List<Employee> listResult = employeeService.getAllCompanyEmployees(mockCompany, mockCompany);
        //Assert
        Assertions.assertTrue(listResult.isEmpty());
    }

    @Test
    public void getAllCompanyEmployees_Should_CallRepository() {
        // Arrange
        var mockCompany = createMockCompany(1);
        Iterable<Employee> list = new ArrayList<>();
        Mockito.when(employeeRepository.findAllByCompanyEquals(mockCompany))
                .thenReturn(list);
        // Act
        employeeService.getAllCompanyEmployees(mockCompany, mockCompany);
        // Assert
        Mockito.verify(employeeRepository, Mockito.times(1))
                .findAllByCompanyEquals(mockCompany);
    }

    @Test
    public void getAllCompanyEmployees_Should_ReturnEmployeesList() {
        // Arrange
        var mockCompany = createMockCompany(1);
        List<Employee> repositoryList = List.of(createMockEmployee(1), createMockEmployee(2));
        Mockito.when(employeeRepository.findAllByCompanyEquals(mockCompany))
                .thenReturn(repositoryList);
        // Act
        List<Employee> serviceList = employeeService.getAllCompanyEmployees(mockCompany, mockCompany);

        // Assert
        assertAll(
                () -> assertEquals(2, serviceList.size(), "Employees list size was not equal"),
                () -> assertEquals(serviceList.get(0), repositoryList.get(0),
                        "The first employee was not equal"),
                () -> assertEquals(serviceList.get(1), repositoryList.get(1),
                        "The second employee was not equal")
        );
    }

    @Test
    public void getCompanyEmployeeById_Should_Throw_WhenEmployeeNotAuthorized() {
        var mockEmployeeAuth = createMockEmployee(1);
        var mockCompany = createMockCompany(2);
        Mockito.when(companyService.getById(mockCompany.getId()))
                .thenReturn(mockCompany);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.getCompanyEmployeeById(mockEmployeeAuth, 2, 0));
    }

    @Test
    public void getCompanyEmployeeById_Should_ThrowException_When_EmployeeDoesNotExistById() {
        var mockEmployeeAuth = createMockEmployee(1);
        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockEmployeeAuth.getCompany());
        Mockito.when(employeeRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> employeeService.getCompanyEmployeeById(mockEmployeeAuth, 2, 1));
    }

    @Test
    public void getCompanyEmployeeById_Should_ThrowException_If_AttemptingToGetEmployeeOfOtherCompany() {
        var mockEmployeeAuth = createMockEmployee(1);
        var mockEmployeeReturn = createMockEmployee(2);
        mockEmployeeReturn.getCompany().setId(5);
        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockEmployeeAuth.getCompany());
        Mockito.when(employeeRepository.findById(anyInt())).thenReturn(Optional.of(mockEmployeeReturn));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.getCompanyEmployeeById(mockEmployeeAuth, 1, 2));
    }

    @Test
    public void getCompanyEmployeeById_Should_CallRepository_WhenSuccessful() {
        var mockEmployeeAuth = createMockEmployee(1);
        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockEmployeeAuth.getCompany());
        Mockito.when(employeeRepository.findById(anyInt())).thenReturn(Optional.of(mockEmployeeAuth));

        employeeService.getCompanyEmployeeById(mockEmployeeAuth, 1, 1);

        Mockito.verify(employeeRepository, Mockito.times(1)).findById(1);
    }


    @Test
    public void getByUsername_Should_CallRepository() {
        employeeService.getByUsername(anyString());
        Mockito.verify(employeeRepository, Mockito.times(1))
                .getEmployeeByUsernameEqualsIgnoreCase(anyString());
    }

    @Test
    public void create_Should_Throw_WhenCompanyNotAuthorized() {
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(2);
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.create(mockCompany, mockEmployee));
    }

    @Test
    public void create_Should_Throw_When_AccountWithSameUsernameOrEmailAlreadyExists() {
        // Arrange
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(1);

        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockEmployee.getUsername(), mockEmployee.getEmail()))
                .thenReturn(true);

        // Act/Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.create(mockCompany, mockEmployee));
    }

    @Test
    public void create_Should_CallRepository_WhenSuccessful() {
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(1);
        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockEmployee.getUsername(), mockEmployee.getEmail()))
                .thenReturn(false);

        employeeService.create(mockCompany, mockEmployee);

        Mockito.verify(employeeRepository, Mockito.times(1))
                .save(mockEmployee);
    }

    @Test
    public void update_Should_Throw_When_RequestedEmployeeAndRequestedCompanyDoNotMatch() {
        var mockEmployeeToUpdate = createMockEmployee(1);
        var mockCompany = createMockCompany(2);

        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockCompany);
        Mockito.when(employeeRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockEmployeeToUpdate));


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.update(mockEmployeeToUpdate, mockEmployeeToUpdate, mockCompany.getId(),
                        mockEmployeeToUpdate.getId()));
    }

    @Test
    public void update_Should_ThrowException_When_EditorAndRequestedCompanyEmployeeAreDifferentEntities() {
        var mockEmployeeToUpdate = createMockEmployee(1);
        var mockCompany = createMockCompany(1);
        var mockAuthenticated = createMockEmployee(2);

        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockCompany);
        Mockito.when(employeeRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockEmployeeToUpdate));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.update(mockAuthenticated, mockEmployeeToUpdate, mockCompany.getId(),
                        mockEmployeeToUpdate.getId()));
    }

    @Test
    public void update_Should_Throw_When_AccountWithIdenticalEmailAlreadyExists() {
        var mockEmployeeToUpdate = createMockEmployee(1);
        var mockCompany = createMockCompany(1);
        var mockEmployee = createMockEmployee(2);

        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockCompany);
        Mockito.when(employeeRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockEmployeeToUpdate));
        Mockito.when(accountRepository.getAccountByEmailEquals(anyString()))
                .thenReturn(Optional.of(mockEmployee));

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> employeeService.update(mockEmployeeToUpdate, mockEmployeeToUpdate, 1, 1));
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(1);

        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockCompany);
        Mockito.when(employeeRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockEmployee));
        Mockito.when(accountRepository.getAccountByEmailEquals(anyString()))
                .thenReturn(Optional.of(mockEmployee));

        employeeService.update(mockEmployee, mockEmployee, 1, 1);

        Mockito.verify(employeeRepository, Mockito.times(1))
                .save(mockEmployee);
    }

    @Test
    public void delete_Should_ThrowException_IfTryingToDeleteAnotherCompanyEmployee() {
        var mockEmployeeToDelete = createMockEmployee(1);
        var mockCompany = createMockCompany(1);
        var mockCompanyAuth = createMockCompany(2);

        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockCompany);
        Mockito.when(employeeRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockEmployeeToDelete));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> employeeService.delete(mockCompanyAuth, 1, mockEmployeeToDelete.getId()));
    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockEmployee = createMockEmployee(1);
        var mockCompany = createMockCompany(1);

        Mockito.when(companyService.getById(anyInt()))
                .thenReturn(mockCompany);
        Mockito.when(employeeRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockEmployee));

        employeeService.delete(mockCompany, mockCompany.getId(), mockEmployee.getId());

        Mockito.verify(employeeRepository, Mockito.times(1))
                .delete(mockEmployee);
    }
}
