package com.example.jobmatch.services;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.FilteringHelper;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.repositories.interfaces.ApplicationRepository;
import com.example.jobmatch.repositories.interfaces.JobOpeningRepository;
import com.example.jobmatch.services.interfaces.CompanyService;
import com.example.jobmatch.services.interfaces.SkillService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
public class JobOpeningServiceTests {

    @Mock
    JobOpeningRepository repository;

    @Mock
    CompanyService companyService;

    @Mock
    ApplicationRepository applicationRepository;

    @Mock
    FilteringHelper filteringHelper;

    @Mock
    SkillService skillService;

    @InjectMocks
    JobOpeningServiceImpl service;

    @Test
    public void getAll_Should_CallRepository() {
        service.getAll();

        Mockito.verify(repository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAll_Should_OrderById() {
        var mockJobOpening1 = createMockJobOpening();
        var mockJobOpening2 = createMockJobOpening();
        var mockJobOpening3 = createMockJobOpening();

        mockJobOpening1.setId(3);
        mockJobOpening2.setId(2);

        List<JobOpening> mockList = new ArrayList<>();

        mockList.add(mockJobOpening1);
        mockList.add(mockJobOpening3);
        mockList.add(mockJobOpening2);

        Mockito.when(repository.findAll()).thenReturn(mockList);

        List<JobOpening> result = service.getAll();

        Assertions.assertAll(
                () -> assertEquals(result.get(0), mockJobOpening3),
                () -> assertEquals(result.get(1), mockJobOpening2),
                () -> assertEquals(result.get(2), mockJobOpening1)
        );
    }

    @Test
    public void searchJobOpening_Should_NotThrowIfCompanyCannotBeFound() {
        Mockito.when(filteringHelper.getLocationFilter(anyString())).thenReturn(Optional.empty());
        Mockito.when(companyService.getByName(anyString())).thenThrow(EntityNotFoundException.class);

        service.searchJobOpening(
                "specialist", "lorem", "Sofia, Bulgaria", "banana",
                2000.0, 2500.0, "active",
                "name", "desc");

        Mockito.verify(repository, Mockito.times(1)).filter(any(JobFilterOptions.class));
    }

    @Test
    public void searchJobOpening_Should_ThrowException_If_StatusQueryArgumentIsIllegal() {
        Mockito.when(filteringHelper.getLocationFilter(anyString())).thenReturn(Optional.empty());
        Mockito.when(companyService.getByName(anyString())).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.searchJobOpening(
                        "specialist", "lorem", "Sofia, Bulgaria", "banana",
                        2000.0, 2500.0, "banana",
                        "name", "desc"));
    }

    @Test
    public void searchJobOpeningOverload_Should_CallRepository_With_FilterOptions() {
        var mockFilterOptions = createMockJobFilter();

        service.searchJobOpening(mockFilterOptions);

        Mockito.verify(repository, Mockito.times(1)).filter(mockFilterOptions);

    }

    @Test
    public void getPotentialMatchingApplications_Should_ThrowException_If_AttemptedByEmployeeOfAnotherCompany() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(createMockCompany(2));
        var salaryThreshold = 5.0;
        var skillThreshold = 3;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getPotentialMatchingApplications(
                        mockEmployee, mockJobOpening,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingApplications_Should_ThrowException_If_JobOpeningIsInactive() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var salaryThreshold = 5.0;
        var skillThreshold = 3;

        mockJobOpening.setActive(false);

        Assertions.assertThrows(IllegalStateException.class,
                () -> service.getPotentialMatchingApplications(
                        mockEmployee, mockJobOpening,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingApplications_Should_ThrowException_If_SalaryThresholdNegative() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var salaryThreshold = -5.0;
        var skillThreshold = 3;

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getPotentialMatchingApplications(
                        mockEmployee, mockJobOpening,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingApplications_Should_ThrowException_If_SalaryThresholdOver99Percent() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var salaryThreshold = 105;
        var skillThreshold = 3;

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getPotentialMatchingApplications(
                        mockEmployee, mockJobOpening,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingApplications_Should_ThrowException_If_SkillThresholdIsNegative() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var salaryThreshold = 5;
        var skillThreshold = -3;

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getPotentialMatchingApplications(
                        mockEmployee, mockJobOpening,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingApplications_Should_CallRepository() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var salaryThreshold = 5;
        var skillThreshold = 3;

        service.getPotentialMatchingApplications(
                mockEmployee, mockJobOpening,
                salaryThreshold, skillThreshold);

        Mockito.verify(applicationRepository, Mockito.times(1)).filter(any(ApplicationFilterOptions.class));
    }

    @Test
    public void getPotentialMatchingApplications_Should_Filter_ResultList() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var mockSkill = createMockSkill();
        mockSkill.setId(2);
        mockSkill.setSkillName("attention to detail");

        var salaryThreshold = 5.0;
        var skillThreshold = 0;

        List<Application> mockApplicationList = List.of(createMockApplication(), createMockApplication());

        mockApplicationList.get(1).setId(2);
        mockApplicationList.get(1).getSkillSet().put(mockSkill, SkillLevel.EXPERT);

        Mockito.when(applicationRepository.filter(any(ApplicationFilterOptions.class))).thenReturn(mockApplicationList);

        Mockito.when(filteringHelper.checkForMatchingSkills(
                        anyMap(), anyMap(), anyInt()))
                .thenReturn(false).thenReturn(true);


        List<Application> result = service.getPotentialMatchingApplications(
                mockEmployee, mockJobOpening, salaryThreshold, skillThreshold);

        Assertions.assertAll(
                () -> assertEquals(1, result.size()),
                () -> assertNotEquals(mockApplicationList, result),
                () -> assertSame(mockApplicationList.get(1), result.get(0))
        );
    }

    @Test
    public void getById_Should_ThrowException_When_JobOpeningDoesNotExist() {
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(anyInt()));
    }

    @Test
    public void getById_Should_CallRepositoryAndReturnJobOpening_WhenSuccessful() {
        var mockJobOpening = createMockJobOpening();
        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockJobOpening));

        JobOpening result = service.getById(anyInt());

        Mockito.verify(repository, Mockito.times(1)).findById(anyInt());
        Assertions.assertEquals(mockJobOpening, result);
    }

    @Test
    public void getByIdAndCompany_Should_ThrowException_When_CompanyAndJobOpeningDontMatch() {
        var mockJobOpening = createMockJobOpening();
        var mockCompany = createMockCompany(2);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockJobOpening));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByIdAndCompany(1, mockCompany));
    }

    @Test
    public void getByIdAndCompany_Should_CallRepositoryAndReturnJobOpening_When_CompanyAndJobOpeningMatch() {
        var mockJobOpening = createMockJobOpening();

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockJobOpening));

        JobOpening result = service.getByIdAndCompany(1, mockJobOpening.getCompany());

        Mockito.verify(repository, Mockito.times(1)).findById(1);
        Assertions.assertEquals(result, mockJobOpening);

    }

    @Test
    public void create_Should_ThrowException_When_EmployeeCompanyAndJobOpeningCompanyDoNotMatch() {
        var mockJobOpening = createMockJobOpening();

        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(createMockCompany(2));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockEmployee, mockJobOpening));
    }

    @Test
    public void create_Should_setNewJobOpeningStatusToActive() {
        var mockJobOpening = createMockJobOpening();
        mockJobOpening.setActive(false);
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.save(any(JobOpening.class))).thenReturn(mockJobOpening);

        JobOpening result = service.create(mockEmployee, mockJobOpening);

        Assertions.assertTrue(result.isActive());
    }

    @Test
    public void create_Should_CallRepository() {
        var mockJobOpening = createMockJobOpening();
        mockJobOpening.setActive(false);
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.save(any(JobOpening.class))).thenReturn(mockJobOpening);

        service.create(mockEmployee, mockJobOpening);

        Mockito.verify(repository, Mockito.times(1)).save(mockJobOpening);
    }

    @Test
    public void update_Should_ThrowException_When_EmployeeCompanyAndJobOpeningCompanyDoNotMatch() {
        var mockJobOpening = createMockJobOpening();

        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(createMockCompany(2));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockEmployee, mockJobOpening, 1));
    }

    @Test
    public void update_Should_ThrowException_When_InputJobOpeningAndOriginalJobOpeningHaveDifferentCompanies() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        var mockOriginalJobOpening = createMockJobOpening();
        var mockCompany = createMockCompany(2);
        mockOriginalJobOpening.setCompany(mockCompany);

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockOriginalJobOpening));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockEmployee, mockJobOpening, 1));
    }

    @Test
    public void update_Should_MaintainOriginalPublicationDate() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        var mockOriginalJobOpening = createMockJobOpening();
        mockOriginalJobOpening.setCompany(mockJobOpening.getCompany());
        mockOriginalJobOpening.setPublishedOn(LocalDateTime.of
                (2020, 5, 12, 23, 55, 23));

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockOriginalJobOpening));
        Mockito.when(repository.save(mockJobOpening)).thenReturn(mockJobOpening);

        JobOpening result = service.update(mockEmployee, mockJobOpening, 1);

        Assertions.assertEquals(mockOriginalJobOpening.getPublishedOn(), result.getPublishedOn());
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        var mockOriginalJobOpening = createMockJobOpening();
        mockOriginalJobOpening.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockOriginalJobOpening));
        Mockito.when(repository.save(mockJobOpening)).thenReturn(mockJobOpening);

        service.update(mockEmployee, mockJobOpening, 1);

        Mockito.verify(repository, Mockito.times(1)).save(mockJobOpening);
    }

    @Test
    public void addOrModifySkill_Should_ThrowException_When_EditorIsNotAuthorized() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(2);
        mockEmployee.setCompany(createMockCompany(2));
        var mockSkill = createMockSkill();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.addOrModifySkill(mockEmployee, mockJobOpening, mockSkill, SkillLevel.EXPERT));
    }

    @Test
    public void addOrModifySkill_Should_CallSkillService_When_Authorized() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var mockSkill = createMockSkill();

        service.addOrModifySkill(mockEmployee, mockJobOpening, mockSkill, SkillLevel.EXPERT);

        Mockito.verify(skillService, Mockito.times(1))
                .addOrModifySkill(mockJobOpening, mockSkill, SkillLevel.EXPERT);
    }

    @Test
    public void removeSkill_Should_ThrowException_When_EditorIsNotAuthorized() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(2);
        mockEmployee.setCompany(createMockCompany(2));
        var mockSkill = createMockSkill();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.removeSkill(mockEmployee, mockJobOpening, mockSkill));
    }

    @Test
    public void removeSkill_Should_CallSkillService_When_Authorized() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());
        var mockSkill = createMockSkill();
        service.removeSkill(mockEmployee, mockJobOpening, mockSkill);

        Mockito.verify(skillService, Mockito.times(1)).removeSkill(mockJobOpening, mockSkill);
    }

    @Test
    public void deleteCompanyOpening_Should_ThrowException_When_EmployeeCompanyAndJobOpeningCompanyDontMatch() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(createMockCompany(2));

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockJobOpening));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteCompanyOpening(mockEmployee, 1));
    }

    @Test
    public void deleteCompanyOpening_Should_CallRepository_WhenSuccessful() {
        var mockJobOpening = createMockJobOpening();
        var mockEmployee = createMockEmployee(1);
        mockEmployee.setCompany(mockJobOpening.getCompany());

        Mockito.when(repository.findById(anyInt())).thenReturn(Optional.of(mockJobOpening));

        service.deleteCompanyOpening(mockEmployee, 1);

        Mockito.verify(repository, Mockito.times(1)).delete(mockJobOpening);
    }

    @Test
    public void countJobOpeningsByActiveStatus_ShouldReturnZero_When_ListIsEmpty() {
        List<JobOpening> emptyList = new ArrayList<>();
        Mockito.when(repository.searchByStatusActive()).thenReturn(emptyList);

        int result = service.countJobOpeningsByActiveStatus();

        Assertions.assertEquals(0, result);
    }

    @Test
    public void countJobOpeningsByActiveStatus_Should_CountCorrectlyWhenListIsNotEmpty() {
        List<JobOpening> mockList = List.of(createMockJobOpening(), createMockJobOpening());
        Mockito.when(repository.searchByStatusActive()).thenReturn(mockList);

        int result = service.countJobOpeningsByActiveStatus();

        Assertions.assertAll(
                () -> assertNotEquals(0, result),
                () -> assertEquals(2, result)
        );
    }

    @Test
    public void findJobOpeningByPublishedOn_Should_CallRepository() {
        List<JobOpening> emptyList = new ArrayList<>();
        Mockito.when(repository.findJobOpeningByPublishedOn()).thenReturn(emptyList);

        service.findJobOpeningByPublishedOn();

        Mockito.verify(repository, Mockito.times(1)).findJobOpeningByPublishedOn();
    }


}
