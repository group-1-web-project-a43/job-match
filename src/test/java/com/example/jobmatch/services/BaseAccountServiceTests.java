package com.example.jobmatch.services;

import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.models.Account;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.jobmatch.services.Helpers.createMockProfessional;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class BaseAccountServiceTests {

    @Mock
    AccountRepository repository;

    @InjectMocks
    BaseAccountServiceImpl service;

    @Test
    public void getAccountByUsername_Should_ThrowException_When_NoSuchAccountExists() {

        Mockito.when(repository.getAccountByUsernameEqualsIgnoreCase(anyString())).thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getAccountByUsername(anyString()));
    }

    @Test
    public void getAccountByUsername_Should_CallRepositoryAndReturnAccountSubclasses() {
        var mockProfessional = createMockProfessional(1);

        Mockito.when(repository.getAccountByUsernameEqualsIgnoreCase(anyString()))
                .thenReturn(Optional.of(mockProfessional));

        Account result = service.getAccountByUsername(anyString());
        Mockito.verify(repository, Mockito.times(1))
                .getAccountByUsernameEqualsIgnoreCase(anyString());

        Assertions.assertEquals(mockProfessional, result);

    }

}
