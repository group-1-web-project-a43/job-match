package com.example.jobmatch.services;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.exceptions.*;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.Professional;
import com.example.jobmatch.models.dtos.PasswordDto;
import com.example.jobmatch.repositories.interfaces.AccountRepository;
import com.example.jobmatch.repositories.interfaces.ProfessionalsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.example.jobmatch.services.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class ProfessionalServiceTests {

    @Mock
    ProfessionalsRepository professionalsRepository;
    @Mock
    AccountRepository accountRepository;

    @InjectMocks
    ProfessionalServiceImpl professionalService;

    @Test
    public void getAllClosedContracts_Should_CallRepository() {
        professionalService.getAllClosedContracts();
        Mockito.verify(professionalsRepository, Mockito.times(1))
                .countProfessionalsByJobOffersIsTrue();
    }



    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoUsersExist() {
        //Arrange

        Iterable<Professional> list = new ArrayList<>();
        Mockito.when(professionalsRepository.findAll())
                .thenReturn(list);
        //Act
        List<Professional> listResult = professionalService.getAll();
        //Assert
        Assertions.assertTrue(listResult.isEmpty());
    }

    @Test
    public void getAll_Should_CallRepository() {
        // Arrange
        Iterable<Professional> emptyList = new ArrayList<>();
        Mockito.when(professionalsRepository.findAll())
                .thenReturn(emptyList);
        // Act
        professionalService.getAll();
        // Assert
        Mockito.verify(professionalsRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    public void getAll_Should_ReturnProfessionalList() {
        // Arrange
        List<Professional> repositoryList = List.of(createMockProfessional(1), createMockProfessional(2));
        Mockito.when(professionalsRepository.findAll()).thenReturn(repositoryList);

        // Act
        List<Professional> profServiceList = professionalService.getAll();

        // Assert
        assertAll(
                () -> assertEquals(2, profServiceList.size(), "Prof list size was not equal"),
                () -> assertEquals(profServiceList.get(0), repositoryList.get(0),
                        "The first professional was not equal"),
                () -> assertEquals(profServiceList.get(1), repositoryList.get(1),
                        "The second professional was not equal")
        );
    }

    @Test
    public void searchProfessionalByName_Should_ThrowException_IfKeywordIsEmpty() {
        Assertions.assertThrows(EmptySearchException.class,
                () -> professionalService.searchProfessionalByName(
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty()));
    }

    @Test
    public void searchProfessionalByName_Should_CallRepository_IfKeywordIsPresent() {
        List<Professional> mockProfessionals = List.of(createMockProfessional(1), createMockProfessional(2));
        Mockito.when(professionalsRepository.findProfessionalByName(anyString())).thenReturn(mockProfessionals);

        professionalService.searchProfessionalByName(
                Optional.of("kiril petkov"),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        Mockito.verify(professionalsRepository, Mockito.times(1))
                .findProfessionalByName("kiril petkov");
    }

    @Test
    public void searchProfessionalByName_Should_NotManipulateCollection() {
        List<Professional> mockProfessionals = List.of(createMockProfessional(1), createMockProfessional(2));
        Mockito.when(professionalsRepository.findProfessionalByName(anyString())).thenReturn(mockProfessionals);

        List<Professional> mockServiceList = professionalService.searchProfessionalByName(
                Optional.of("kiril petkov"),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.empty());

        assertAll(
                () -> assertSame(mockServiceList, mockProfessionals,
                        "Professionals list should be the same"),
                () -> assertEquals(2, mockServiceList.size(),
                        "Beer list size was not equal"),
                () -> assertEquals(mockServiceList.get(0), mockProfessionals.get(0),
                        "The first beer was not equal"),
                () -> assertEquals(mockServiceList.get(1), mockProfessionals.get(1),
                        "The second beer was not equal")
        );
    }

    @Test
    public void getById_Should_ReturnProfessional_WhenIdExists() {
        //Arrange
        var mockProfessional = createMockProfessional(1);


        Mockito.when(professionalsRepository.findById(mockProfessional.getId()))
                .thenReturn(Optional.of(mockProfessional));
        //Act
        Professional receivedUser = professionalService.getById(mockProfessional.getId());
        //Assert
        Assertions.assertSame(mockProfessional, receivedUser);
    }

    @Test
    public void getById_Should_ThrowException_WhenIdDoesNotExist() {
        //Arrange
        var mockProfessional = createMockProfessional(1);
        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.empty());

        //Act/Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> professionalService.getById(2));
    }

    @Test
    public void getByUsername_Should_ReturnProfessional_WhenUsernameExists() {
        //Arrange
        var mockProfessional = createMockProfessional(1);
        Mockito.when(professionalsRepository
                        .findProfessionalByUsernameEqualsIgnoreCase(mockProfessional.getUsername()))
                .thenReturn(Optional.of(mockProfessional));

        //Act
        Professional receivedUser = professionalService.getByUsername(mockProfessional.getUsername());
        //Assert
        Assertions.assertSame(mockProfessional, receivedUser);
    }

    @Test
    public void getByUsername_Should_ThrowException_WhenUsernameDoesNotExist() {
        //Arrange
        var mockProfessional = createMockProfessional(1);
        Mockito.when(professionalsRepository.findProfessionalByUsernameEqualsIgnoreCase(anyString()))
                .thenReturn(Optional.empty());

        //Act/Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> professionalService.getByUsername(mockProfessional.getUsername()));
    }

    @Test
    public void getAllProfessionalApplications_Should_ThrowException_When_ProfessionalIsNotAuthorized() {
        var mockProfessional = createMockProfessional(1);
        var mockProfessional2 = createMockProfessional(2);
        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> professionalService.getAllProfessionalApplications(mockProfessional2, 1));
    }

    @Test
    public void getAllProfessionalApplications_Should_ReturnAllOfAProfessionalsApplications() {
        var mockApplication = createMockApplication();
        var mockProfessional = mockApplication.getProfessional();
        var mockApplication2 = createMockApplication();
        mockApplication2.setId(2);
        mockApplication2.setName("asd asda");
        mockApplication2.setApplicationStatus(ApplicationStatus.PRIVATE);
        mockProfessional.getApplications().addAll(Set.of(mockApplication, mockApplication2));

        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));

        Set<Application> result = professionalService.getAllProfessionalApplications(mockProfessional, 1);

        Assertions.assertAll(
                () -> assertEquals(2, result.size()),
                () -> assertSame(result, mockProfessional.getApplications())
        );
    }

    @Test
    public void create_Should_CallRepository_When_UserWithSameUsernameOrEmailDoesNotExist() {
        // Arrange
        Professional mockUser = createMockProfessional(0);

        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockUser.getUsername(), mockUser.getEmail()))
                .thenReturn(false);

        // Act
        professionalService.create(mockUser);

        // Assert
        Mockito.verify(professionalsRepository, Mockito.times(1))
                .save(mockUser);
    }

    @Test
    public void create_Should_Throw_When_UserWithSameUsernameOrEmailAlreadyExists() {
        // Arrange
        var mockProfessional = createMockProfessional(0);

        Mockito.when(accountRepository.existsAccountByUsernameEqualsIgnoreCaseOrEmailEqualsIgnoreCase(
                        mockProfessional.getUsername(), mockProfessional.getEmail()))
                .thenReturn(true);

        // Act/Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> professionalService.create(mockProfessional));

    }

    @Test
    public void update_Should_ThrowException_When_AuthenticatedUserIsNotAuthorizedToEdit() {
        // Arrange
        var mockUpdateObject = createMockProfessional(0);
        var mockProfessional = createMockProfessional(1);
        var mockProfessional2 = createMockProfessional(2);

        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));

        // Act/Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> professionalService.update(mockProfessional2, mockUpdateObject, 1));
    }

    @Test
    public void update_Should_ThrowException_When_AccountWithIdenticalEmailAlreadyExists() {
        var mockUpdateObject = createMockProfessional(0);
        var mockProfessional = createMockProfessional(1);

        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));
        Mockito.when(accountRepository.existsAccountByEmailAndIdNot(mockProfessional.getEmail(), 1))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> professionalService.update(mockProfessional, mockUpdateObject, 1));
    }

    @Test
    public void update_Should_Maintain_ProfessionalsCompositionRelationships() {
        //Arrange
        var mockUpdateObject = createMockProfessional(0);

        var mockMatchRequest = createMockMatchRequest();
        var mockProfessional = mockMatchRequest.getApplication().getProfessional();
        mockProfessional.setMainApplication(mockMatchRequest.getApplication());
        mockProfessional.getJobOffers().put(mockMatchRequest, false);
        mockProfessional.getApplications().add(mockMatchRequest.getApplication());
        mockProfessional.getApplications().add(createMockApplication());

        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));
        Mockito.when(accountRepository.existsAccountByEmailAndIdNot(mockProfessional.getEmail(), 1))
                .thenReturn(false);

        //Act
        professionalService.update(mockProfessional, mockUpdateObject, 1);

        //Assert
        Assertions.assertAll(
                () -> assertEquals(mockProfessional.getMainApplication().get(),
                        mockUpdateObject.getMainApplication().get(),
                        "main application should be the same"),
                () -> assertEquals(mockProfessional.getId(), mockUpdateObject.getId(),
                        "id should be the same"),
                () -> assertSame(mockProfessional.getJobOffers(), mockUpdateObject.getJobOffers(),
                        "job offers should be the same"),
                () -> assertSame(mockProfessional.getApplications(), mockUpdateObject.getApplications(),
                        "applications list should be the same"),
                () -> assertSame(mockProfessional.getProfessionalInfo().get(),
                        mockUpdateObject.getProfessionalInfo().get(),
                        "info should be the same")
        );
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        //Arrange
        var mockUpdateObject = createMockProfessional(0);
        var mockProfessional = createMockProfessional(1);

        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));
        Mockito.when(accountRepository.existsAccountByEmailAndIdNot(mockProfessional.getEmail(), 1))
                .thenReturn(false);

        professionalService.update(mockProfessional, mockUpdateObject, 1);

        Mockito.verify(professionalsRepository, Mockito.times(1))
                .save(mockUpdateObject);
    }

    @Test
    public void setMainApplication_Should_ThrowException_When_ProfessionalIsNotAuthorized() {
        var mockProfessional = createMockProfessional(1);
        var mockAuthenticated = createMockProfessional(2);

        var mockApplication = createMockApplication();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> professionalService.setMainApplication(mockAuthenticated, mockApplication, mockProfessional));
    }

    @Test
    public void setMainApplication_Should_ThrowException_When_ProfessionalsGetApplicationsDoesNotContainApplication() {
        var mockApplication = createMockApplication();
        var mockProfessional = mockApplication.getProfessional();

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> professionalService.setMainApplication(mockProfessional, mockApplication, mockProfessional));
    }

    @Test
    public void setMainApplication_Should_ThrowException_When_ApplicationAlreadySetToMainApplication() {
        var mockApplication = createMockApplication();
        var mockProfessional = mockApplication.getProfessional();
        mockProfessional.getApplications().add(mockApplication);
        mockProfessional.setMainApplication(mockApplication);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> professionalService.setMainApplication(mockProfessional, mockApplication, mockProfessional));
    }

    @Test
    public void setMainApplication_Should_CallRepository_And_SetMainApplicationOfProfessional() {
        var mockApplication = createMockApplication();
        var mockProfessional = mockApplication.getProfessional();
        mockProfessional.getApplications().add(mockApplication);

        professionalService.setMainApplication(mockProfessional, mockApplication, mockProfessional);

        Mockito.verify(professionalsRepository, Mockito.times(1)).save(mockProfessional);
        Assertions.assertEquals(mockProfessional.getMainApplication().get(), mockApplication);
    }


    @Test
    public void changePassword_Should_ThrowException_IfProfessionalIsNotAuthorized() {
        var mockAuthenticated = createMockProfessional(1);
        var mockProfessional = createMockProfessional(2);

        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockProfessional));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> professionalService.changePassword(mockAuthenticated, new PasswordDto(), 1));
    }

    @Test
    public void changePassword_Should_ThrowException_IfPasswordMismatchesOldPassword() {
        var mockAuthenticated = createMockProfessional(1);
        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword("banana");

        Assertions.assertThrows(PasswordMismatchException.class,
                () -> professionalService.changePassword(mockAuthenticated, passwordChange, 1));
    }

    @Test
    public void changePassword_Should_ThrowException_ifNewPasswordIsDuplicateOfOld() {
        var mockAuthenticated = createMockProfessional(1);
        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword(mockAuthenticated.getPassword());
        passwordChange.setNewPassword(passwordChange.getOldPassword());
        passwordChange.setNewPasswordConfirmation(passwordChange.getOldPassword());

        Assertions.assertThrows(PasswordDuplicateException.class,
                () -> professionalService.changePassword(mockAuthenticated, passwordChange, 1));
    }

    @Test
    public void changePassword_Should_ThrowException_IfNewPasswordAndConfirmationMismatch() {
        var mockAuthenticated = createMockProfessional(1);
        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword(mockAuthenticated.getPassword());
        passwordChange.setNewPassword("banana");
        passwordChange.setNewPasswordConfirmation("orangutan");

        Assertions.assertThrows(PasswordMismatchException.class,
                () -> professionalService.changePassword(mockAuthenticated, passwordChange, 1));
    }

    @Test
    public void changePassword_Should_CallRepository_WhenSuccessful() {
        var mockAuthenticated = createMockProfessional(1);
        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockAuthenticated));

        var passwordChange = new PasswordDto();
        passwordChange.setOldPassword(mockAuthenticated.getPassword());
        passwordChange.setNewPassword("orangutan");
        passwordChange.setNewPasswordConfirmation("orangutan");

        professionalService.changePassword(mockAuthenticated, passwordChange, 1);


        Assertions.assertEquals(mockAuthenticated.getPassword(), passwordChange.getNewPassword());
        Mockito.verify(professionalsRepository, Mockito.times(1))
                .save(mockAuthenticated);
    }


    @Test
    public void delete_Should_ThrowException_IfTryingToDeleteAnotherProfessional() {
        var mockAuthenticated = createMockProfessional(1);
        var mockProfessional = createMockProfessional(2);
        mockProfessional.setUsername("bananana");
        mockProfessional.setEmail("kapanafest@telerik.bg");

        Mockito.when(professionalsRepository.findById(anyInt())).thenReturn(Optional.of(mockProfessional));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> professionalService.delete(mockAuthenticated, 1));

    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockAuthenticated = createMockProfessional(1);
        var mockProfessional = mockAuthenticated;

        Mockito.when(professionalsRepository.findById(anyInt()))
                .thenReturn(Optional.of(mockProfessional));

        professionalService.delete(mockAuthenticated, 1);

        Mockito.verify(professionalsRepository, Mockito.times(1))
                .delete(mockProfessional);
    }

}
