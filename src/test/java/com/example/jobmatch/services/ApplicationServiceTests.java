package com.example.jobmatch.services;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.exceptions.ApplicationMismatchException;
import com.example.jobmatch.exceptions.EntityNotFoundException;
import com.example.jobmatch.exceptions.UnauthorizedOperationException;
import com.example.jobmatch.helpers.FilteringHelper;
import com.example.jobmatch.models.Application;
import com.example.jobmatch.models.JobOpening;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;
import com.example.jobmatch.repositories.interfaces.ApplicationRepository;
import com.example.jobmatch.repositories.interfaces.JobOpeningRepository;
import com.example.jobmatch.services.interfaces.SkillService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.example.jobmatch.services.Helpers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
public class ApplicationServiceTests {
    @Mock
    ApplicationRepository applicationRepository;
    @Mock
    JobOpeningRepository jobOpeningRepository;

    @Mock
    FilteringHelper filteringHelper;

    @Mock
    SkillService skillService;

    @InjectMocks
    ApplicationServiceImpl service;


    @Test
    public void getAll_Should_CallRepository() {
        service.getAll();
        Mockito.verify(applicationRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void searchActiveApplications_Should_CallRepository() {
        Mockito.when(filteringHelper.getLocationFilter(anyString())).thenReturn(Optional.empty());

        service.searchActiveApplications(
                "specialist", "lorem", "Sofia, Bulgaria",
                2000.0, 2500.0,
                "name", "desc");

        Mockito.verify(applicationRepository, Mockito.times(1))
                .filter(any(ApplicationFilterOptions.class));
    }

    @Test
    public void searchActiveApplicationsOverloadedMethod_Should_CallRepository() {
        var mockFilterOptions = createMockApplicationFilter();
        service.searchActiveApplications(mockFilterOptions);

        Mockito.verify(applicationRepository, Mockito.times(1))
                .filter(mockFilterOptions);
    }


    @Test
    public void getPotentialMatchingJobOpenings_Should_ThrowException_When_ProfessionalIsNotAuthorized() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(2);
        double salaryThreshold = 5.0;
        int skillThreshold = 3;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getPotentialMatchingJobOpenings(
                        mockProfessional, mockApplication,
                        salaryThreshold, skillThreshold));

    }

    @Test
    public void getPotentialMatchingJobOpenings_Should_ThrowException_When_ApplicationIsAlreadyMatched() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(1);
        double salaryThreshold = 5.0;
        int skillThreshold = 3;

        mockApplication.setApplicationStatus(ApplicationStatus.MATCHED);

        Assertions.assertThrows(IllegalStateException.class,
                () -> service.getPotentialMatchingJobOpenings(
                        mockProfessional, mockApplication,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingJobOpenings_Should_ThrowException_When_SalaryThresholdIsNegative() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(1);
        double salaryThreshold = -5.0;
        int skillThreshold = 3;


        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getPotentialMatchingJobOpenings(
                        mockProfessional, mockApplication,
                        salaryThreshold, skillThreshold));

    }

    @Test
    public void getPotentialMatchingJobOpenings_Should_ThrowException_When_SalaryThresholdIsOver99Percent() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(1);
        double salaryThreshold = 102;
        int skillThreshold = 3;


        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getPotentialMatchingJobOpenings(
                        mockProfessional, mockApplication,
                        salaryThreshold, skillThreshold));

    }

    @Test
    public void getPotentialMatchingJobOpenings_Should_ThrowException_When_SkillThresholdIsNegative() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(1);
        double salaryThreshold = 5;
        int skillThreshold = -2;


        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.getPotentialMatchingJobOpenings(
                        mockProfessional, mockApplication,
                        salaryThreshold, skillThreshold));
    }

    @Test
    public void getPotentialMatchingJobOpenings_Should_ReturnListWhenSkillsMatch() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(1);
        double salaryThreshold = 5.0;
        int skillThreshold = 3;

        List<JobOpening> mockJobList = List.of(createMockJobOpening(), createMockJobOpening());
        mockJobList.get(1).setId(2);
        mockJobList.get(1).setMaxSalary(3000.0);

        Mockito.when(jobOpeningRepository.filter(any(JobFilterOptions.class))).thenReturn(mockJobList);
        Mockito.when(filteringHelper.checkForMatchingSkills(
                        anyMap(),
                        anyMap(),
                        anyInt()))
                .thenReturn(true);

        List<JobOpening> result = service.getPotentialMatchingJobOpenings(mockProfessional, mockApplication,
                salaryThreshold, skillThreshold);

        Assertions.assertAll(
                () -> assertEquals(mockJobList.size(), result.size(), "size of return list should be same"),
                () -> assertSame(mockJobList.get(0), result.get(0), "first job opening should be same"),
                () -> assertSame(mockJobList.get(1), result.get(1), "second job opening should be same")
        );
    }

    @Test
    public void getPotentialMatchingJobOpenings_Should_ReturnEmptyListWhenSkillsDontMatch() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(1);
        double salaryThreshold = 5.0;
        int skillThreshold = 3;

        List<JobOpening> mockJobList = List.of(createMockJobOpening(), createMockJobOpening());
        mockJobList.get(1).setId(2);
        mockJobList.get(1).setMaxSalary(3000.0);

        Mockito.when(jobOpeningRepository.filter(any(JobFilterOptions.class))).thenReturn(mockJobList);
        Mockito.when(filteringHelper.checkForMatchingSkills(
                        anyMap(),
                        anyMap(),
                        anyInt()))
                .thenReturn(false);

        List<JobOpening> result = service.getPotentialMatchingJobOpenings(mockProfessional, mockApplication,
                salaryThreshold, skillThreshold);

        Assertions.assertAll(
                () -> assertNotEquals(mockJobList.size(), result.size(), "size of return list should be same"),
                () -> assertEquals(0, result.size(), "result list should be empty")
        );
    }

    @Test
    public void getById_Should_ThrowException_When_ApplicationIsNotFound() {
        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.getById(anyInt()));
    }

    @Test
    public void getById_Should_CallRepository_When_Successful() {
        var mockApplication = createMockApplication();
        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplication));

        service.getById(anyInt());

        Mockito.verify(applicationRepository, Mockito.times(1)).findById(anyInt());
    }

    @Test
    public void getProfessionalApplicationById_Should_ThrowException_IfApplicationAndProfessionalDontMatch() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(2);
        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplication));

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> service.getProfessionalApplicationById(mockProfessional, 2));
    }

    @Test
    public void getProfessionalApplicationById_Should_ReturnApplicationAndCallRepository_WhenSuccessful() {
        var mockApplication = createMockApplication();
        var mockProfessional = mockApplication.getProfessional();
        mockProfessional.getApplications().add(mockApplication);

        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplication));

        Application result = service.getProfessionalApplicationById(mockProfessional, 1);

        Mockito.verify(applicationRepository, Mockito.times(1)).findById(1);
        Assertions.assertEquals(result, mockApplication);

    }

    @Test
    public void create_Should_ThrowException_IfCreatorIsAttemptingToCreateApplicationForAnotherUser() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockProfessional, mockApplication));
    }

    @Test
    public void create_Should_SetActiveStatus_WhenCreatingNewApplication() {
        var mockApplication = createMockApplication();
        mockApplication.setApplicationStatus(ApplicationStatus.HIDDEN);

        Mockito.when(applicationRepository.save(mockApplication)).thenReturn(mockApplication);

        Application result = service.create(mockApplication.getProfessional(), mockApplication);

        Assertions.assertEquals(ApplicationStatus.ACTIVE, result.getApplicationStatus());
    }

    @Test
    public void create_Should_CallRepository_WhenSuccessful() {
        var mockApplication = createMockApplication();

        service.create(mockApplication.getProfessional(), mockApplication);

        Mockito.verify(applicationRepository, Mockito.times(1))
                .save(mockApplication);
    }

    @Test
    public void update_Should_ThrowException_IfEditorIsNotAuthorized() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockProfessional, mockApplication, 1));
    }

    @Test
    public void update_Should_MaintainExistingApplicationSkillset() {
        var mockApplication = createMockApplication();
        var mockApplicationFromDatabase = createMockApplication();
        var newSkill = createMockSkill();
        newSkill.setId(2);
        newSkill.setSkillName("attention to detail");
        mockApplicationFromDatabase.getSkillSet().put(newSkill, SkillLevel.EXPERT);

        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplicationFromDatabase));
        Mockito.when(applicationRepository.save(any(Application.class))).thenReturn(mockApplication);

        Application result = service.update(mockApplication.getProfessional(), mockApplication, 1);

        Assertions.assertAll(
                () -> Assertions.assertEquals(result.getSkillSet().entrySet().size(),
                        mockApplicationFromDatabase.getSkillSet().entrySet().size()),
                () -> Assertions.assertEquals(result.getSkillSet().entrySet(),
                        mockApplicationFromDatabase.getSkillSet().entrySet())
        );
    }

    @Test
    public void update_Should_CallRepository_WhenSuccessful() {
        var mockApplication = createMockApplication();
        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplication));

        service.update(mockApplication.getProfessional(), mockApplication, 1);

        Mockito.verify(applicationRepository, Mockito.times(1))
                .save(mockApplication);
    }

    @Test
    public void addOrModifySkill_Should_ThrowException_When_EditorIsNotAuthorized() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(2);
        var mockSkill = createMockSkill();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.addOrModifySkill(mockProfessional, mockApplication, mockSkill, SkillLevel.EXPERT));
    }

    @Test
    public void addOrModifySkill_Should_CallSkillService_When_Authorized() {
        var mockApplication = createMockApplication();
        var mockSkill = createMockSkill();
        service.addOrModifySkill(mockApplication.getProfessional(), mockApplication, mockSkill, SkillLevel.EXPERT);

        Mockito.verify(skillService, Mockito.times(1))
                .addOrModifySkill(mockApplication, mockSkill, SkillLevel.EXPERT);
    }

    @Test
    public void removeSkill_Should_ThrowException_When_EditorIsNotAuthorized() {
        var mockApplication = createMockApplication();
        var mockProfessional = createMockProfessional(2);
        var mockSkill = createMockSkill();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.removeSkill(mockProfessional, mockApplication, mockSkill));
    }

    @Test
    public void removeSkill_Should_CallSkillService_When_Authorized() {
        var mockApplication = createMockApplication();
        var mockSkill = createMockSkill();

        service.removeSkill(mockApplication.getProfessional(), mockApplication, mockSkill);

        Mockito.verify(skillService, Mockito.times(1)).removeSkill(mockApplication, mockSkill);
    }


    @Test
    public void delete_Should_ThrowException_IfTryingToDeleteAnotherProfessionalsApplication() {
        var mockProfessional = createMockProfessional(1);
        var mockAuthenticated = createMockProfessional(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockAuthenticated, mockProfessional, 1));
    }

    @Test
    public void delete_Should_ThrowException_WhenTryingToViewAnotherUsersApplication() {
        var mockApplication = createMockApplication();
        var mockAuthenticated = createMockProfessional(2);

        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplication));

        Assertions.assertThrows(ApplicationMismatchException.class,
                () -> service.delete(mockAuthenticated, mockAuthenticated, 2));

    }

    @Test
    public void delete_Should_CallRepository_WhenSuccessful() {
        var mockApplication = createMockApplication();
        var mockAuthenticated = createMockProfessional(1);

        Mockito.when(applicationRepository.findById(anyInt())).thenReturn(Optional.of(mockApplication));

        service.delete(mockAuthenticated, mockAuthenticated, 1);

        Mockito.verify(applicationRepository, Mockito.times(1))
                .delete(mockApplication);

    }


}
