package com.example.jobmatch.services;

import com.example.jobmatch.enums.ApplicationStatus;
import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.models.*;
import com.example.jobmatch.models.filters.ApplicationFilterOptions;
import com.example.jobmatch.models.filters.JobFilterOptions;

import java.time.LocalDateTime;

public class Helpers {

    public static Professional createMockProfessional(int id) {
        var mockProfessional = new Professional();
        mockProfessional.setId(id);
        mockProfessional.setFirstName("Kiril");
        mockProfessional.setLastName("Angelov");
        mockProfessional.setEmail("kiril@abv.bg");
        mockProfessional.setUsername("kirl99");
        mockProfessional.setPassword("kirilpass1");
        return mockProfessional;
    }

    public static Company createMockCompany(int id) {
        var mockCompany = new Company();
        mockCompany.setId(id);
        mockCompany.setName("Telerik");
        mockCompany.setEmail("telerik@abv.bg");
        mockCompany.setUsername("telerikacademy");
        mockCompany.setPassword("password1");
        return mockCompany;
    }

    public static Employee createMockEmployee(int id) {
        var mockEmployee = new Employee();
        mockEmployee.setId(id);
        mockEmployee.setName("Peter Karaboev");
        mockEmployee.setEmail("peterk@telerik.bg");
        mockEmployee.setUsername("petertelerik");
        mockEmployee.setPassword("password1");
        mockEmployee.setCompany(createMockCompany(1));
        return mockEmployee;
    }

    public static Location createMockLocation() {
        var mockLocation = new Location();
        mockLocation.setId(1);
        mockLocation.setCity("Sofia");
        mockLocation.setCountry("Bulgaria");
        return mockLocation;
    }

    public static Skill createMockSkill() {
        var mockSkill = new Skill();
        mockSkill.setId(1);
        mockSkill.setSkillName("leadership");
        return mockSkill;
    }

    public static ProfessionalInfo createMockProfessionalInfo() {
        var mockProfessionalInfo = new ProfessionalInfo();
        mockProfessionalInfo.setId(1);
        mockProfessionalInfo.setActive(true);
        mockProfessionalInfo.setPictureUrl("/static/images/default.png");
        mockProfessionalInfo.setDescription("lorem ipsum dorem sit amet");
        mockProfessionalInfo.setLocation(createMockLocation());
        mockProfessionalInfo.getLocation().setId(1);
        mockProfessionalInfo.setProfessional(createMockProfessional(1));
        mockProfessionalInfo.getProfessional().setProfessionalInfo(mockProfessionalInfo);
        return mockProfessionalInfo;
    }

    public static CompanyInfo createMockCompanyInfo() {
        var mockCompanyInfo = new CompanyInfo();
        mockCompanyInfo.setId(1);
        mockCompanyInfo.setPictureUrl("/static/images/default.png");
        mockCompanyInfo.setDescription("lorem ipsum dorem sit amet");
        mockCompanyInfo.setLocation(createMockLocation());
        mockCompanyInfo.getLocation().setId(1);
        mockCompanyInfo.setCompany(createMockCompany(1));
        mockCompanyInfo.getCompany().setCompanyInfo(mockCompanyInfo);
        return mockCompanyInfo;
    }

    public static JobOpening createMockJobOpening() {
        var mockJobOpening = new JobOpening();
        mockJobOpening.setId(1);
        CompanyInfo companyInfo = createMockCompanyInfo();
        Company company = companyInfo.getCompany();
        company.setCompanyInfo(companyInfo);
        mockJobOpening.setCompany(company);
        mockJobOpening.setDescription("lorem ipsum");
        mockJobOpening.setLocation(createMockLocation());
        mockJobOpening.getLocation().setId(1);
        mockJobOpening.setActive(true);
        mockJobOpening.setPublishedOn(LocalDateTime.now());
        mockJobOpening.setMinSalary(1500.0);
        mockJobOpening.setMaxSalary(2000.0);
        mockJobOpening.setName("IT Manager");
        mockJobOpening.setRemote(false);
        mockJobOpening.getSkillSet().put(createMockSkill(), SkillLevel.EXPERT);
        company.getJobOpenings().add(mockJobOpening);
        return mockJobOpening;
    }

    public static Application createMockApplication() {
        var mockApplication = new Application();
        mockApplication.setId(1);
        ProfessionalInfo profInfo = createMockProfessionalInfo();
        Professional professional = profInfo.getProfessional();
        professional.setProfessionalInfo(profInfo);
        mockApplication.setProfessional(professional);
        mockApplication.setDescription("lorem ipsum");
        mockApplication.setLocation(createMockLocation());
        mockApplication.getLocation().setId(1);
        mockApplication.setApplicationStatus(ApplicationStatus.ACTIVE);
        mockApplication.setMinSalary(1500.0);
        mockApplication.setMaxSalary(2000.0);
        mockApplication.setName("IT Manager");
        mockApplication.setRemote(false);
        mockApplication.getSkillSet().put(createMockSkill(), SkillLevel.EXPERT);
        return mockApplication;
    }

    public static JobFilterOptions createMockJobFilter() {
        var jobFilter = new JobFilterOptions();
        jobFilter.setName("banana");
        jobFilter.setDescription("lorem ipsum");
        jobFilter.setLocation(createMockLocation().getId());
        jobFilter.setCompany(null);
        jobFilter.setMinSalary(1500.0);
        jobFilter.setMaxSalary(2000.0);
        jobFilter.setSortBy("name");
        jobFilter.setIsActive(true);
        jobFilter.setSortOrder("desc");

        return jobFilter;
    }

    public static ApplicationFilterOptions createMockApplicationFilter() {
        var appFilter = new ApplicationFilterOptions();
        appFilter.setName("banana");
        appFilter.setDescription("lorem ipsum");
        appFilter.setLocation(createMockLocation().getId());
        appFilter.setMinSalary(1500.0);
        appFilter.setMaxSalary(2000.0);
        appFilter.setSortBy("name");
        appFilter.setSortOrder("desc");
        return appFilter;
    }

    public static MatchRequest createMockMatchRequest() {
        var mockJobOpening = createMockJobOpening();
        var mockApplication = createMockApplication();
        mockApplication.setId(2);
        var mockMatchRequest = new MatchRequest();
        mockMatchRequest.setId(1);
        mockMatchRequest.setApplication(mockApplication);
        mockMatchRequest.setJobOpening(mockJobOpening);
        mockMatchRequest.setMatch(false);
        return mockMatchRequest;
    }


}
