package com.example.jobmatch.helpers;

import com.example.jobmatch.enums.SkillLevel;
import com.example.jobmatch.models.Skill;
import com.example.jobmatch.services.interfaces.LocationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.example.jobmatch.services.Helpers.createMockLocation;
import static com.example.jobmatch.services.Helpers.createMockSkill;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class FilteringHelperTests {

    @Mock
    LocationService service;

    @InjectMocks
    FilteringHelper filteringHelper;

    @Test
    public void checkForMatchingSkills_Should_ReturnTrue_When_SkillSetAndThresholdMatchRequirements() {
        Map<Skill, SkillLevel> skillSet = new HashMap<>();
        Map<Skill, SkillLevel> requirements = new HashMap<>();
        int skillTreshold = 2;

        var skill1 = createMockSkill();
        var skill2 = createMockSkill();
        var skill3 = createMockSkill();
        var skill4 = createMockSkill();
        var skill5 = createMockSkill();

        skill2.setSkillName("attention");
        skill2.setId(2);
        skill3.setSkillName("sailors");
        skill3.setId(3);
        skill4.setSkillName("banana");
        skill4.setId(4);
        skill5.setSkillName("cat");
        skill5.setId(5);

        Set<Skill> tempSet = new java.util.HashSet<>(Set.of(skill1, skill2, skill3, skill4, skill5));

        tempSet.forEach(skill -> requirements.putIfAbsent(skill, SkillLevel.ADVANCED));
        tempSet.remove(skill1);
        tempSet.remove(skill2);

        tempSet.forEach(skill -> skillSet.putIfAbsent(skill, SkillLevel.EXPERT));

        boolean result = filteringHelper.checkForMatchingSkills(skillSet, requirements, skillTreshold);
        assertTrue(result);
    }

    @Test
    public void checkForMatchingSkills_Should_ReturnFalse_When_SkillSetAndThresholdDoNotMatchRequirements() {
        Map<Skill, SkillLevel> skillSet = new HashMap<>();
        Map<Skill, SkillLevel> requirements = new HashMap<>();
        int skillTreshold = 0;

        var skill1 = createMockSkill();
        var skill2 = createMockSkill();
        var skill3 = createMockSkill();
        var skill4 = createMockSkill();
        var skill5 = createMockSkill();

        skill2.setSkillName("attention");
        skill2.setId(2);
        skill3.setSkillName("sailors");
        skill3.setId(3);
        skill4.setSkillName("banana");
        skill4.setId(4);
        skill5.setSkillName("cat");
        skill5.setId(5);

        Set<Skill> tempSet = new java.util.HashSet<>(Set.of(skill1, skill2, skill3, skill4, skill5));

        tempSet.forEach(skill -> requirements.putIfAbsent(skill, SkillLevel.ADVANCED));
        tempSet.remove(skill1);
        tempSet.remove(skill2);

        tempSet.forEach(skill -> skillSet.putIfAbsent(skill, SkillLevel.EXPERT));

        boolean result = filteringHelper.checkForMatchingSkills(skillSet, requirements, skillTreshold);
        Assertions.assertFalse(result);
    }

    @Test
    public void checkForMatchingSkills_Should_NotChangeOriginalCollection() {
        Map<Skill, SkillLevel> skillSet = new HashMap<>();
        Map<Skill, SkillLevel> requirements = new HashMap<>();
        int skillTreshold = 2;

        var skill1 = createMockSkill();
        var skill2 = createMockSkill();
        var skill3 = createMockSkill();
        var skill4 = createMockSkill();
        var skill5 = createMockSkill();

        skill2.setSkillName("attention");
        skill2.setId(2);
        skill3.setSkillName("sailors");
        skill3.setId(3);
        skill4.setSkillName("banana");
        skill4.setId(4);
        skill5.setSkillName("cat");
        skill5.setId(5);

        Set<Skill> tempSet = new java.util.HashSet<>(Set.of(skill1, skill2, skill3, skill4, skill5));

        tempSet.forEach(skill -> requirements.putIfAbsent(skill, SkillLevel.ADVANCED));
        Map<Skill, SkillLevel> testReqs = new HashMap<>(requirements);
        tempSet.remove(skill1);
        tempSet.remove(skill2);

        tempSet.forEach(skill -> skillSet.putIfAbsent(skill, SkillLevel.EXPERT));
        Map<Skill, SkillLevel> testSkillSet = new HashMap<>(skillSet);

        filteringHelper.checkForMatchingSkills(skillSet, requirements, skillTreshold);

        Assertions.assertAll(
                () -> assertEquals(5, requirements.size()),
                () -> assertEquals(3, skillSet.size()),
                () -> assertEquals(requirements, testReqs),
                () -> assertEquals(skillSet, testSkillSet)
        );
    }

    @Test
    public void getLocationFilter_Should_ReturnEmptyOptional_When_InputStringIsEmpty() {
        String input = "";
        Optional<Integer> result = filteringHelper.getLocationFilter(input);
        assertTrue(result.isEmpty());
    }

    @Test
    public void getLocationFilter_Should_ReturnEmptyOptional_When_InputStringIsNull() {
        String input = null;
        Optional<Integer> result = filteringHelper.getLocationFilter(input);
        assertTrue(result.isEmpty());
    }

    @Test
    public void getLocationFilter_Should_CallLocationService_When_InputStringContainsCommaDivider() {
        String input = "Sofia, Bulgaria";
        Mockito.when(service.getByCountryAndCity(anyString(), anyString())).thenReturn(createMockLocation());

        filteringHelper.getLocationFilter(input);
        Mockito.verify(service, Mockito.times(1)).getByCountryAndCity(anyString(), anyString());
    }

    @Test
    public void getLocationFilter_Should_CallLocationService_When_InputStringConsistsOfOnlyACity() {
        String input = "Sofia";
        Mockito.when(service.getByCity(anyString())).thenReturn(createMockLocation());

        filteringHelper.getLocationFilter(input);
        Mockito.verify(service, Mockito.times(1)).getByCity(anyString());
    }

    @Test
    public void getLocationFilter_Should_ReturnOptionalEmpty_When_NoLocationIsFound() {
        String input = "Sofia, Bulgaria";
        Mockito.when(service.getByCountryAndCity(anyString(), anyString())).thenReturn(null);

        Optional<Integer> result = filteringHelper.getLocationFilter(input);

        assertTrue(result.isEmpty());
    }

    @Test
    public void getLocationFilter_Should_ReturnOptionalOfLocationId_When_LocationIsFound() {
        String input = "Sofia, Bulgaria";
        var mockLocation = createMockLocation();
        Mockito.when(service.getByCountryAndCity(anyString(), anyString())).thenReturn(mockLocation);

        Optional<Integer> result = filteringHelper.getLocationFilter(input);

        Assertions.assertAll(
                () -> assertTrue(result.isPresent()),
                () -> assertEquals(result.get(), mockLocation.getId())
        );
    }


}
