## Job Match - One step closer to your dream job

## Project Description

Job Match is a web application where companies have open positions 
and professionals are looking for their next dream job.There are 3 different logins :Employees(representatives of the company), 
Companies and Professionals. Professionals can edit their information after logging in, they can create/update/delete new applications, they can view all their applications, they can apply for job openings, they can search for other professionals, companies and job openings. On the other hand, companies can edit their information, view their employees, they can 
generate new accounts  for employees or delete current accounts for employees(which are representatives of the company). Employees can view ,edit or delete job openings for companies. They can see all the applicants that have applied for the current job position and accept or decline their application. If they decide to accept the professionals application in the professionals dashboard a pop up will show with the current job opening they have applied to with a Pending status. Once the professional has confirmed that he is willing to work for the company that he has selected, the match is completed and both sides are happy. Employees can filter job openings by status.  

## Documentation
The application will be available [here](http://localhost:8080).

The REST part of the application is documented with Swagger and is available [here](http://localhost:8080/swagger-ui.html) while the application is running.



## Things we are proud of
* implementing HashMap through Hibernate and Thymeleaf 
* implementing HashMap for Match<MatchRequest, Boolean> 

## Technologies used
* Java
* Spring MVC
* Hibernate
* SQL 
* MariaDB
* Mockito
* JUnit
* HTML
* CSS
* Thymeleaf
* JavaScript
* JQuery
* AJAX
* Bootstrap

## Authors 

<br>Deyan Dimitrov  [Gitlab](https://gitlab.com/Deyan.D) <br/>
<br>Milen Mladenov [Gitlab](https://gitlab.com/MilenMladenoff) <br/>
<br>Yoan Bozhkov  [Gitlab](https://gitlab.com/yoanbozhkov) <br/>

### Image of database
<img src="db/db_schema.png" alt="database">


"If everyone is moving forward together, then success takes care of itself"
 `Henry Ford`