create or replace table job_match.accounts
(
    id       int auto_increment
        primary key,
    username varchar(20) not null,
    password varchar(25) not null,
    email    varchar(35) not null,
    constraint accounts_emails_pk
        unique (email),
    constraint accounts_username_pk
        unique (username)
);

create or replace table job_match.companies
(
    account_id int                  not null
        primary key,
    name       varchar(35)          not null,
    constraint companies_pk
        unique (name),
    constraint companies_accounts_fk
        foreign key (account_id) references job_match.accounts (id)
);

create or replace table job_match.employees
(
    account_id int         not null
        primary key,
    name       varchar(40) not null,
    company_id int         not null,
    constraint employees_accounts_fk
        foreign key (account_id) references job_match.accounts (id),
    constraint employees_companies_fk
        foreign key (company_id) references job_match.companies (account_id)
);

create or replace table job_match.locations
(
    id      int auto_increment
        primary key,
    country varchar(30) not null,
    city    varchar(30) not null,
    constraint locations_pk
        unique (country, city)
);

create or replace table job_match.basic_infos
(
    id          int auto_increment
        primary key,
    description text        not null,
    picture_url varchar(40) not null,
    location_id int         not null,
    constraint basic_infos_locations_fk
        foreign key (location_id) references job_match.locations (id)
);

create or replace table job_match.company_infos
(
    info_id    int          not null
        primary key,
    company_id int          not null,
    contacts   varchar(200) not null,
    constraint company_info_basic_infos_fk
        foreign key (info_id) references job_match.basic_infos (id),
    constraint company_info_companies_fk
        foreign key (company_id) references job_match.companies (account_id)
);

create or replace table job_match.job_forms
(
    id          int auto_increment
        primary key,
    name        varchar(45)          not null,
    salary_min  double               not null,
    salary_max  double               not null,
    description text                 not null,
    location_id int                  not null,
    is_remote   tinyint(1) default 0 not null,
    constraint job_forms_locations_fk
        foreign key (location_id) references job_match.locations (id),
    check (`salary_min` <= `salary_max`)
);

create or replace table job_match.job_openings
(
    job_form_id  int                                    not null
        primary key,
    company_id   int                                    not null,
    is_active    tinyint(1) default 1                   not null,
    published_on timestamp  default current_timestamp() null,
    constraint job_openings_companies_fk
        foreign key (company_id) references job_match.companies (account_id),
    constraint job_openings_job_forms_fk
        foreign key (job_form_id) references job_match.job_forms (id)
);

create or replace table job_match.professionals
(
    account_id int                  not null
        primary key,
    first_name varchar(15)          not null,
    last_name  varchar(15)          not null,
    constraint professional_accounts_fk
        foreign key (account_id) references job_match.accounts (id)
);

create or replace table job_match.applications
(
    job_form_id        int                                             not null
        primary key,
    professional_id    int                                             not null,
    application_status enum ('ACTIVE', 'HIDDEN', 'PRIVATE', 'MATCHED') not null,
    constraint applications_job_forms_fk
        foreign key (job_form_id) references job_match.job_forms (id),
    constraint applications_professionals_fk
        foreign key (professional_id) references job_match.professionals (account_id)
);

create or replace table job_match.match_requests
(
    match_request_id int auto_increment
        primary key,
    job_opening_id   int                  not null,
    application_id   int                  not null,
    is_match         tinyint(1) default 0 not null,
    constraint match_requests_pk2
        unique (application_id, job_opening_id),
    constraint match_requests_applications_fk
        foreign key (application_id) references job_match.applications (job_form_id),
    constraint match_requests_job_openings_fk
        foreign key (job_opening_id) references job_match.job_openings (job_form_id)
);

create or replace table job_match.professional_infos
(
    info_id         int                  not null
        primary key,
    professional_id int                  null,
    is_active       tinyint(1) default 1 not null,
    constraint professional_infos_basic_infos_fk
        foreign key (info_id) references job_match.basic_infos (id),
    constraint professional_infos_professionals_fk
        foreign key (professional_id) references job_match.professionals (account_id)
);

create or replace table job_match.professionals_main_applications
(
    professional_id int not null
        primary key,
    application_id  int not null,
    constraint professionals_main_applications_pk
        unique (application_id),
    constraint professionals_main_applications_applications_fk
        foreign key (application_id) references job_match.applications (job_form_id),
    constraint professionals_main_applications_professionals_fk
        foreign key (professional_id) references job_match.professionals (account_id)
);

create or replace table job_match.professionals_match_requests
(
    professional_id  int                  not null,
    match_request_id int                  not null,
    accepted_match   tinyint(1) default 0 not null,
    constraint professionals_match_requests_pk
        unique (professional_id, match_request_id),
    constraint professionals_match_requests_match_requests_fk
        foreign key (match_request_id) references job_match.match_requests (match_request_id),
    constraint professionals_match_requests_professionals_fk
        foreign key (professional_id) references job_match.professionals (account_id)
);

create or replace table job_match.skills
(
    id    int auto_increment
        primary key,
    skill varchar(30) not null
);

create or replace table job_match.job_forms_skillsets
(
    job_form_id int                                                                    not null,
    skill_id    int                                                                    not null,
    skill_level enum ('NOVICE', 'INTERMEDIATE', 'ADVANCED', 'EXPERT') default 'NOVICE' not null,
    primary key (job_form_id, skill_id),
    constraint job_forms_skillsets_job_forms_fk
        foreign key (job_form_id) references job_match.job_forms (id),
    constraint job_forms_skillsets_skills_fk
        foreign key (skill_id) references job_match.skills (id)
);

